<?xml version="1.0" encoding="UTF-8"?>
<solution name="hfu.mps.BeamLib" uuid="47ea545b-079c-437c-a69e-a4b00c2128f8" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
    <modelRoot contentPath="C:/Users/pschu/.m2/repository/org/apache/beam/beam-runners-direct-java" type="java_classes">
      <sourceRoot location="2.19.0/beam-runners-direct-java-2.19.0.jar" />
    </modelRoot>
    <modelRoot contentPath="C:/Users/pschu/.m2/repository/org/apache/beam/beam-sdks-java-core" type="java_classes">
      <sourceRoot location="2.19.0/beam-sdks-java-core-2.19.0.jar" />
    </modelRoot>
    <modelRoot contentPath="C:/Users/pschu/.m2/repository/joda-time/joda-time" type="java_classes">
      <sourceRoot location="2.10.3/joda-time-2.10.3.jar" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:f3061a53-9226-4cc5-a443-f952ceaf5816:jetbrains.mps.baseLanguage" version="9" />
    <language slang="l:f2801650-65d5-424e-bb1b-463a8781b786:jetbrains.mps.baseLanguage.javadoc" version="2" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:9ded098b-ad6a-4657-bfd9-48636cfe8bc3:jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="47ea545b-079c-437c-a69e-a4b00c2128f8(hfu.mps.BeamLib)" version="0" />
  </dependencyVersions>
</solution>

