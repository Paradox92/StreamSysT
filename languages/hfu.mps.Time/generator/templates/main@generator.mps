<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:37cc5618-05c2-4405-9786-53ba238d2faf(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="jyas" ref="r:55f07890-1eb3-4198-b15d-e4bd575dc4d1(hfu.mps.Time.structure)" />
    <import index="w08f" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.joda.time(hfu.mps.BeamLib/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1112730859144" name="jetbrains.mps.lang.generator.structure.TemplateSwitch" flags="ig" index="jVnub">
        <child id="1167340453568" name="reductionMappingRule" index="3aUrZf" />
      </concept>
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="5133195082121471908" name="jetbrains.mps.lang.generator.structure.LabelMacro" flags="ln" index="2ZBi8u" />
      <concept id="1167168920554" name="jetbrains.mps.lang.generator.structure.BaseMappingRule_Condition" flags="in" index="30G5F_" />
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <property id="1167272244852" name="applyToConceptInheritors" index="36QftV" />
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
        <child id="1167169362365" name="conditionFunction" index="30HLyM" />
      </concept>
      <concept id="1195158154974" name="jetbrains.mps.lang.generator.structure.InlineSwitch_RuleConsequence" flags="lg" index="14YyZ8">
        <child id="1195158241124" name="defaultConsequence" index="14YRTM" />
        <child id="1195158408710" name="case" index="14ZwWg" />
      </concept>
      <concept id="1195158388553" name="jetbrains.mps.lang.generator.structure.InlineSwitch_Case" flags="ng" index="14ZrTv">
        <child id="1195158608805" name="conditionFunction" index="150hEN" />
        <child id="1195158637244" name="caseConsequence" index="150oIE" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1087833466690" name="jetbrains.mps.lang.generator.structure.NodeMacro" flags="lg" index="17VmuZ">
        <reference id="1200912223215" name="mappingLabel" index="2rW$FS" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="982871510068000147" name="jetbrains.mps.lang.generator.structure.TemplateSwitchMacro" flags="lg" index="1sPUBX" />
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167770111131" name="jetbrains.mps.lang.generator.structure.ReferenceMacro_GetReferent" flags="in" index="3$xsQk" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1088761943574" name="jetbrains.mps.lang.generator.structure.ReferenceMacro" flags="ln" index="1ZhdrF">
        <child id="1167770376702" name="referentFunction" index="3$ytzL" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1216860049627" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOutputByLabelAndInput" flags="nn" index="1iwH70">
        <reference id="1216860049628" name="label" index="1iwH77" />
        <child id="1216860049632" name="inputNode" index="1iwH7V" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="role_DebugInfo" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="5Q9IfCXUjNh">
    <property role="TrG5h" value="beam" />
    <node concept="3aamgX" id="5Q9IfCXV4tX" role="3acgRq">
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="gft3U" id="6$tYh_AaOwB" role="1lVwrX">
        <node concept="10M0yZ" id="6$tYh_AaOx7" role="gfFT$">
          <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
          <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
          <node concept="1sPUBX" id="6$tYh_AaOxe" role="lGtFl">
            <ref role="v9R2y" node="5Q9IfCXVqO_" resolve="switch_Time" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6$tYh_AaJ5t" role="3acgRq">
      <ref role="30HIoZ" to="jyas:5Q9IfCXUkc_" resolve="TimeDeclaration" />
      <node concept="14YyZ8" id="6$tYh_AaJ5z" role="1lVwrX">
        <node concept="14ZrTv" id="6$tYh_AaJ5L" role="14ZwWg">
          <node concept="30G5F_" id="6$tYh_AaJ5M" role="150hEN">
            <node concept="3clFbS" id="6$tYh_AaJ5N" role="2VODD2">
              <node concept="3clFbF" id="6$tYh_AaJ9T" role="3cqZAp">
                <node concept="2OqwBi" id="6$tYh_AaKyZ" role="3clFbG">
                  <node concept="2OqwBi" id="6$tYh_AaJtZ" role="2Oq$k0">
                    <node concept="30H73N" id="6$tYh_AaJ9S" role="2Oq$k0" />
                    <node concept="2Xjw5R" id="6$tYh_AaJPF" role="2OqNvi">
                      <node concept="1xMEDy" id="6$tYh_AaJPH" role="1xVPHs">
                        <node concept="chp4Y" id="6$tYh_AaK2f" role="ri$Ld">
                          <ref role="cht4Q" to="tpee:6LFqxSRBTg8" resolve="MethodDeclaration" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3x8VRR" id="6$tYh_AaL6v" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="gft3U" id="6$tYh_AaLj2" role="150oIE">
            <node concept="3cpWs8" id="6$tYh_AaLsd" role="gfFT$">
              <node concept="3cpWsn" id="6$tYh_AaLse" role="3cpWs9">
                <property role="TrG5h" value="duration" />
                <node concept="3uibUv" id="6$tYh_AaLsl" role="1tU5fm">
                  <ref role="3uigEE" to="w08f:~Duration" resolve="Duration" />
                </node>
                <node concept="17Uvod" id="6$tYh_AaLsu" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="6$tYh_AaLsx" role="3zH0cK">
                    <node concept="3clFbS" id="6$tYh_AaLsy" role="2VODD2">
                      <node concept="3clFbF" id="6$tYh_AaLsC" role="3cqZAp">
                        <node concept="2OqwBi" id="6$tYh_AaLsz" role="3clFbG">
                          <node concept="3TrcHB" id="6$tYh_AaLsA" role="2OqNvi">
                            <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                          </node>
                          <node concept="30H73N" id="6$tYh_AaLsB" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="10M0yZ" id="6$tYh_AaMte" role="33vP2m">
                  <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
                  <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
                  <node concept="29HgVG" id="6$tYh_AaMtZ" role="lGtFl">
                    <node concept="3NFfHV" id="6$tYh_AaMu0" role="3NFExx">
                      <node concept="3clFbS" id="6$tYh_AaMu1" role="2VODD2">
                        <node concept="3clFbF" id="6$tYh_AaMu7" role="3cqZAp">
                          <node concept="2OqwBi" id="6$tYh_AaMu2" role="3clFbG">
                            <node concept="3TrEf2" id="6$tYh_AaMu5" role="2OqNvi">
                              <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcC" resolve="time" />
                            </node>
                            <node concept="30H73N" id="6$tYh_AaMu6" role="2Oq$k0" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2ZBi8u" id="6$tYh_AaMxl" role="lGtFl">
                  <ref role="2rW$FS" node="6$tYh_AaMxk" resolve="TimeRef" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="gft3U" id="6$tYh_AaNOz" role="14YRTM">
          <node concept="Wx3nA" id="6$tYh_AaNQj" role="gfFT$">
            <property role="TrG5h" value="duration" />
            <node concept="3Tm6S6" id="6$tYh_AaNQk" role="1B3o_S" />
            <node concept="3uibUv" id="6$tYh_AaNQr" role="1tU5fm">
              <ref role="3uigEE" to="w08f:~Duration" resolve="Duration" />
            </node>
            <node concept="10M0yZ" id="6$tYh_AaO4_" role="33vP2m">
              <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
              <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
              <node concept="29HgVG" id="6$tYh_AaO4K" role="lGtFl">
                <node concept="3NFfHV" id="6$tYh_AaO4L" role="3NFExx">
                  <node concept="3clFbS" id="6$tYh_AaO4M" role="2VODD2">
                    <node concept="3clFbF" id="6$tYh_AaO4S" role="3cqZAp">
                      <node concept="2OqwBi" id="6$tYh_AaO4N" role="3clFbG">
                        <node concept="3TrEf2" id="6$tYh_AaO4Q" role="2OqNvi">
                          <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcC" resolve="time" />
                        </node>
                        <node concept="30H73N" id="6$tYh_AaO4R" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2ZBi8u" id="6$tYh_AaOaj" role="lGtFl">
              <ref role="2rW$FS" node="6$tYh_AaMxk" resolve="TimeRef" />
            </node>
            <node concept="17Uvod" id="4vp2seWc0zs" role="lGtFl">
              <property role="2qtEX9" value="name" />
              <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
              <node concept="3zFVjK" id="4vp2seWc0zt" role="3zH0cK">
                <node concept="3clFbS" id="4vp2seWc0zu" role="2VODD2">
                  <node concept="3clFbF" id="4vp2seWc0_j" role="3cqZAp">
                    <node concept="2OqwBi" id="4vp2seWc0QZ" role="3clFbG">
                      <node concept="30H73N" id="4vp2seWc0_i" role="2Oq$k0" />
                      <node concept="3TrcHB" id="4vp2seWc1bc" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6$tYh_AaOxk" role="3acgRq">
      <ref role="30HIoZ" to="jyas:5Q9IfCXUkcF" resolve="TimePlusOperation" />
      <node concept="gft3U" id="6$tYh_AaOJl" role="1lVwrX">
        <node concept="2OqwBi" id="6$tYh_AaP3X" role="gfFT$">
          <node concept="10M0yZ" id="6$tYh_AaOJK" role="2Oq$k0">
            <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
            <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
            <node concept="29HgVG" id="6$tYh_AaPxi" role="lGtFl">
              <node concept="3NFfHV" id="6$tYh_AaPxj" role="3NFExx">
                <node concept="3clFbS" id="6$tYh_AaPxk" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaPxq" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaPxl" role="3clFbG">
                      <node concept="3TrEf2" id="6$tYh_AaPxo" role="2OqNvi">
                        <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcG" resolve="left" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaPxp" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="liA8E" id="6$tYh_AaPor" role="2OqNvi">
            <ref role="37wK5l" to="w08f:~Duration.plus(org.joda.time.ReadableDuration)" resolve="plus" />
            <node concept="10M0yZ" id="6$tYh_AaPpV" role="37wK5m">
              <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
              <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
              <node concept="29HgVG" id="6$tYh_AaPs2" role="lGtFl">
                <node concept="3NFfHV" id="6$tYh_AaPs3" role="3NFExx">
                  <node concept="3clFbS" id="6$tYh_AaPs4" role="2VODD2">
                    <node concept="3clFbF" id="6$tYh_AaPsa" role="3cqZAp">
                      <node concept="2OqwBi" id="6$tYh_AaPs5" role="3clFbG">
                        <node concept="3TrEf2" id="6$tYh_AaPs8" role="2OqNvi">
                          <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcI" resolve="right" />
                        </node>
                        <node concept="30H73N" id="6$tYh_AaPs9" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6$tYh_AaPzl" role="3acgRq">
      <ref role="30HIoZ" to="jyas:5Q9IfCXUkcL" resolve="TimeMinusOperation" />
      <node concept="gft3U" id="6$tYh_AaPOM" role="1lVwrX">
        <node concept="2OqwBi" id="6$tYh_AaPON" role="gfFT$">
          <node concept="10M0yZ" id="6$tYh_AaPOO" role="2Oq$k0">
            <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
            <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
            <node concept="29HgVG" id="6$tYh_AaPOP" role="lGtFl">
              <node concept="3NFfHV" id="6$tYh_AaPOQ" role="3NFExx">
                <node concept="3clFbS" id="6$tYh_AaPOR" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaPOS" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaPOT" role="3clFbG">
                      <node concept="3TrEf2" id="6$tYh_AaPOU" role="2OqNvi">
                        <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcG" resolve="left" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaPOV" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="liA8E" id="6$tYh_AaPOW" role="2OqNvi">
            <ref role="37wK5l" to="w08f:~Duration.minus(org.joda.time.ReadableDuration)" resolve="minus" />
            <node concept="10M0yZ" id="6$tYh_AaPOX" role="37wK5m">
              <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
              <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
              <node concept="29HgVG" id="6$tYh_AaPOY" role="lGtFl">
                <node concept="3NFfHV" id="6$tYh_AaPOZ" role="3NFExx">
                  <node concept="3clFbS" id="6$tYh_AaPP0" role="2VODD2">
                    <node concept="3clFbF" id="6$tYh_AaPP1" role="3cqZAp">
                      <node concept="2OqwBi" id="6$tYh_AaPP2" role="3clFbG">
                        <node concept="3TrEf2" id="6$tYh_AaPP3" role="2OqNvi">
                          <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcI" resolve="right" />
                        </node>
                        <node concept="30H73N" id="6$tYh_AaPP4" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6$tYh_AhFc_" role="3acgRq">
      <ref role="30HIoZ" to="jyas:5Q9IfCXUkcw" resolve="TimeReference" />
      <node concept="gft3U" id="6$tYh_AhFln" role="1lVwrX">
        <node concept="37vLTw" id="6$tYh_AhHz0" role="gfFT$">
          <node concept="1ZhdrF" id="6$tYh_AhHz3" role="lGtFl">
            <property role="2qtEX8" value="variableDeclaration" />
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <node concept="3$xsQk" id="6$tYh_AhHz4" role="3$ytzL">
              <node concept="3clFbS" id="6$tYh_AhHz5" role="2VODD2">
                <node concept="3clFbF" id="6$tYh_AhHzS" role="3cqZAp">
                  <node concept="2OqwBi" id="6$tYh_AhHKg" role="3clFbG">
                    <node concept="1iwH7S" id="6$tYh_AhHzR" role="2Oq$k0" />
                    <node concept="1iwH70" id="6$tYh_AhHPE" role="2OqNvi">
                      <ref role="1iwH77" node="6$tYh_AaMxk" resolve="TimeRef" />
                      <node concept="2OqwBi" id="6$tYh_AhIav" role="1iwH7V">
                        <node concept="30H73N" id="6$tYh_AhHYS" role="2Oq$k0" />
                        <node concept="3TrEf2" id="6$tYh_AhIlu" role="2OqNvi">
                          <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcz" resolve="ref" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2rT7sh" id="6$tYh_AaMxk" role="2rTMjI">
      <property role="TrG5h" value="TimeRef" />
      <ref role="2rTdP9" to="jyas:5Q9IfCXUkc_" resolve="TimeDeclaration" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
    </node>
  </node>
  <node concept="jVnub" id="5Q9IfCXVqO_">
    <property role="TrG5h" value="switch_Time" />
    <node concept="3aamgX" id="5Q9IfCXVrLZ" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="gft3U" id="6$tYh_AaHP$" role="1lVwrX">
        <node concept="2YIFZM" id="6$tYh_AaIeu" role="gfFT$">
          <ref role="37wK5l" to="w08f:~Duration.millis(long)" resolve="millis" />
          <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
          <node concept="3cmrfG" id="6$tYh_AaIeD" role="37wK5m">
            <property role="3cmrfH" value="0" />
            <node concept="17Uvod" id="6$tYh_AaIfG" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
              <node concept="3zFVjK" id="6$tYh_AaIfJ" role="3zH0cK">
                <node concept="3clFbS" id="6$tYh_AaIfK" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaIfQ" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaIfL" role="3clFbG">
                      <node concept="3TrcHB" id="6$tYh_AaIfO" role="2OqNvi">
                        <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaIfP" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="5Q9IfCXVrM1" role="30HLyM">
        <node concept="3clFbS" id="5Q9IfCXVrM2" role="2VODD2">
          <node concept="3clFbF" id="5Q9IfCXVrM3" role="3cqZAp">
            <node concept="2OqwBi" id="5Q9IfCXVrM4" role="3clFbG">
              <node concept="2OqwBi" id="5Q9IfCXVrM5" role="2Oq$k0">
                <node concept="30H73N" id="5Q9IfCXVrM6" role="2Oq$k0" />
                <node concept="3TrcHB" id="5Q9IfCXVrM7" role="2OqNvi">
                  <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                </node>
              </node>
              <node concept="21noJN" id="5Q9IfCXVrM8" role="2OqNvi">
                <node concept="21nZrQ" id="5Q9IfCXVrM9" role="21noJM">
                  <ref role="21nZrZ" to="jyas:5Q9IfCXUkdH" resolve="Milliseconds" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5Q9IfCXVqOA" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="30G5F_" id="5Q9IfCXVqOE" role="30HLyM">
        <node concept="3clFbS" id="5Q9IfCXVqOF" role="2VODD2">
          <node concept="3clFbF" id="5Q9IfCXVqSB" role="3cqZAp">
            <node concept="2OqwBi" id="5Q9IfCXVrp9" role="3clFbG">
              <node concept="2OqwBi" id="5Q9IfCXVr3_" role="2Oq$k0">
                <node concept="30H73N" id="5Q9IfCXVqSA" role="2Oq$k0" />
                <node concept="3TrcHB" id="5Q9IfCXVrdw" role="2OqNvi">
                  <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                </node>
              </node>
              <node concept="21noJN" id="5Q9IfCXVrzE" role="2OqNvi">
                <node concept="21nZrQ" id="5Q9IfCXVs98" role="21noJM">
                  <ref role="21nZrZ" to="jyas:5Q9IfCXUkdI" resolve="Seconds" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="gft3U" id="6$tYh_AaIrB" role="1lVwrX">
        <node concept="2YIFZM" id="6$tYh_AaISB" role="gfFT$">
          <ref role="37wK5l" to="w08f:~Duration.standardSeconds(long)" resolve="standardSeconds" />
          <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
          <node concept="3cmrfG" id="6$tYh_AaISC" role="37wK5m">
            <property role="3cmrfH" value="0" />
            <node concept="17Uvod" id="6$tYh_AaISD" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
              <node concept="3zFVjK" id="6$tYh_AaISE" role="3zH0cK">
                <node concept="3clFbS" id="6$tYh_AaISF" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaISG" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaISH" role="3clFbG">
                      <node concept="3TrcHB" id="6$tYh_AaISI" role="2OqNvi">
                        <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaISJ" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5Q9IfCXVrTC" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="30G5F_" id="5Q9IfCXVrTE" role="30HLyM">
        <node concept="3clFbS" id="5Q9IfCXVrTF" role="2VODD2">
          <node concept="3clFbF" id="5Q9IfCXVrTG" role="3cqZAp">
            <node concept="2OqwBi" id="5Q9IfCXVrTH" role="3clFbG">
              <node concept="2OqwBi" id="5Q9IfCXVrTI" role="2Oq$k0">
                <node concept="30H73N" id="5Q9IfCXVrTJ" role="2Oq$k0" />
                <node concept="3TrcHB" id="5Q9IfCXVrTK" role="2OqNvi">
                  <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                </node>
              </node>
              <node concept="21noJN" id="5Q9IfCXVrTL" role="2OqNvi">
                <node concept="21nZrQ" id="5Q9IfCXVsh1" role="21noJM">
                  <ref role="21nZrZ" to="jyas:5Q9IfCXUkdL" resolve="Minutes" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="gft3U" id="6$tYh_AaIIe" role="1lVwrX">
        <node concept="2YIFZM" id="6$tYh_AaIRi" role="gfFT$">
          <ref role="37wK5l" to="w08f:~Duration.standardMinutes(long)" resolve="standardMinutes" />
          <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
          <node concept="3cmrfG" id="6$tYh_AaIRj" role="37wK5m">
            <property role="3cmrfH" value="0" />
            <node concept="17Uvod" id="6$tYh_AaIRk" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
              <node concept="3zFVjK" id="6$tYh_AaIRl" role="3zH0cK">
                <node concept="3clFbS" id="6$tYh_AaIRm" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaIRn" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaIRo" role="3clFbG">
                      <node concept="3TrcHB" id="6$tYh_AaIRp" role="2OqNvi">
                        <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaIRq" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5Q9IfCXVrWR" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="30G5F_" id="5Q9IfCXVrWT" role="30HLyM">
        <node concept="3clFbS" id="5Q9IfCXVrWU" role="2VODD2">
          <node concept="3clFbF" id="5Q9IfCXVrWV" role="3cqZAp">
            <node concept="2OqwBi" id="5Q9IfCXVrWW" role="3clFbG">
              <node concept="2OqwBi" id="5Q9IfCXVrWX" role="2Oq$k0">
                <node concept="30H73N" id="5Q9IfCXVrWY" role="2Oq$k0" />
                <node concept="3TrcHB" id="5Q9IfCXVrWZ" role="2OqNvi">
                  <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                </node>
              </node>
              <node concept="21noJN" id="5Q9IfCXVrX0" role="2OqNvi">
                <node concept="21nZrQ" id="5Q9IfCXVsoU" role="21noJM">
                  <ref role="21nZrZ" to="jyas:5Q9IfCXUkdP" resolve="Hours" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="gft3U" id="6$tYh_AaIKi" role="1lVwrX">
        <node concept="2YIFZM" id="6$tYh_AaIPX" role="gfFT$">
          <ref role="37wK5l" to="w08f:~Duration.standardHours(long)" resolve="standardHours" />
          <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
          <node concept="3cmrfG" id="6$tYh_AaIPY" role="37wK5m">
            <property role="3cmrfH" value="0" />
            <node concept="17Uvod" id="6$tYh_AaIPZ" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
              <node concept="3zFVjK" id="6$tYh_AaIQ0" role="3zH0cK">
                <node concept="3clFbS" id="6$tYh_AaIQ1" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaIQ2" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaIQ3" role="3clFbG">
                      <node concept="3TrcHB" id="6$tYh_AaIQ4" role="2OqNvi">
                        <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaIQ5" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="5Q9IfCXVs0P" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="jyas:5Q9IfCXUjXT" resolve="Time" />
      <node concept="30G5F_" id="5Q9IfCXVs0R" role="30HLyM">
        <node concept="3clFbS" id="5Q9IfCXVs0S" role="2VODD2">
          <node concept="3clFbF" id="5Q9IfCXVs0T" role="3cqZAp">
            <node concept="2OqwBi" id="5Q9IfCXVs0U" role="3clFbG">
              <node concept="2OqwBi" id="5Q9IfCXVs0V" role="2Oq$k0">
                <node concept="30H73N" id="5Q9IfCXVs0W" role="2Oq$k0" />
                <node concept="3TrcHB" id="5Q9IfCXVs0X" role="2OqNvi">
                  <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                </node>
              </node>
              <node concept="21noJN" id="5Q9IfCXVs0Y" role="2OqNvi">
                <node concept="21nZrQ" id="5Q9IfCXVstd" role="21noJM">
                  <ref role="21nZrZ" to="jyas:5Q9IfCXUkdU" resolve="Days" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="gft3U" id="6$tYh_AaIMo" role="1lVwrX">
        <node concept="2YIFZM" id="6$tYh_AaIOC" role="gfFT$">
          <ref role="37wK5l" to="w08f:~Duration.standardDays(long)" resolve="standardDays" />
          <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
          <node concept="3cmrfG" id="6$tYh_AaIOD" role="37wK5m">
            <property role="3cmrfH" value="0" />
            <node concept="17Uvod" id="6$tYh_AaIOE" role="lGtFl">
              <property role="2qtEX9" value="value" />
              <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
              <node concept="3zFVjK" id="6$tYh_AaIOF" role="3zH0cK">
                <node concept="3clFbS" id="6$tYh_AaIOG" role="2VODD2">
                  <node concept="3clFbF" id="6$tYh_AaIOH" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AaIOI" role="3clFbG">
                      <node concept="3TrcHB" id="6$tYh_AaIOJ" role="2OqNvi">
                        <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                      </node>
                      <node concept="30H73N" id="6$tYh_AaIOK" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

