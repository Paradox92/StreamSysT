<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:55f07890-1eb3-4198-b15d-e4bd575dc4d1(hfu.mps.Time.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
        <property id="672037151186491528" name="presentation" index="1L1pqM" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="5Q9IfCXUjXS">
    <property role="TrG5h" value="ITime" />
    <property role="EcuMT" value="6740121702256689015" />
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUjXT">
    <property role="EcuMT" value="6740121702256689017" />
    <property role="TrG5h" value="Time" />
    <property role="34LRSv" value="Time" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="PrWs8" id="5Q9IfCXUkcu" role="PzmwI">
      <ref role="PrY4T" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyi" id="5Q9IfCXUke0" role="1TKVEl">
      <property role="IQ2nx" value="6740121702256690048" />
      <property role="TrG5h" value="duration" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="5Q9IfCXUke2" role="1TKVEl">
      <property role="IQ2nx" value="6740121702256690050" />
      <property role="TrG5h" value="unit" />
      <ref role="AX2Wp" node="5Q9IfCXUkdG" resolve="TimeUnit" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUkcw">
    <property role="EcuMT" value="6740121702256689952" />
    <property role="TrG5h" value="TimeReference" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="PrWs8" id="5Q9IfCXUkcx" role="PzmwI">
      <ref role="PrY4T" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyj" id="5Q9IfCXUkcz" role="1TKVEi">
      <property role="IQ2ns" value="6740121702256689955" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5Q9IfCXUkc_" resolve="TimeDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUkc_">
    <property role="EcuMT" value="6740121702256689957" />
    <property role="TrG5h" value="TimeDeclaration" />
    <property role="34LRSv" value="Time" />
    <ref role="1TJDcQ" to="tpee:fzclF8l" resolve="Statement" />
    <node concept="PrWs8" id="5Q9IfCXUkcA" role="PzmwI">
      <ref role="PrY4T" to="tpee:h9ngReX" resolve="ClassifierMember" />
    </node>
    <node concept="PrWs8" id="5Q9IfCXULVJ" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
    <node concept="1TJgyj" id="5Q9IfCXUkcC" role="1TKVEi">
      <property role="IQ2ns" value="6740121702256689960" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="time" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUkcE">
    <property role="EcuMT" value="6740121702256689962" />
    <property role="TrG5h" value="TimeBinaryOperation" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="5Q9IfCXUkcG" role="1TKVEi">
      <property role="IQ2ns" value="6740121702256689964" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="left" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyj" id="5Q9IfCXUkcI" role="1TKVEi">
      <property role="IQ2ns" value="6740121702256689966" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="right" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="PrWs8" id="5Q9IfCXUX5j" role="PzmwI">
      <ref role="PrY4T" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUkcF">
    <property role="EcuMT" value="6740121702256689963" />
    <property role="TrG5h" value="TimePlusOperation" />
    <property role="34LRSv" value="+" />
    <ref role="1TJDcQ" node="5Q9IfCXUkcE" resolve="TimeBinaryOperation" />
  </node>
  <node concept="1TIwiD" id="5Q9IfCXUkcL">
    <property role="EcuMT" value="6740121702256689969" />
    <property role="TrG5h" value="TimeMinusOperation" />
    <property role="34LRSv" value="-" />
    <ref role="1TJDcQ" node="5Q9IfCXUkcE" resolve="TimeBinaryOperation" />
  </node>
  <node concept="25R3W" id="5Q9IfCXUkdG">
    <property role="3F6X1D" value="6740121702256690028" />
    <property role="TrG5h" value="TimeUnit" />
    <node concept="25R33" id="5Q9IfCXUkdH" role="25R1y">
      <property role="3tVfz5" value="6740121702256690029" />
      <property role="TrG5h" value="Milliseconds" />
      <property role="1L1pqM" value="S" />
    </node>
    <node concept="25R33" id="5Q9IfCXUkdI" role="25R1y">
      <property role="3tVfz5" value="6740121702256690030" />
      <property role="TrG5h" value="Seconds" />
      <property role="1L1pqM" value="s" />
    </node>
    <node concept="25R33" id="5Q9IfCXUkdL" role="25R1y">
      <property role="3tVfz5" value="6740121702256690033" />
      <property role="TrG5h" value="Minutes" />
      <property role="1L1pqM" value="m" />
    </node>
    <node concept="25R33" id="5Q9IfCXUkdP" role="25R1y">
      <property role="3tVfz5" value="6740121702256690037" />
      <property role="TrG5h" value="Hours" />
      <property role="1L1pqM" value="h" />
    </node>
    <node concept="25R33" id="5Q9IfCXUkdU" role="25R1y">
      <property role="3tVfz5" value="6740121702256690042" />
      <property role="TrG5h" value="Days" />
      <property role="1L1pqM" value="d" />
    </node>
  </node>
  <node concept="1TIwiD" id="1UrpsVVz5W2">
    <property role="TrG5h" value="IncompleteTime" />
    <property role="EcuMT" value="2205468382184713673" />
    <property role="R5$K7" value="true" />
    <node concept="1TJgyi" id="1UrpsVVyWc$" role="1TKVEl">
      <property role="IQ2nx" value="2205468382185898788" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="PrWs8" id="1UrpsVVuqRa" role="PzmwI">
      <ref role="PrY4T" node="5Q9IfCXUjXS" resolve="ITime" />
    </node>
  </node>
</model>

