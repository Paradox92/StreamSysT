<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:a03a10d1-9179-4765-9075-21630f8886de(hfu.mps.Time.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="13" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" />
    <import index="jyas" ref="r:55f07890-1eb3-4198-b15d-e4bd575dc4d1(hfu.mps.Time.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1597643335227097138" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_TransformationMenu_node" flags="ng" index="7Obwk" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="784421273959492578" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_IncludeMenu" flags="ng" index="mvV$s">
        <child id="6718020819487784677" name="menuReference" index="A14EM" />
      </concept>
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="6718020819487620873" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Named" flags="ng" index="A1WHu">
        <reference id="6718020819487620874" name="menu" index="A1WHt" />
      </concept>
      <concept id="3473224453637651916" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform_PlaceInCellHolder" flags="ng" index="CtIbL">
        <property id="3473224453637651917" name="placeInCell" index="CtIbK" />
      </concept>
      <concept id="1638911550608571617" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Default" flags="ng" index="IW6AY" />
      <concept id="1638911550608610798" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Execute" flags="ig" index="IWg2L" />
      <concept id="1638911550608610278" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Action" flags="ng" index="IWgqT">
        <child id="1638911550608610281" name="executeFunction" index="IWgqQ" />
        <child id="5692353713941573325" name="textFunction" index="1hCUd6" />
      </concept>
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="2896773699153795590" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform" flags="ng" index="3cWJ9i">
        <child id="3473224453637651919" name="placeInCell" index="CtIbM" />
      </concept>
      <concept id="5692353713941573329" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_ActionLabelText" flags="ig" index="1hCUdq" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="3308396621974580100" name="jetbrains.mps.lang.editor.structure.SubstituteMenu_Default" flags="ng" index="3p36aQ" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1215007883204" name="jetbrains.mps.lang.editor.structure.PaddingLeftStyleClassItem" flags="ln" index="3$7fVu" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="7985135009827365938" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Placeholder" flags="ng" index="1IAO7e" />
      <concept id="5624877018226904808" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Named" flags="ng" index="3ICXOK" />
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1138661924179" name="jetbrains.mps.lang.smodel.structure.Property_SetOperation" flags="nn" index="tyxLq">
        <child id="1138662048170" name="value" index="tz02z" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="5Q9IfCXUkcU">
    <ref role="1XX52x" to="jyas:5Q9IfCXUkc_" resolve="TimeDeclaration" />
    <node concept="3EZMnI" id="5Q9IfCXUkcW" role="2wV5jI">
      <node concept="3F0ifn" id="5Q9IfCXUkd3" role="3EZMnx">
        <property role="3F0ifm" value="Time" />
      </node>
      <node concept="3F0A7n" id="5Q9IfCXULVT" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5Q9IfCXUPAc" role="3EZMnx">
        <property role="3F0ifm" value=":" />
        <node concept="11L4FC" id="5Q9IfCXURR$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="5Q9IfCXUkdl" role="3EZMnx">
        <ref role="1NtTu8" to="jyas:5Q9IfCXUkcC" resolve="time" />
      </node>
      <node concept="l2Vlx" id="5Q9IfCXUkcZ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5Q9IfCXUkdy">
    <ref role="1XX52x" to="jyas:5Q9IfCXUjXT" resolve="Time" />
    <node concept="3EZMnI" id="5Q9IfCXUkd$" role="2wV5jI">
      <node concept="3F0A7n" id="5Q9IfCXUke5" role="3EZMnx">
        <ref role="1NtTu8" to="jyas:5Q9IfCXUke0" resolve="duration" />
        <node concept="A1WHu" id="1yapuqB6B6q" role="3vIgyS">
          <ref role="A1WHt" node="1yapuqB6$PJ" resolve="Time_Left" />
        </node>
      </node>
      <node concept="3F0A7n" id="5Q9IfCXUkeb" role="3EZMnx">
        <ref role="1k5W1q" to="tpen:hFITtyA" resolve="CompactKeyWord" />
        <ref role="1NtTu8" to="jyas:5Q9IfCXUke2" resolve="unit" />
        <node concept="3$7fVu" id="4vp2seWdnz_" role="3F10Kt">
          <property role="3$6WeP" value="-0.75" />
        </node>
        <node concept="A1WHu" id="1yapuqB6B6u" role="3vIgyS">
          <ref role="A1WHt" node="1yapuqB6wPm" resolve="Time_Right" />
        </node>
      </node>
      <node concept="l2Vlx" id="1yapuqB6ctr" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5Q9IfCXUkex">
    <ref role="1XX52x" to="jyas:5Q9IfCXUkcE" resolve="TimeBinaryOperation" />
    <node concept="3EZMnI" id="5Q9IfCXUkez" role="2wV5jI">
      <node concept="3F1sOY" id="5Q9IfCXUkeE" role="3EZMnx">
        <property role="1$x2rV" value="&lt;time&gt;" />
        <ref role="1NtTu8" to="jyas:5Q9IfCXUkcG" resolve="left" />
      </node>
      <node concept="PMmxH" id="5Q9IfCXUkeK" role="3EZMnx">
        <ref role="PMmxG" to="tpco:2wZex4PafBj" resolve="alias" />
      </node>
      <node concept="3F1sOY" id="5Q9IfCXUkeW" role="3EZMnx">
        <property role="1$x2rV" value="&lt;time&gt;" />
        <ref role="1NtTu8" to="jyas:5Q9IfCXUkcI" resolve="right" />
      </node>
      <node concept="l2Vlx" id="5Q9IfCXUkeA" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5Q9IfCXUkf9">
    <ref role="1XX52x" to="jyas:5Q9IfCXUkcw" resolve="TimeReference" />
    <node concept="1iCGBv" id="5Q9IfCXUkfb" role="2wV5jI">
      <ref role="1NtTu8" to="jyas:5Q9IfCXUkcz" resolve="ref" />
      <node concept="1sVBvm" id="5Q9IfCXUkfd" role="1sWHZn">
        <node concept="3F0A7n" id="5Q9IfCXUkfk" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="A1WHr" id="1UrpsVV_19E" role="3vIgyS">
        <ref role="2ZyFGn" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
      </node>
    </node>
  </node>
  <node concept="IW6AY" id="1UrpsVVurrH">
    <ref role="aqKnT" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
    <node concept="1Qtc8_" id="Pq8sy4fOhU" role="IW6Ez">
      <node concept="3cWJ9i" id="Pq8sy4fOhV" role="1Qtc8$">
        <node concept="CtIbL" id="Pq8sy4fOhW" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fOhX" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fOhY" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fOhZ" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fOi0" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fOi1" role="3clFbG">
                <property role="Xl_RC" value="S" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fOi2" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fOi3" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fOi4" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fOi5" role="3cpWs9">
                <property role="TrG5h" value="time" />
                <node concept="3Tqbb2" id="Pq8sy4fOi6" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fOi7" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fOi8" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fOi9" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOia" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fOib" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOic" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fOid" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UrpsVVz6xv" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4fOif" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fOig" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOi5" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOih" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOii" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fOij" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOik" role="2Oq$k0">
                  <node concept="37vLTw" id="Pq8sy4fOil" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOi5" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOim" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                  </node>
                </node>
                <node concept="tyxLq" id="Pq8sy4fOin" role="2OqNvi">
                  <node concept="21nZrQ" id="Pq8sy4fOio" role="tz02z">
                    <ref role="21nZrZ" to="jyas:5Q9IfCXUkdH" resolve="Milliseconds" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4goac" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4gocy" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4goaa" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4goeo" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4gogo" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fOi5" resolve="time" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fOis" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fOit" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fOiu" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fOiv" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fOiw" role="3clFbG">
                <property role="Xl_RC" value="s" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fOix" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fOiy" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fOiz" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fOi$" role="3cpWs9">
                <property role="TrG5h" value="time" />
                <node concept="3Tqbb2" id="Pq8sy4fOi_" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fOiA" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fOiB" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fOiC" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOiD" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fOiE" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOiF" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fOiG" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UrpsVVz6RG" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4fOiI" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fOiJ" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOi$" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOiK" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOiL" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fOiM" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOiN" role="2Oq$k0">
                  <node concept="37vLTw" id="Pq8sy4fOiO" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOi$" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOiP" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                  </node>
                </node>
                <node concept="tyxLq" id="Pq8sy4fOiQ" role="2OqNvi">
                  <node concept="21nZrQ" id="Pq8sy4fOiR" role="tz02z">
                    <ref role="21nZrZ" to="jyas:5Q9IfCXUkdI" resolve="Seconds" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4goiG" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4goiH" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4goiI" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4goiJ" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4gonG" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fOi$" resolve="time" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fOiV" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fOiW" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fOiX" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fOiY" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fOiZ" role="3clFbG">
                <property role="Xl_RC" value="m" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fOj0" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fOj1" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fOj2" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fOj3" role="3cpWs9">
                <property role="TrG5h" value="time" />
                <node concept="3Tqbb2" id="Pq8sy4fOj4" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fOj5" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fOj6" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fOj7" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOj8" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fOj9" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOja" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fOjb" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UrpsVVz7a1" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4fOjd" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fOje" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOj3" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOjf" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOjg" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fOjh" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOji" role="2Oq$k0">
                  <node concept="37vLTw" id="Pq8sy4fOjj" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOj3" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOjk" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                  </node>
                </node>
                <node concept="tyxLq" id="Pq8sy4fOjl" role="2OqNvi">
                  <node concept="21nZrQ" id="Pq8sy4fOjm" role="tz02z">
                    <ref role="21nZrZ" to="jyas:5Q9IfCXUkdL" resolve="Minutes" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4goqn" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4goqo" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4goqp" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4goqq" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4gots" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fOj3" resolve="time" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fOjq" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fOjr" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fOjs" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fOjt" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fOju" role="3clFbG">
                <property role="Xl_RC" value="h" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fOjv" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fOjw" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fOjx" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fOjy" role="3cpWs9">
                <property role="TrG5h" value="time" />
                <node concept="3Tqbb2" id="Pq8sy4fOjz" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fOj$" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fOj_" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fOjA" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOjB" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fOjC" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOjD" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fOjE" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UrpsVVz7jA" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4fOjG" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fOjH" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOjy" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOjI" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOjJ" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fOjK" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOjL" role="2Oq$k0">
                  <node concept="37vLTw" id="Pq8sy4fOjM" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOjy" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOjN" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                  </node>
                </node>
                <node concept="tyxLq" id="Pq8sy4fOjO" role="2OqNvi">
                  <node concept="21nZrQ" id="Pq8sy4fOjP" role="tz02z">
                    <ref role="21nZrZ" to="jyas:5Q9IfCXUkdP" resolve="Hours" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4gox3" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4gox4" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4gox5" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4gox6" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4goES" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fOjy" resolve="time" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fOjT" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fOjU" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fOjV" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fOjW" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fOjX" role="3clFbG">
                <property role="Xl_RC" value="d" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fOjY" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fOjZ" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fOk0" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fOk1" role="3cpWs9">
                <property role="TrG5h" value="time" />
                <node concept="3Tqbb2" id="Pq8sy4fOk2" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fOk3" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fOk4" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fOk5" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUjXT" resolve="Time" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOk6" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fOk7" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOk8" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fOk9" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1UrpsVVz7tb" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4fOkb" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fOkc" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOk1" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOkd" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke0" resolve="duration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fOke" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fOkf" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fOkg" role="2Oq$k0">
                  <node concept="37vLTw" id="Pq8sy4fOkh" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fOk1" resolve="time" />
                  </node>
                  <node concept="3TrcHB" id="Pq8sy4fOki" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:5Q9IfCXUke2" resolve="unit" />
                  </node>
                </node>
                <node concept="tyxLq" id="Pq8sy4fOkj" role="2OqNvi">
                  <node concept="21nZrQ" id="Pq8sy4fOkk" role="tz02z">
                    <ref role="21nZrZ" to="jyas:5Q9IfCXUkdU" resolve="Days" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4goIV" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4goIW" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4goIX" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4goIY" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4goMW" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fOk1" resolve="time" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="IW6AY" id="1UrpsVVw7No">
    <ref role="aqKnT" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    <node concept="1Qtc8_" id="1yapuqB6zu2" role="IW6Ez">
      <node concept="3cWJ9i" id="1yapuqB6zAQ" role="1Qtc8$">
        <node concept="CtIbL" id="1yapuqB6zAS" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
      <node concept="mvV$s" id="1yapuqB6zAW" role="1Qtc8A">
        <node concept="A1WHu" id="1yapuqB6zAY" role="A14EM">
          <ref role="A1WHt" node="1yapuqB6wPm" resolve="Time_Right" />
        </node>
      </node>
    </node>
    <node concept="1Qtc8_" id="1yapuqB6A85" role="IW6Ez">
      <node concept="3cWJ9i" id="1yapuqB6Aat" role="1Qtc8$">
        <node concept="CtIbL" id="1yapuqB6Aav" role="CtIbM">
          <property role="CtIbK" value="1A4kJjlVmVt/LEFT" />
        </node>
      </node>
      <node concept="mvV$s" id="1yapuqB6Aaz" role="1Qtc8A">
        <node concept="A1WHu" id="1yapuqB6Aa_" role="A14EM">
          <ref role="A1WHt" node="1yapuqB6$PJ" resolve="Time_Left" />
        </node>
      </node>
    </node>
    <node concept="1Qtc8_" id="1UrpsVVvkcY" role="IW6Ez">
      <node concept="IWgqT" id="Pq8sy4hfEp" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4hfEq" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4hfEr" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4hfEs" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4hfEt" role="3clFbG">
                <property role="Xl_RC" value="0" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4hfEu" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4hfEv" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4hfEw" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4hfEx" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="Pq8sy4hfEy" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4hfEz" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4hfE$" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4hfE_" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfEA" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4hfEB" role="3clFbG">
                <node concept="3cmrfG" id="Pq8sy4hfEC" role="37vLTx">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4hfED" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4hfEE" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4hfEx" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzr_L" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfEG" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4hfEH" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4hfEI" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4hfEJ" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4hfEK" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4hfEx" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4hfx6" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4hfx7" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4hfx8" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4hfx9" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4hfxa" role="3clFbG">
                <property role="Xl_RC" value="1" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4hfxb" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4hfxc" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4hfxd" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4hfxe" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="Pq8sy4hfxf" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4hfxg" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4hfxh" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4hfxi" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfxj" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4hfxk" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4hfxm" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4hfxn" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4hfxe" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzqJv" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="Pq8sy4hgBC" role="37vLTx">
                  <property role="3cmrfH" value="1" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfxp" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4hfxq" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4hfxr" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4hfxs" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4hfxt" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4hfxe" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4hfIO" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4hfIP" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4hfIQ" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4hfIR" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4hfIS" role="3clFbG">
                <property role="Xl_RC" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4hfIT" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4hfIU" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4hfIV" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4hfIW" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="Pq8sy4hfIX" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4hfIY" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4hfIZ" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4hfJ0" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfJ1" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4hfJ2" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4hfJ4" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4hfJ5" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4hfIW" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzqKd" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="Pq8sy4hgN8" role="37vLTx">
                  <property role="3cmrfH" value="2" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4hfJ7" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4hfJ8" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4hfJ9" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4hfJa" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4hfJb" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4hfIW" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYt$" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYt_" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYtA" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYtB" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYtC" role="3clFbG">
                <property role="Xl_RC" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYtD" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYtE" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYtF" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYtG" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYtH" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYtI" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYtJ" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYtK" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYtL" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYtM" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYtN" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYtO" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYtG" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrAK" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVxZf0" role="37vLTx">
                  <property role="3cmrfH" value="3" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYtR" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYtS" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYtT" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYtU" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYtV" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYtG" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYxx" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYxy" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYxz" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYx$" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYx_" role="3clFbG">
                <property role="Xl_RC" value="4" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYxA" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYxB" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYxC" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYxD" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYxE" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYxF" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYxG" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYxH" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYxI" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYxJ" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYxK" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYxL" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYxD" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrDm" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVxZoR" role="37vLTx">
                  <property role="3cmrfH" value="4" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYxO" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYxP" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYxQ" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYxR" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYxS" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYxD" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYA7" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYA8" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYA9" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYAa" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYAb" role="3clFbG">
                <property role="Xl_RC" value="5" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYAc" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYAd" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYAe" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYAf" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYAg" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYAh" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYAi" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYAj" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYAk" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYAl" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYAm" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYAn" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYAf" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrE4" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVxZyI" role="37vLTx">
                  <property role="3cmrfH" value="5" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYAq" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYAr" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYAs" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYAt" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYAu" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYAf" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYFm" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYFn" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYFo" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYFp" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYFq" role="3clFbG">
                <property role="Xl_RC" value="6" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYFr" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYFs" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYFt" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYFu" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYFv" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYFw" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYFx" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYFy" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYFz" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYF$" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYF_" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYFA" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYFu" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrEM" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVxZN2" role="37vLTx">
                  <property role="3cmrfH" value="6" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYFD" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYFE" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYFF" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYFG" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYFH" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYFu" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYLe" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYLf" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYLg" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYLh" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYLi" role="3clFbG">
                <property role="Xl_RC" value="7" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYLj" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYLk" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYLl" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYLm" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYLn" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYLo" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYLp" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYLq" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYLr" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYLs" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYLt" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYLu" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYLm" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrFw" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVxZWT" role="37vLTx">
                  <property role="3cmrfH" value="7" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYLx" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYLy" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYLz" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYL$" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYL_" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYLm" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVxYYp" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVxYYq" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVxYYr" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVxYYs" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVxYYt" role="3clFbG">
                <property role="Xl_RC" value="8" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVxYYu" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVxYYv" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVxYYw" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVxYYx" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVxYYy" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVxYYz" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVxYY$" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVxYY_" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYYA" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVxYYB" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVxYYC" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVxYYD" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVxYYx" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrGv" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVy06K" role="37vLTx">
                  <property role="3cmrfH" value="8" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVxYYG" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVxYYH" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVxYYI" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVxYYJ" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVxYYK" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVxYYx" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="1UrpsVVy0fU" role="1Qtc8A">
        <node concept="1hCUdq" id="1UrpsVVy0fV" role="1hCUd6">
          <node concept="3clFbS" id="1UrpsVVy0fW" role="2VODD2">
            <node concept="3clFbF" id="1UrpsVVy0fX" role="3cqZAp">
              <node concept="Xl_RD" id="1UrpsVVy0fY" role="3clFbG">
                <property role="Xl_RC" value="9" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="1UrpsVVy0fZ" role="IWgqQ">
          <node concept="3clFbS" id="1UrpsVVy0g0" role="2VODD2">
            <node concept="3cpWs8" id="1UrpsVVy0g1" role="3cqZAp">
              <node concept="3cpWsn" id="1UrpsVVy0g2" role="3cpWs9">
                <property role="TrG5h" value="integer" />
                <node concept="3Tqbb2" id="1UrpsVVy0g3" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                </node>
                <node concept="2ShNRf" id="1UrpsVVy0g4" role="33vP2m">
                  <node concept="3zrR0B" id="1UrpsVVy0g5" role="2ShVmc">
                    <node concept="3Tqbb2" id="1UrpsVVy0g6" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVy0g7" role="3cqZAp">
              <node concept="37vLTI" id="1UrpsVVy0g8" role="3clFbG">
                <node concept="2OqwBi" id="1UrpsVVy0g9" role="37vLTJ">
                  <node concept="37vLTw" id="1UrpsVVy0ga" role="2Oq$k0">
                    <ref role="3cqZAo" node="1UrpsVVy0g2" resolve="integer" />
                  </node>
                  <node concept="3TrcHB" id="1UrpsVVzrJ5" role="2OqNvi">
                    <ref role="3TsBF5" to="jyas:1UrpsVVyWc$" resolve="value" />
                  </node>
                </node>
                <node concept="3cmrfG" id="1UrpsVVy0sF" role="37vLTx">
                  <property role="3cmrfH" value="9" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1UrpsVVy0gd" role="3cqZAp">
              <node concept="2OqwBi" id="1UrpsVVy0ge" role="3clFbG">
                <node concept="7Obwk" id="1UrpsVVy0gf" role="2Oq$k0" />
                <node concept="1P9Npp" id="1UrpsVVy0gg" role="2OqNvi">
                  <node concept="37vLTw" id="1UrpsVVy0gh" role="1P9ThW">
                    <ref role="3cqZAo" node="1UrpsVVy0g2" resolve="integer" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1IAO7e" id="1UrpsVVvFnF" role="1Qtc8A" />
      <node concept="3cWJ9i" id="1UrpsVV_k5e" role="1Qtc8$">
        <node concept="CtIbL" id="1UrpsVV_k5g" role="CtIbM">
          <property role="CtIbK" value="1A4kJjlVmVt/LEFT" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1UrpsVVwVgW">
    <ref role="1XX52x" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
    <node concept="3EZMnI" id="1UrpsVVwVh1" role="2wV5jI">
      <node concept="l2Vlx" id="1UrpsVVwVh4" role="2iSdaV" />
      <node concept="3F0A7n" id="1UrpsVVwVh8" role="3EZMnx">
        <ref role="1NtTu8" to="jyas:1UrpsVVyWc$" resolve="value" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1UrpsVV_k80">
    <ref role="1XX52x" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    <node concept="3EZMnI" id="1UrpsVV_Ouo" role="2wV5jI">
      <node concept="3F0ifn" id="1UrpsVV_Ouy" role="3EZMnx">
        <property role="ilYzB" value="&lt;time&gt;" />
        <node concept="VPM3Z" id="1UrpsVVA4Cj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VPxyj" id="1UrpsVVAkJA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="1UrpsVV_Our" role="2iSdaV" />
    </node>
  </node>
  <node concept="3p36aQ" id="1UrpsVVAX6e">
    <ref role="aqKnT" to="jyas:1UrpsVVz5W2" resolve="IncompleteTime" />
  </node>
  <node concept="3ICXOK" id="1yapuqB6wPm">
    <property role="TrG5h" value="Time_Right" />
    <ref role="aqKnT" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    <node concept="1Qtc8_" id="Pq8sy4fa2p" role="IW6Ez">
      <node concept="IWgqT" id="Pq8sy4fa2_" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fa2B" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fa2D" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fa7v" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fa7u" role="3clFbG">
                <property role="Xl_RC" value="+" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fa2F" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fa2H" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4faAy" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4faA_" role="3cpWs9">
                <property role="TrG5h" value="plus" />
                <node concept="3Tqbb2" id="Pq8sy4faAw" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUkcF" resolve="TimePlusOperation" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4faBK" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4faBI" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4faBJ" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUkcF" resolve="TimePlusOperation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4faEF" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fbft" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fiGr" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fbhK" role="2Oq$k0" />
                  <node concept="1$rogu" id="Pq8sy4fiTe" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4faO5" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4faED" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4faA_" resolve="plus" />
                  </node>
                  <node concept="3TrEf2" id="Pq8sy4fb6b" role="2OqNvi">
                    <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcG" resolve="left" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fbJD" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fbQX" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4fbJB" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4fbZX" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4fc25" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4faA_" resolve="plus" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fv5V" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fv5W" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fv5X" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fvaN" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fvaM" role="3clFbG">
                <property role="Xl_RC" value="-" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fv5Y" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fv5Z" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fvbV" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fvbW" role="3cpWs9">
                <property role="TrG5h" value="minus" />
                <node concept="3Tqbb2" id="Pq8sy4fvbX" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUkcL" resolve="TimeMinusOperation" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fvbY" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fvbZ" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fvc0" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUkcL" resolve="TimeMinusOperation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fvc1" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fvc2" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fvc3" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fvc4" role="2Oq$k0" />
                  <node concept="1$rogu" id="Pq8sy4fvc5" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4fvc6" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fvc7" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fvbW" resolve="minus" />
                  </node>
                  <node concept="3TrEf2" id="Pq8sy4fvYv" role="2OqNvi">
                    <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcG" resolve="left" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fvc9" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fvca" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4fvcb" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4fvcc" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4fvcd" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fvbW" resolve="minus" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1IAO7e" id="1UrpsVVwHrs" role="1Qtc8A" />
      <node concept="3cWJ9i" id="Pq8sy4fa2t" role="1Qtc8$">
        <node concept="CtIbL" id="Pq8sy4fa2v" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
    </node>
  </node>
  <node concept="3ICXOK" id="1yapuqB6$PJ">
    <property role="TrG5h" value="Time_Left" />
    <ref role="aqKnT" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    <node concept="1Qtc8_" id="1UrpsVVzYKJ" role="IW6Ez">
      <node concept="IWgqT" id="Pq8sy4fuMl" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fuMm" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fuMn" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fuRd" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fuRc" role="3clFbG">
                <property role="Xl_RC" value="+" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fuMo" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fuMp" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fuSl" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fuSm" role="3cpWs9">
                <property role="TrG5h" value="plus" />
                <node concept="3Tqbb2" id="Pq8sy4fuSn" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUkcF" resolve="TimePlusOperation" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fuSo" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fuSp" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fuSq" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUkcF" resolve="TimePlusOperation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fuSr" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fuSs" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fuSt" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fuSu" role="2Oq$k0" />
                  <node concept="1$rogu" id="Pq8sy4fuSv" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4fuSw" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fuSx" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fuSm" resolve="plus" />
                  </node>
                  <node concept="3TrEf2" id="Pq8sy4fuYZ" role="2OqNvi">
                    <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcI" resolve="right" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fuSz" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fuS$" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4fuS_" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4fuSA" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4fuSB" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fuSm" resolve="plus" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4fvsy" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4fvsz" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4fvs$" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4fvs_" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4fvsA" role="3clFbG">
                <property role="Xl_RC" value="-" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4fvsB" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4fvsC" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4fvsD" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4fvsE" role="3cpWs9">
                <property role="TrG5h" value="minus" />
                <node concept="3Tqbb2" id="Pq8sy4fvsF" role="1tU5fm">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUkcL" resolve="TimeMinusOperation" />
                </node>
                <node concept="2ShNRf" id="Pq8sy4fvsG" role="33vP2m">
                  <node concept="3zrR0B" id="Pq8sy4fvsH" role="2ShVmc">
                    <node concept="3Tqbb2" id="Pq8sy4fvsI" role="3zrR0E">
                      <ref role="ehGHo" to="jyas:5Q9IfCXUkcL" resolve="TimeMinusOperation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fvsJ" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4fvsK" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4fvsL" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4fvsM" role="2Oq$k0" />
                  <node concept="1$rogu" id="Pq8sy4fvsN" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4fvsO" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4fvsP" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4fvsE" resolve="minus" />
                  </node>
                  <node concept="3TrEf2" id="Pq8sy4fvsQ" role="2OqNvi">
                    <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcI" resolve="right" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4fvsR" role="3cqZAp">
              <node concept="2OqwBi" id="Pq8sy4fvsS" role="3clFbG">
                <node concept="7Obwk" id="Pq8sy4fvsT" role="2Oq$k0" />
                <node concept="1P9Npp" id="Pq8sy4fvsU" role="2OqNvi">
                  <node concept="37vLTw" id="Pq8sy4fvsV" role="1P9ThW">
                    <ref role="3cqZAo" node="Pq8sy4fvsE" resolve="minus" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cWJ9i" id="1UrpsVVzYTw" role="1Qtc8$">
        <node concept="CtIbL" id="1UrpsVVzYTy" role="CtIbM">
          <property role="CtIbK" value="1A4kJjlVmVt/LEFT" />
        </node>
      </node>
    </node>
  </node>
</model>

