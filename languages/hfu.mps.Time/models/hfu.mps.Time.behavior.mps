<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f0f8b0ef-50c3-4637-b45e-377aaf72262d(hfu.mps.Time.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="17" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="jyas" ref="r:55f07890-1eb3-4198-b15d-e4bd575dc4d1(hfu.mps.Time.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="1UrpsVVABw8">
    <ref role="13h7C2" to="jyas:5Q9IfCXUkcE" resolve="TimeBinaryOperation" />
    <node concept="13hLZK" id="1UrpsVVABw9" role="13h7CW">
      <node concept="3clFbS" id="1UrpsVVABwa" role="2VODD2">
        <node concept="3clFbF" id="1UrpsVVAB$0" role="3cqZAp">
          <node concept="37vLTI" id="1UrpsVVAC3d" role="3clFbG">
            <node concept="2ShNRf" id="1UrpsVVAC5$" role="37vLTx">
              <node concept="3zrR0B" id="1UrpsVVAC5y" role="2ShVmc">
                <node concept="3Tqbb2" id="1UrpsVVAC5z" role="3zrR0E">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1UrpsVVABGy" role="37vLTJ">
              <node concept="13iPFW" id="1UrpsVVABzZ" role="2Oq$k0" />
              <node concept="3TrEf2" id="1UrpsVVABQg" role="2OqNvi">
                <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcG" resolve="left" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1UrpsVVAC8H" role="3cqZAp">
          <node concept="37vLTI" id="1UrpsVVACA$" role="3clFbG">
            <node concept="2ShNRf" id="1UrpsVVACCS" role="37vLTx">
              <node concept="3zrR0B" id="1UrpsVVACBb" role="2ShVmc">
                <node concept="3Tqbb2" id="1UrpsVVACBc" role="3zrR0E">
                  <ref role="ehGHo" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1UrpsVVAChN" role="37vLTJ">
              <node concept="13iPFW" id="1UrpsVVAC8F" role="2Oq$k0" />
              <node concept="3TrEf2" id="1UrpsVVACrv" role="2OqNvi">
                <ref role="3Tt5mk" to="jyas:5Q9IfCXUkcI" resolve="right" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

