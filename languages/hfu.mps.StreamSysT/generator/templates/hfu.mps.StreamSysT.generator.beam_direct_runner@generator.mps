<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:288a3d84-6604-450f-bc7d-ad5ca87a9333(hfu.mps.StreamSysT.generator.beam_direct_runner@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="qyjc" ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)" />
    <import index="c43l" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk.options(hfu.mps.BeamLib/)" />
    <import index="fu0t" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.runners.direct(hfu.mps.BeamLib/)" />
    <import index="5ssx" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk(hfu.mps.BeamLib/)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="cqpf" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk.testing(hfu.mps.BeamLib/)" />
    <import index="54yz" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk.coders(hfu.mps.BeamLib/)" />
    <import index="w08f" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.joda.time(hfu.mps.BeamLib/)" />
    <import index="g9lr" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk.values(hfu.mps.BeamLib/)" />
    <import index="hqtk" ref="47ea545b-079c-437c-a69e-a4b00c2128f8/java:org.apache.beam.sdk.transforms.windowing(hfu.mps.BeamLib/)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="rjhg" ref="49808fad-9d41-4b96-83fa-9231640f6b2b/java:org.junit(JUnit/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="25xb" ref="r:e1747b17-3913-4d6b-b45c-af3ef3de533d(hfu.mps.StreamSysT.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
        <child id="1206060520071" name="elsifClauses" index="3eNLev" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1206060495898" name="jetbrains.mps.baseLanguage.structure.ElsifClause" flags="ng" index="3eNFk2">
        <child id="1206060619838" name="condition" index="3eO9$A" />
        <child id="1206060644605" name="statementList" index="3eOfB_" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1510949579266781519" name="jetbrains.mps.lang.generator.structure.TemplateCallMacro" flags="ln" index="5jKBG">
        <child id="1510949579266801461" name="sourceNodeQuery" index="5jGum" />
      </concept>
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1202776937179" name="jetbrains.mps.lang.generator.structure.AbandonInput_RuleConsequence" flags="lg" index="b5Tf3" />
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1200911492601" name="mappingLabel" index="2rTMjI" />
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1168559512253" name="jetbrains.mps.lang.generator.structure.DismissTopMappingRule" flags="lg" index="j$LIH">
        <child id="1169669152123" name="generatorMessage" index="1lHHLF" />
      </concept>
      <concept id="1112730859144" name="jetbrains.mps.lang.generator.structure.TemplateSwitch" flags="ig" index="jVnub">
        <child id="1168558750579" name="defaultConsequence" index="jxRDz" />
        <child id="1167340453568" name="reductionMappingRule" index="3aUrZf" />
      </concept>
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1200911316486" name="jetbrains.mps.lang.generator.structure.MappingLabelDeclaration" flags="lg" index="2rT7sh">
        <reference id="1200911342686" name="sourceConcept" index="2rTdP9" />
        <reference id="1200913004646" name="targetConcept" index="2rZz_L" />
      </concept>
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="5133195082121471908" name="jetbrains.mps.lang.generator.structure.LabelMacro" flags="ln" index="2ZBi8u" />
      <concept id="1167168920554" name="jetbrains.mps.lang.generator.structure.BaseMappingRule_Condition" flags="in" index="30G5F_" />
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <property id="1167272244852" name="applyToConceptInheritors" index="36QftV" />
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
        <child id="1167169362365" name="conditionFunction" index="30HLyM" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1195158154974" name="jetbrains.mps.lang.generator.structure.InlineSwitch_RuleConsequence" flags="lg" index="14YyZ8">
        <child id="1195158241124" name="defaultConsequence" index="14YRTM" />
        <child id="1195158408710" name="case" index="14ZwWg" />
      </concept>
      <concept id="1195158388553" name="jetbrains.mps.lang.generator.structure.InlineSwitch_Case" flags="ng" index="14ZrTv">
        <child id="1195158608805" name="conditionFunction" index="150hEN" />
        <child id="1195158637244" name="caseConsequence" index="150oIE" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1087833466690" name="jetbrains.mps.lang.generator.structure.NodeMacro" flags="lg" index="17VmuZ">
        <reference id="1200912223215" name="mappingLabel" index="2rW$FS" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1169670156577" name="jetbrains.mps.lang.generator.structure.GeneratorMessage" flags="lg" index="1lLz0L">
        <property id="1169670173015" name="messageText" index="1lLB17" />
        <property id="1169670356567" name="messageType" index="1lMjX7" />
      </concept>
      <concept id="982871510068000147" name="jetbrains.mps.lang.generator.structure.TemplateSwitchMacro" flags="lg" index="1sPUBX" />
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167770111131" name="jetbrains.mps.lang.generator.structure.ReferenceMacro_GetReferent" flags="in" index="3$xsQk" />
      <concept id="1167945743726" name="jetbrains.mps.lang.generator.structure.IfMacro_Condition" flags="in" index="3IZrLx" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="8900764248744213868" name="jetbrains.mps.lang.generator.structure.InlineTemplateWithContext_RuleConsequence" flags="lg" index="1Koe21">
        <child id="8900764248744213871" name="contentNode" index="1Koe22" />
      </concept>
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1118773211870" name="jetbrains.mps.lang.generator.structure.IfMacro" flags="ln" index="1W57fq">
        <child id="1194989344771" name="alternativeConsequence" index="UU_$l" />
        <child id="1167945861827" name="conditionFunction" index="3IZSJc" />
      </concept>
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
      <concept id="1088761943574" name="jetbrains.mps.lang.generator.structure.ReferenceMacro" flags="ln" index="1ZhdrF">
        <child id="1167770376702" name="referentFunction" index="3$ytzL" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1216860049627" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOutputByLabelAndInput" flags="nn" index="1iwH70">
        <reference id="1216860049628" name="label" index="1iwH77" />
        <child id="1216860049632" name="inputNode" index="1iwH7V" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="6332851714983831325" name="jetbrains.mps.baseLanguage.logging.structure.MsgStatement" flags="ng" index="2xdQw9">
        <property id="6332851714983843871" name="severity" index="2xdLsb" />
        <child id="5721587534047265374" name="message" index="9lYJi" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="role_DebugInfo" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1162934736510" name="jetbrains.mps.baseLanguage.collections.structure.GetElementOperation" flags="nn" index="34jXtK" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
    </language>
  </registry>
  <node concept="13MO4I" id="1CpVIWKV6_E">
    <property role="TrG5h" value="reduce_Pipeline" />
    <ref role="3gUMe" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
    <node concept="3clFbS" id="7MdWNyBgT3$" role="13RCb5">
      <node concept="3cpWs8" id="1CpVIWKUNgv" role="3cqZAp">
        <node concept="3cpWsn" id="1CpVIWKUNgw" role="3cpWs9">
          <property role="TrG5h" value="pipeline" />
          <node concept="2YIFZM" id="1CpVIWKUNgy" role="33vP2m">
            <ref role="1Pybhc" to="cqpf:~TestPipeline" resolve="TestPipeline" />
            <ref role="37wK5l" to="cqpf:~TestPipeline.create()" resolve="create" />
          </node>
          <node concept="17Uvod" id="1CpVIWKUNgI" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="1CpVIWKUNgJ" role="3zH0cK">
              <node concept="3clFbS" id="1CpVIWKUNgK" role="2VODD2">
                <node concept="3clFbF" id="1CpVIWKUNgL" role="3cqZAp">
                  <node concept="3cpWs3" id="6MwzKcXRQq$" role="3clFbG">
                    <node concept="Xl_RD" id="6MwzKcXRQqE" role="3uHU7w">
                      <property role="Xl_RC" value="_Pipeline" />
                    </node>
                    <node concept="2OqwBi" id="1CpVIWKUNgM" role="3uHU7B">
                      <node concept="3TrcHB" id="1CpVIWKUNgN" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                      </node>
                      <node concept="30H73N" id="1CpVIWKUNgO" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="1CpVIWKUNgP" role="lGtFl">
            <ref role="2rW$FS" node="5T7UbTDiT7z" resolve="PipelineRef" />
          </node>
          <node concept="3uibUv" id="67vTL6Xye9A" role="1tU5fm">
            <ref role="3uigEE" to="cqpf:~TestPipeline" resolve="TestPipeline" />
          </node>
        </node>
        <node concept="raruj" id="1CpVIWKV7KF" role="lGtFl" />
      </node>
      <node concept="3clFbH" id="67vTL6Xv3xB" role="3cqZAp" />
    </node>
  </node>
  <node concept="13MO4I" id="4o2stJ2hzZH">
    <property role="TrG5h" value="reduce_Assert" />
    <ref role="3gUMe" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
    <node concept="3clFbS" id="4o2stJ2hzZN" role="13RCb5">
      <node concept="3cpWs8" id="4o2stJ2h$03" role="3cqZAp">
        <node concept="3cpWsn" id="4o2stJ2h$04" role="3cpWs9">
          <property role="TrG5h" value="window" />
          <node concept="3uibUv" id="4o2stJ2h$05" role="1tU5fm">
            <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="4o2stJ2h$0x" role="3cqZAp">
        <node concept="3cpWsn" id="4o2stJ2h$0y" role="3cpWs9">
          <property role="TrG5h" value="sinkCollection" />
          <node concept="3uibUv" id="4o2stJ2h$0z" role="1tU5fm">
            <ref role="3uigEE" to="g9lr:~PCollection" resolve="PCollection" />
            <node concept="3uibUv" id="4o2stJ2h$AL" role="11_B2D">
              <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="4o2stJ2h$0O" role="3cqZAp">
        <node concept="2OqwBi" id="4o2stJ2h_9b" role="3clFbG">
          <node concept="2OqwBi" id="4o2stJ2h$qN" role="2Oq$k0">
            <node concept="2YIFZM" id="4o2stJ2h$1b" role="2Oq$k0">
              <ref role="1Pybhc" to="cqpf:~PAssert" resolve="PAssert" />
              <ref role="37wK5l" to="cqpf:~PAssert.that(java.lang.String,org.apache.beam.sdk.values.PCollection)" resolve="that" />
              <node concept="Xl_RD" id="4o2stJ2h$1I" role="37wK5m">
                <property role="Xl_RC" value="message" />
                <node concept="17Uvod" id="1SqhhvM3NPB" role="lGtFl">
                  <property role="2qtEX9" value="value" />
                  <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                  <node concept="3zFVjK" id="1SqhhvM3NPE" role="3zH0cK">
                    <node concept="3clFbS" id="1SqhhvM3NPF" role="2VODD2">
                      <node concept="3clFbF" id="1SqhhvM3NPL" role="3cqZAp">
                        <node concept="2OqwBi" id="1SqhhvM3NPG" role="3clFbG">
                          <node concept="3TrcHB" id="1SqhhvM3NPJ" role="2OqNvi">
                            <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                          </node>
                          <node concept="30H73N" id="1SqhhvM3NPK" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1W57fq" id="Pq8sy4aQem" role="lGtFl">
                  <node concept="3IZrLx" id="Pq8sy4aQen" role="3IZSJc">
                    <node concept="3clFbS" id="Pq8sy4aQeo" role="2VODD2">
                      <node concept="3clFbF" id="Pq8sy4aXxR" role="3cqZAp">
                        <node concept="2OqwBi" id="Pq8sy4aYOk" role="3clFbG">
                          <node concept="2OqwBi" id="Pq8sy4aXMv" role="2Oq$k0">
                            <node concept="30H73N" id="Pq8sy4aXxQ" role="2Oq$k0" />
                            <node concept="3TrcHB" id="Pq8sy4aY4e" role="2OqNvi">
                              <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                            </node>
                          </node>
                          <node concept="17RvpY" id="Pq8sy4aZ34" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="Pq8sy4j8EV" role="37wK5m">
                <ref role="3cqZAo" node="4o2stJ2h$0y" resolve="sinkCollection" />
                <node concept="29HgVG" id="Pq8sy4j8EW" role="lGtFl">
                  <node concept="3NFfHV" id="Pq8sy4j8EX" role="3NFExx">
                    <node concept="3clFbS" id="Pq8sy4j8EY" role="2VODD2">
                      <node concept="3clFbF" id="Pq8sy4j8EZ" role="3cqZAp">
                        <node concept="2OqwBi" id="Pq8sy4j8F0" role="3clFbG">
                          <node concept="30H73N" id="Pq8sy4j8F1" role="2Oq$k0" />
                          <node concept="3TrEf2" id="Pq8sy4j8F2" role="2OqNvi">
                            <ref role="3Tt5mk" to="qyjc:4vp2seWke22" resolve="sink" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="liA8E" id="4o2stJ2h$Se" role="2OqNvi">
              <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.inEarlyPane(org.apache.beam.sdk.transforms.windowing.BoundedWindow)" resolve="inEarlyPane" />
              <node concept="37vLTw" id="4o2stJ2h$Ug" role="37wK5m">
                <ref role="3cqZAo" node="4o2stJ2h$04" resolve="window" />
                <node concept="1ZhdrF" id="4o2stJ2hRt4" role="lGtFl">
                  <property role="2qtEX8" value="variableDeclaration" />
                  <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                  <node concept="3$xsQk" id="4o2stJ2hRt5" role="3$ytzL">
                    <node concept="3clFbS" id="4o2stJ2hRt6" role="2VODD2">
                      <node concept="3clFbF" id="4o2stJ2hRwA" role="3cqZAp">
                        <node concept="2OqwBi" id="4o2stJ2hSdp" role="3clFbG">
                          <node concept="1iwH7S" id="4o2stJ2hRZ_" role="2Oq$k0" />
                          <node concept="1iwH70" id="4o2stJ2hSiN" role="2OqNvi">
                            <ref role="1iwH77" node="4o2stJ24hGS" resolve="WindowRef" />
                            <node concept="2OqwBi" id="4vp2seWklGT" role="1iwH7V">
                              <node concept="2OqwBi" id="4o2stJ2hSA4" role="2Oq$k0">
                                <node concept="30H73N" id="4o2stJ2hSs1" role="2Oq$k0" />
                                <node concept="3TrEf2" id="4vp2seWkltB" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWke2c" resolve="window" />
                                </node>
                              </node>
                              <node concept="3TrEf2" id="4vp2seWklTq" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4vp2seWfjgK" resolve="ref" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1sPUBX" id="4o2stJ2iuRr" role="lGtFl">
                <ref role="v9R2y" node="4o2stJ2awe9" resolve="AssertionScope" />
              </node>
            </node>
          </node>
          <node concept="liA8E" id="4o2stJ2h_gd" role="2OqNvi">
            <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.containsInAnyOrder(java.lang.Object...)" resolve="containsInAnyOrder" />
            <node concept="10Nm6u" id="4o2stJ2h_pK" role="37wK5m" />
            <node concept="1sPUBX" id="4o2stJ2iLrJ" role="lGtFl">
              <ref role="v9R2y" node="4o2stJ29Emh" resolve="AssertionOrder" />
            </node>
          </node>
        </node>
        <node concept="raruj" id="4o2stJ2h_uD" role="lGtFl" />
        <node concept="1W57fq" id="7MdWNyBfGps" role="lGtFl">
          <node concept="3IZrLx" id="7MdWNyBfGpt" role="3IZSJc">
            <node concept="3clFbS" id="7MdWNyBfGpu" role="2VODD2">
              <node concept="3clFbF" id="7MdWNyBfGCY" role="3cqZAp">
                <node concept="3fqX7Q" id="7MdWNyBfHeQ" role="3clFbG">
                  <node concept="1eOMI4" id="7MdWNyBfHeS" role="3fr31v">
                    <node concept="2OqwBi" id="7MdWNyBfHeT" role="1eOMHV">
                      <node concept="30H73N" id="7MdWNyBfHeU" role="2Oq$k0" />
                      <node concept="3TrcHB" id="7MdWNyBfHeV" role="2OqNvi">
                        <ref role="3TsBF5" to="qyjc:7MdWNyBf7G4" resolve="applyFunction" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="gft3U" id="7MdWNyBfHjo" role="UU_$l">
            <node concept="3clFbF" id="7MdWNyBfH$N" role="gfFT$">
              <node concept="2OqwBi" id="7MdWNyBfIh_" role="3clFbG">
                <node concept="2YIFZM" id="7MdWNyBfH_a" role="2Oq$k0">
                  <ref role="1Pybhc" to="cqpf:~PAssert" resolve="PAssert" />
                  <ref role="37wK5l" to="cqpf:~PAssert.that(java.lang.String,org.apache.beam.sdk.values.PCollection)" resolve="that" />
                  <node concept="Xl_RD" id="7MdWNyBfH_W" role="37wK5m">
                    <property role="Xl_RC" value="message" />
                    <node concept="17Uvod" id="7MdWNyBfH_X" role="lGtFl">
                      <property role="2qtEX9" value="value" />
                      <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                      <node concept="3zFVjK" id="7MdWNyBfH_Y" role="3zH0cK">
                        <node concept="3clFbS" id="7MdWNyBfH_Z" role="2VODD2">
                          <node concept="3clFbF" id="7MdWNyBfHA0" role="3cqZAp">
                            <node concept="2OqwBi" id="7MdWNyBfHA1" role="3clFbG">
                              <node concept="3TrcHB" id="7MdWNyBfHA2" role="2OqNvi">
                                <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                              </node>
                              <node concept="30H73N" id="7MdWNyBfHA3" role="2Oq$k0" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1W57fq" id="Pq8sy4bTQU" role="lGtFl">
                      <node concept="3IZrLx" id="Pq8sy4bTQV" role="3IZSJc">
                        <node concept="3clFbS" id="Pq8sy4bTQW" role="2VODD2">
                          <node concept="3clFbF" id="Pq8sy4bTYo" role="3cqZAp">
                            <node concept="2OqwBi" id="Pq8sy4bURD" role="3clFbG">
                              <node concept="2OqwBi" id="Pq8sy4bUeY" role="2Oq$k0">
                                <node concept="30H73N" id="Pq8sy4bTYn" role="2Oq$k0" />
                                <node concept="3TrcHB" id="Pq8sy4bUx4" role="2OqNvi">
                                  <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                                </node>
                              </node>
                              <node concept="17RvpY" id="Pq8sy4bV9y" role="2OqNvi" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="7MdWNyBfHXt" role="37wK5m">
                    <ref role="3cqZAo" node="4o2stJ2h$0y" resolve="sinkCollection" />
                    <node concept="29HgVG" id="Pq8sy4j7i2" role="lGtFl">
                      <node concept="3NFfHV" id="Pq8sy4j7me" role="3NFExx">
                        <node concept="3clFbS" id="Pq8sy4j7mf" role="2VODD2">
                          <node concept="3clFbF" id="Pq8sy4j7EC" role="3cqZAp">
                            <node concept="2OqwBi" id="Pq8sy4j7Ri" role="3clFbG">
                              <node concept="30H73N" id="Pq8sy4j7EB" role="2Oq$k0" />
                              <node concept="3TrEf2" id="Pq8sy4j85y" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4vp2seWke22" resolve="sink" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="7MdWNyBfIx_" role="2OqNvi">
                  <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.satisfies(org.apache.beam.sdk.transforms.SerializableFunction)" resolve="satisfies" />
                  <node concept="10Nm6u" id="7MdWNyBgs2G" role="37wK5m">
                    <node concept="29HgVG" id="7MdWNyBgs9O" role="lGtFl">
                      <node concept="3NFfHV" id="7MdWNyBgske" role="3NFExx">
                        <node concept="3clFbS" id="7MdWNyBgskf" role="2VODD2">
                          <node concept="3clFbF" id="7MdWNyBgssy" role="3cqZAp">
                            <node concept="2OqwBi" id="7MdWNyBgs_F" role="3clFbG">
                              <node concept="30H73N" id="7MdWNyBgssx" role="2Oq$k0" />
                              <node concept="3TrEf2" id="7MdWNyBgsGO" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:7MdWNyBf7FZ" resolve="satisfiesFunc" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1W57fq" id="4aABesdx3Ja" role="lGtFl">
          <node concept="3IZrLx" id="4aABesdx3Jd" role="3IZSJc">
            <node concept="3clFbS" id="4aABesdx3Je" role="2VODD2">
              <node concept="3clFbF" id="4aABesdx3Jk" role="3cqZAp">
                <node concept="2OqwBi" id="4aABesdx9aQ" role="3clFbG">
                  <node concept="2OqwBi" id="4aABesdx3Jf" role="2Oq$k0">
                    <node concept="30H73N" id="4aABesdx3Jj" role="2Oq$k0" />
                    <node concept="3TrcHB" id="4aABesdx4dY" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                    </node>
                  </node>
                  <node concept="3x8VRR" id="4aABesdx9ip" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="gft3U" id="4aABesdxdKf" role="UU_$l">
            <node concept="3clFbF" id="4aABesdxjzp" role="gfFT$">
              <node concept="2OqwBi" id="4aABesdxk2h" role="3clFbG">
                <node concept="2YIFZM" id="4aABesdxj$_" role="2Oq$k0">
                  <ref role="1Pybhc" to="cqpf:~PAssert" resolve="PAssert" />
                  <ref role="37wK5l" to="cqpf:~PAssert.that(java.lang.String,org.apache.beam.sdk.values.PCollection)" resolve="that" />
                  <node concept="Xl_RD" id="4aABesdxj$A" role="37wK5m">
                    <property role="Xl_RC" value="message" />
                    <node concept="17Uvod" id="4aABesdxj$B" role="lGtFl">
                      <property role="2qtEX9" value="value" />
                      <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                      <node concept="3zFVjK" id="4aABesdxj$C" role="3zH0cK">
                        <node concept="3clFbS" id="4aABesdxj$D" role="2VODD2">
                          <node concept="3clFbF" id="4aABesdxj$E" role="3cqZAp">
                            <node concept="2OqwBi" id="4aABesdxj$F" role="3clFbG">
                              <node concept="3TrcHB" id="4aABesdxj$G" role="2OqNvi">
                                <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                              </node>
                              <node concept="30H73N" id="4aABesdxj$H" role="2Oq$k0" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="1W57fq" id="Pq8sy4bSdb" role="lGtFl">
                      <node concept="3IZrLx" id="Pq8sy4bSdc" role="3IZSJc">
                        <node concept="3clFbS" id="Pq8sy4bSdd" role="2VODD2">
                          <node concept="3clFbF" id="Pq8sy4bSkH" role="3cqZAp">
                            <node concept="2OqwBi" id="Pq8sy4bThx" role="3clFbG">
                              <node concept="2OqwBi" id="Pq8sy4bS_j" role="2Oq$k0">
                                <node concept="30H73N" id="Pq8sy4bSkG" role="2Oq$k0" />
                                <node concept="3TrcHB" id="Pq8sy4bSRp" role="2OqNvi">
                                  <ref role="3TsBF5" to="qyjc:4o2stJ28Y9V" resolve="description" />
                                </node>
                              </node>
                              <node concept="17RvpY" id="Pq8sy4bTzq" role="2OqNvi" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="37vLTw" id="Pq8sy4j88d" role="37wK5m">
                    <ref role="3cqZAo" node="4o2stJ2h$0y" resolve="sinkCollection" />
                    <node concept="29HgVG" id="Pq8sy4j88e" role="lGtFl">
                      <node concept="3NFfHV" id="Pq8sy4j88f" role="3NFExx">
                        <node concept="3clFbS" id="Pq8sy4j88g" role="2VODD2">
                          <node concept="3clFbF" id="Pq8sy4j88h" role="3cqZAp">
                            <node concept="2OqwBi" id="Pq8sy4j88i" role="3clFbG">
                              <node concept="30H73N" id="Pq8sy4j88j" role="2Oq$k0" />
                              <node concept="3TrEf2" id="Pq8sy4j88k" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4vp2seWke22" resolve="sink" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="4aABesdxk_e" role="2OqNvi">
                  <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.containsInAnyOrder(java.lang.Object...)" resolve="containsInAnyOrder" />
                  <node concept="10Nm6u" id="4aABesdxk_f" role="37wK5m" />
                  <node concept="1sPUBX" id="4aABesdxk_g" role="lGtFl">
                    <ref role="v9R2y" node="4o2stJ29Emh" resolve="AssertionOrder" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="jVnub" id="1CpVIWKTjvh">
    <property role="TrG5h" value="PipeProcess" />
    <property role="3GE5qa" value="PipeProcess" />
    <node concept="3aamgX" id="RoIJyc3sxl" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:1CpVIWKKzQq" resolve="AdvanceWatermark" />
      <node concept="14YyZ8" id="QA4204DsXr" role="1lVwrX">
        <node concept="14ZrTv" id="QA4204DsX$" role="14ZwWg">
          <node concept="30G5F_" id="QA4204DsX_" role="150hEN">
            <node concept="3clFbS" id="QA4204DsXA" role="2VODD2">
              <node concept="3clFbF" id="QA4204DtsE" role="3cqZAp">
                <node concept="2OqwBi" id="QA4204DtZX" role="3clFbG">
                  <node concept="2OqwBi" id="QA4204DtEp" role="2Oq$k0">
                    <node concept="30H73N" id="QA4204DtsD" role="2Oq$k0" />
                    <node concept="3TrcHB" id="QA4204DtMT" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:QA4204BsDk" resolve="advanceOption" />
                    </node>
                  </node>
                  <node concept="21noJN" id="QA4204Du7w" role="2OqNvi">
                    <node concept="21nZrQ" id="QA4204DubQ" role="21noJM">
                      <ref role="21nZrZ" to="qyjc:QA4204BsD0" resolve="to" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1Koe21" id="4o2stJ24cM5" role="150oIE">
            <node concept="3clFbS" id="4o2stJ24cM9" role="1Koe22">
              <node concept="3cpWs8" id="4o2stJ24cQ3" role="3cqZAp">
                <node concept="3cpWsn" id="4o2stJ24cQ4" role="3cpWs9">
                  <property role="TrG5h" value="source" />
                  <node concept="3uibUv" id="4o2stJ24cQ5" role="1tU5fm">
                    <ref role="3uigEE" to="cqpf:~TestStream$Builder" resolve="TestStream.Builder" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs8" id="4o2stJ24deG" role="3cqZAp">
                <node concept="3cpWsn" id="4o2stJ24deH" role="3cpWs9">
                  <property role="TrG5h" value="now" />
                  <node concept="3uibUv" id="4o2stJ24deI" role="1tU5fm">
                    <ref role="3uigEE" to="w08f:~Instant" resolve="Instant" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="1SqhhvLQqST" role="3cqZAp">
                <node concept="37vLTI" id="1SqhhvLQroC" role="3clFbG">
                  <node concept="37vLTw" id="1SqhhvLQqSV" role="37vLTJ">
                    <ref role="3cqZAo" node="4o2stJ24cQ4" resolve="source" />
                    <node concept="1ZhdrF" id="1SqhhvLQqSW" role="lGtFl">
                      <property role="2qtEX8" value="variableDeclaration" />
                      <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                      <node concept="3$xsQk" id="1SqhhvLQqSX" role="3$ytzL">
                        <node concept="3clFbS" id="1SqhhvLQqSY" role="2VODD2">
                          <node concept="3clFbF" id="1SqhhvLQqSZ" role="3cqZAp">
                            <node concept="2OqwBi" id="1SqhhvLQqT0" role="3clFbG">
                              <node concept="1iwH7S" id="1SqhhvLQqT1" role="2Oq$k0" />
                              <node concept="1iwH70" id="1SqhhvLQqT2" role="2OqNvi">
                                <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                                <node concept="2OqwBi" id="1SqhhvLQqT3" role="1iwH7V">
                                  <node concept="30H73N" id="1SqhhvLQqT4" role="2Oq$k0" />
                                  <node concept="2Xjw5R" id="1SqhhvLQqT5" role="2OqNvi">
                                    <node concept="1xMEDy" id="1SqhhvLQqT6" role="1xVPHs">
                                      <node concept="chp4Y" id="1SqhhvLQqT7" role="ri$Ld">
                                        <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1SqhhvLQrqJ" role="37vLTx">
                    <node concept="37vLTw" id="1SqhhvLQrqK" role="2Oq$k0">
                      <ref role="3cqZAo" node="4o2stJ24cQ4" resolve="source" />
                      <node concept="1ZhdrF" id="1SqhhvLQrqL" role="lGtFl">
                        <property role="2qtEX8" value="variableDeclaration" />
                        <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                        <node concept="3$xsQk" id="1SqhhvLQrqM" role="3$ytzL">
                          <node concept="3clFbS" id="1SqhhvLQrqN" role="2VODD2">
                            <node concept="3clFbF" id="1SqhhvLQrqO" role="3cqZAp">
                              <node concept="2OqwBi" id="1SqhhvLQrqP" role="3clFbG">
                                <node concept="1iwH7S" id="1SqhhvLQrqQ" role="2Oq$k0" />
                                <node concept="1iwH70" id="1SqhhvLQrqR" role="2OqNvi">
                                  <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                                  <node concept="2OqwBi" id="1SqhhvLQrqS" role="1iwH7V">
                                    <node concept="30H73N" id="1SqhhvLQrqT" role="2Oq$k0" />
                                    <node concept="2Xjw5R" id="1SqhhvLQrqU" role="2OqNvi">
                                      <node concept="1xMEDy" id="1SqhhvLQrqV" role="1xVPHs">
                                        <node concept="chp4Y" id="1SqhhvLQrqW" role="ri$Ld">
                                          <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="1SqhhvLQrqX" role="2OqNvi">
                      <ref role="37wK5l" to="cqpf:~TestStream$Builder.advanceWatermarkTo(org.joda.time.Instant)" resolve="advanceWatermarkTo" />
                      <node concept="2OqwBi" id="Jw6Yu7Lguw" role="37wK5m">
                        <node concept="2ShNRf" id="Jw6Yu7LfMA" role="2Oq$k0">
                          <node concept="1pGfFk" id="Jw6Yu7Lg8u" role="2ShVmc">
                            <ref role="37wK5l" to="w08f:~Instant.&lt;init&gt;(long)" resolve="Instant" />
                            <node concept="3cmrfG" id="Jw6Yu7LkBB" role="37wK5m">
                              <property role="3cmrfH" value="0" />
                            </node>
                          </node>
                        </node>
                        <node concept="liA8E" id="Jw6Yu7LgOh" role="2OqNvi">
                          <ref role="37wK5l" to="w08f:~Instant.plus(org.joda.time.ReadableDuration)" resolve="plus" />
                          <node concept="10M0yZ" id="Jw6Yu7Lh1Z" role="37wK5m">
                            <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
                            <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
                            <node concept="29HgVG" id="Jw6Yu7Lhj0" role="lGtFl">
                              <node concept="3NFfHV" id="Jw6Yu7Lhj1" role="3NFExx">
                                <node concept="3clFbS" id="Jw6Yu7Lhj2" role="2VODD2">
                                  <node concept="3clFbF" id="Jw6Yu7Lhj8" role="3cqZAp">
                                    <node concept="2OqwBi" id="Jw6Yu7Lhj3" role="3clFbG">
                                      <node concept="3TrEf2" id="Jw6Yu7Lhj6" role="2OqNvi">
                                        <ref role="3Tt5mk" to="qyjc:4o2stJ22OSx" resolve="time" />
                                      </node>
                                      <node concept="30H73N" id="Jw6Yu7Lhj7" role="2Oq$k0" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="1SqhhvLQrD_" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="j$LIH" id="QA4204DsXv" role="14YRTM">
          <node concept="1lLz0L" id="QA4204DsXy" role="1lHHLF">
            <property role="1lMjX7" value="h1lM37o/error" />
            <property role="1lLB17" value="Unknown state of advance watermark" />
          </node>
        </node>
        <node concept="14ZrTv" id="QA4204DuFh" role="14ZwWg">
          <node concept="30G5F_" id="QA4204DuFi" role="150hEN">
            <node concept="3clFbS" id="QA4204DuFj" role="2VODD2">
              <node concept="3clFbF" id="QA4204DuR6" role="3cqZAp">
                <node concept="2OqwBi" id="QA4204Dvs2" role="3clFbG">
                  <node concept="2OqwBi" id="QA4204Dv4$" role="2Oq$k0">
                    <node concept="30H73N" id="QA4204DuR5" role="2Oq$k0" />
                    <node concept="3TrcHB" id="QA4204DvgF" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:QA4204BsDk" resolve="advanceOption" />
                    </node>
                  </node>
                  <node concept="21noJN" id="QA4204Dvz_" role="2OqNvi">
                    <node concept="21nZrQ" id="QA4204DvGe" role="21noJM">
                      <ref role="21nZrZ" to="qyjc:QA4204BsCZ" resolve="by" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="j$LIH" id="QA4204DuQY" role="150oIE">
            <node concept="1lLz0L" id="QA4204DuR3" role="1lHHLF">
              <property role="1lMjX7" value="h1lM37o/error" />
              <property role="1lLB17" value="Relative watermark advance is not supported in apache beam" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="RoIJyc3MbO" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:1CpVIWKKpFP" resolve="Event" />
      <node concept="1Koe21" id="78izV9gVOpk" role="1lVwrX">
        <node concept="3clFbS" id="78izV9gVOpl" role="1Koe22">
          <node concept="3cpWs8" id="78izV9gVOpm" role="3cqZAp">
            <node concept="3cpWsn" id="78izV9gVOpn" role="3cpWs9">
              <property role="TrG5h" value="source" />
              <node concept="3uibUv" id="78izV9gVOpo" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~TestStream$Builder" resolve="TestStream.Builder" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="78izV9gWL7o" role="3cqZAp">
            <node concept="37vLTI" id="78izV9gWLee" role="3clFbG">
              <node concept="2OqwBi" id="78izV9gWLf_" role="37vLTx">
                <node concept="37vLTw" id="78izV9gWLfA" role="2Oq$k0">
                  <ref role="3cqZAo" node="78izV9gVOpn" resolve="source" />
                  <node concept="1ZhdrF" id="78izV9gWLfB" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="78izV9gWLfC" role="3$ytzL">
                      <node concept="3clFbS" id="78izV9gWLfD" role="2VODD2">
                        <node concept="3clFbF" id="78izV9gWLfE" role="3cqZAp">
                          <node concept="2OqwBi" id="78izV9gWLfF" role="3clFbG">
                            <node concept="1iwH7S" id="78izV9gWLfG" role="2Oq$k0" />
                            <node concept="1iwH70" id="78izV9gWLfH" role="2OqNvi">
                              <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                              <node concept="2OqwBi" id="78izV9gWLfI" role="1iwH7V">
                                <node concept="30H73N" id="78izV9gWLfJ" role="2Oq$k0" />
                                <node concept="2Xjw5R" id="78izV9gWLfK" role="2OqNvi">
                                  <node concept="1xMEDy" id="78izV9gWLfL" role="1xVPHs">
                                    <node concept="chp4Y" id="78izV9gWLfM" role="ri$Ld">
                                      <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="78izV9gWLfN" role="2OqNvi">
                  <ref role="37wK5l" to="cqpf:~TestStream$Builder.addElements(java.lang.Object,java.lang.Object...)" resolve="addElements" />
                  <node concept="10Nm6u" id="78izV9gWLfO" role="37wK5m">
                    <node concept="29HgVG" id="78izV9gWLfP" role="lGtFl">
                      <node concept="3NFfHV" id="78izV9gWLfQ" role="3NFExx">
                        <node concept="3clFbS" id="78izV9gWLfR" role="2VODD2">
                          <node concept="3clFbF" id="78izV9gWLfS" role="3cqZAp">
                            <node concept="2OqwBi" id="78izV9gWLfT" role="3clFbG">
                              <node concept="3TrEf2" id="78izV9gWLfU" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:1CpVIWKNoWl" resolve="creation" />
                              </node>
                              <node concept="30H73N" id="78izV9gWLfV" role="2Oq$k0" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="78izV9gWW7j" role="37vLTJ">
                <ref role="3cqZAo" node="78izV9gVOpn" resolve="source" />
                <node concept="1ZhdrF" id="78izV9gWW7k" role="lGtFl">
                  <property role="2qtEX8" value="variableDeclaration" />
                  <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                  <node concept="3$xsQk" id="78izV9gWW7l" role="3$ytzL">
                    <node concept="3clFbS" id="78izV9gWW7m" role="2VODD2">
                      <node concept="3clFbF" id="78izV9gWW7n" role="3cqZAp">
                        <node concept="2OqwBi" id="78izV9gWW7o" role="3clFbG">
                          <node concept="1iwH7S" id="78izV9gWW7p" role="2Oq$k0" />
                          <node concept="1iwH70" id="78izV9gWW7q" role="2OqNvi">
                            <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                            <node concept="2OqwBi" id="78izV9gWW7r" role="1iwH7V">
                              <node concept="30H73N" id="78izV9gWW7s" role="2Oq$k0" />
                              <node concept="2Xjw5R" id="78izV9gWW7t" role="2OqNvi">
                                <node concept="1xMEDy" id="78izV9gWW7u" role="1xVPHs">
                                  <node concept="chp4Y" id="78izV9gWW7v" role="ri$Ld">
                                    <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="raruj" id="78izV9gWLvU" role="lGtFl" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="QA4204Ad5U" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:QA4204A5TJ" resolve="AdvanceProcessingTime" />
      <node concept="14YyZ8" id="QA4204CKHO" role="1lVwrX">
        <node concept="14ZrTv" id="QA4204CLs_" role="14ZwWg">
          <node concept="30G5F_" id="QA4204CLsA" role="150hEN">
            <node concept="3clFbS" id="QA4204CLsB" role="2VODD2">
              <node concept="3clFbF" id="QA4204CLt3" role="3cqZAp">
                <node concept="2OqwBi" id="QA4204CLWK" role="3clFbG">
                  <node concept="2OqwBi" id="QA4204CLDJ" role="2Oq$k0">
                    <node concept="30H73N" id="QA4204CLt2" role="2Oq$k0" />
                    <node concept="3TrcHB" id="QA4204CLOB" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:QA4204BsD3" resolve="advanceOption" />
                    </node>
                  </node>
                  <node concept="21noJN" id="QA4204CM3I" role="2OqNvi">
                    <node concept="21nZrQ" id="QA4204Dugc" role="21noJM">
                      <ref role="21nZrZ" to="qyjc:QA4204BsCZ" resolve="by" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1Koe21" id="QA4204CMfF" role="150oIE">
            <node concept="3clFbS" id="QA4204CMfG" role="1Koe22">
              <node concept="3cpWs8" id="QA4204CMfH" role="3cqZAp">
                <node concept="3cpWsn" id="QA4204CMfI" role="3cpWs9">
                  <property role="TrG5h" value="source" />
                  <node concept="3uibUv" id="QA4204CMfJ" role="1tU5fm">
                    <ref role="3uigEE" to="cqpf:~TestStream$Builder" resolve="TestStream.Builder" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="QA4204CMfN" role="3cqZAp">
                <node concept="37vLTI" id="QA4204CMfO" role="3clFbG">
                  <node concept="37vLTw" id="QA4204CMfP" role="37vLTJ">
                    <ref role="3cqZAo" node="QA4204CMfI" resolve="source" />
                    <node concept="1ZhdrF" id="QA4204CMfQ" role="lGtFl">
                      <property role="2qtEX8" value="variableDeclaration" />
                      <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                      <node concept="3$xsQk" id="QA4204CMfR" role="3$ytzL">
                        <node concept="3clFbS" id="QA4204CMfS" role="2VODD2">
                          <node concept="3clFbF" id="QA4204CMfT" role="3cqZAp">
                            <node concept="2OqwBi" id="QA4204CMfU" role="3clFbG">
                              <node concept="1iwH7S" id="QA4204CMfV" role="2Oq$k0" />
                              <node concept="1iwH70" id="QA4204CMfW" role="2OqNvi">
                                <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                                <node concept="2OqwBi" id="QA4204CMfX" role="1iwH7V">
                                  <node concept="30H73N" id="QA4204CMfY" role="2Oq$k0" />
                                  <node concept="2Xjw5R" id="QA4204CMfZ" role="2OqNvi">
                                    <node concept="1xMEDy" id="QA4204CMg0" role="1xVPHs">
                                      <node concept="chp4Y" id="QA4204CMg1" role="ri$Ld">
                                        <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="QA4204CMg2" role="37vLTx">
                    <node concept="37vLTw" id="QA4204CMg3" role="2Oq$k0">
                      <ref role="3cqZAo" node="QA4204CMfI" resolve="source" />
                      <node concept="1ZhdrF" id="QA4204CMg4" role="lGtFl">
                        <property role="2qtEX8" value="variableDeclaration" />
                        <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                        <node concept="3$xsQk" id="QA4204CMg5" role="3$ytzL">
                          <node concept="3clFbS" id="QA4204CMg6" role="2VODD2">
                            <node concept="3clFbF" id="QA4204CMg7" role="3cqZAp">
                              <node concept="2OqwBi" id="QA4204CMg8" role="3clFbG">
                                <node concept="1iwH7S" id="QA4204CMg9" role="2Oq$k0" />
                                <node concept="1iwH70" id="QA4204CMga" role="2OqNvi">
                                  <ref role="1iwH77" node="1CpVIWKTt2l" resolve="SourceRef" />
                                  <node concept="2OqwBi" id="QA4204CMgb" role="1iwH7V">
                                    <node concept="30H73N" id="QA4204CMgc" role="2Oq$k0" />
                                    <node concept="2Xjw5R" id="QA4204CMgd" role="2OqNvi">
                                      <node concept="1xMEDy" id="QA4204CMge" role="1xVPHs">
                                        <node concept="chp4Y" id="QA4204CMgf" role="ri$Ld">
                                          <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                                        </node>
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="QA4204CMgg" role="2OqNvi">
                      <ref role="37wK5l" to="cqpf:~TestStream$Builder.advanceProcessingTime(org.joda.time.Duration)" resolve="advanceProcessingTime" />
                      <node concept="10M0yZ" id="QA4204CVVj" role="37wK5m">
                        <ref role="3cqZAo" to="w08f:~Duration.ZERO" resolve="ZERO" />
                        <ref role="1PxDUh" to="w08f:~Duration" resolve="Duration" />
                        <node concept="29HgVG" id="6$tYh_AgqmX" role="lGtFl">
                          <node concept="3NFfHV" id="6$tYh_Agqqz" role="3NFExx">
                            <node concept="3clFbS" id="6$tYh_Agqq$" role="2VODD2">
                              <node concept="3clFbF" id="6$tYh_AgqJS" role="3cqZAp">
                                <node concept="2OqwBi" id="6$tYh_AgqTj" role="3clFbG">
                                  <node concept="30H73N" id="6$tYh_AgqJR" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="6$tYh_Agr0N" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:QA4204A5TK" resolve="time" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="QA4204CMgD" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
        <node concept="14ZrTv" id="QA4204CN0a" role="14ZwWg">
          <node concept="30G5F_" id="QA4204CN0b" role="150hEN">
            <node concept="3clFbS" id="QA4204CN0c" role="2VODD2">
              <node concept="3clFbF" id="QA4204CNi2" role="3cqZAp">
                <node concept="2OqwBi" id="QA4204CNKL" role="3clFbG">
                  <node concept="2OqwBi" id="QA4204CNuI" role="2Oq$k0">
                    <node concept="30H73N" id="QA4204CNi1" role="2Oq$k0" />
                    <node concept="3TrcHB" id="QA4204CN_R" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:QA4204BsD3" resolve="advanceOption" />
                    </node>
                  </node>
                  <node concept="21noJN" id="QA4204CNRJ" role="2OqNvi">
                    <node concept="21nZrQ" id="QA4204DuiK" role="21noJM">
                      <ref role="21nZrZ" to="qyjc:QA4204BsD0" resolve="to" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="j$LIH" id="QA4204CO0o" role="150oIE">
            <node concept="1lLz0L" id="QA4204CO0Z" role="1lHHLF">
              <property role="1lMjX7" value="h1lM37o/error" />
              <property role="1lLB17" value="Absolute advance of processing time is not supported in apache beam" />
            </node>
          </node>
        </node>
        <node concept="j$LIH" id="QA4204CSP7" role="14YRTM">
          <node concept="1lLz0L" id="QA4204CSWi" role="1lHHLF">
            <property role="1lMjX7" value="h1lM37o/error" />
            <property role="1lLB17" value="Unkown state of advance processing time" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="bUwia" id="5T7UbTDhtf_">
    <property role="TrG5h" value="beam_direct_runner" />
    <node concept="2rT7sh" id="5T7UbTDixNf" role="2rTMjI">
      <property role="TrG5h" value="OptionsRef" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
      <ref role="2rTdP9" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
    </node>
    <node concept="2rT7sh" id="5T7UbTDiT7z" role="2rTMjI">
      <property role="TrG5h" value="PipelineRef" />
      <ref role="2rTdP9" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
    </node>
    <node concept="2rT7sh" id="1CpVIWKTt2l" role="2rTMjI">
      <property role="TrG5h" value="SourceRef" />
      <ref role="2rTdP9" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
    </node>
    <node concept="3aamgX" id="1CpVIWKV6lW" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
      <node concept="j$656" id="1CpVIWKV6_G" role="1lVwrX">
        <ref role="v9R2y" node="1CpVIWKV6_E" resolve="reduce_Pipeline" />
      </node>
    </node>
    <node concept="2rT7sh" id="78izV9gWuw_" role="2rTMjI">
      <property role="TrG5h" value="InputRef" />
      <ref role="2rTdP9" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
    </node>
    <node concept="3aamgX" id="78izV9gX8CV" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
      <node concept="j$656" id="78izV9gX8CW" role="1lVwrX">
        <ref role="v9R2y" node="78izV9gX8CT" resolve="reduce_EventSource" />
      </node>
    </node>
    <node concept="2rT7sh" id="4o2stJ1YWa9" role="2rTMjI">
      <property role="TrG5h" value="InputCollectionRef" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
      <ref role="2rTdP9" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
    </node>
    <node concept="2rT7sh" id="4o2stJ22OtC" role="2rTMjI">
      <property role="TrG5h" value="SinkRef" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
      <ref role="2rTdP9" to="qyjc:1CpVIWKKpwu" resolve="EventSink" />
    </node>
    <node concept="3aamgX" id="4o2stJ243sy" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:4o2stJ22OSe" resolve="Window" />
      <node concept="j$656" id="4o2stJ243sz" role="1lVwrX">
        <ref role="v9R2y" node="4o2stJ243sw" resolve="reduce_Window" />
      </node>
    </node>
    <node concept="2rT7sh" id="4o2stJ24hGS" role="2rTMjI">
      <property role="TrG5h" value="WindowRef" />
      <ref role="2rTdP9" to="qyjc:4o2stJ22OSe" resolve="Window" />
      <ref role="2rZz_L" to="tpee:fz3uBXI" resolve="VariableDeclaration" />
    </node>
    <node concept="3aamgX" id="4o2stJ28XVS" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="j$656" id="4o2stJ2hzZJ" role="1lVwrX">
        <ref role="v9R2y" node="4o2stJ2hzZH" resolve="reduce_Assert" />
      </node>
    </node>
    <node concept="3aamgX" id="4o2stJ2geKo" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:1CpVIWKKpwu" resolve="EventSink" />
      <node concept="j$656" id="4o2stJ2geKp" role="1lVwrX">
        <ref role="v9R2y" node="4o2stJ2geKm" resolve="reduce_EventSink" />
      </node>
    </node>
    <node concept="3aamgX" id="5$bixu1d_XR" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:5$bixu1d_8P" resolve="ExpectedEntry" />
      <node concept="gft3U" id="5$bixu1d_XP" role="1lVwrX">
        <property role="3GE5qa" value="Validation" />
        <node concept="2ShNRf" id="5$bixu1d_Yd" role="gfFT$">
          <node concept="1pGfFk" id="5$bixu1dCbg" role="2ShVmc">
            <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
          </node>
          <node concept="29HgVG" id="5$bixu1dCbw" role="lGtFl">
            <node concept="3NFfHV" id="5$bixu1dCbx" role="3NFExx">
              <node concept="3clFbS" id="5$bixu1dCby" role="2VODD2">
                <node concept="3clFbF" id="5$bixu1dCbC" role="3cqZAp">
                  <node concept="2OqwBi" id="5$bixu1dCbz" role="3clFbG">
                    <node concept="3TrEf2" id="5$bixu1dCbA" role="2OqNvi">
                      <ref role="3Tt5mk" to="qyjc:5$bixu1d_8Q" resolve="expression" />
                    </node>
                    <node concept="30H73N" id="5$bixu1dCbB" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7MdWNyBaM$d" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
      <node concept="gft3U" id="Pq8sy4imuX" role="1lVwrX">
        <node concept="37vLTw" id="7MdWNyBaMAa" role="gfFT$">
          <node concept="1ZhdrF" id="7MdWNyBaMAk" role="lGtFl">
            <property role="2qtEX8" value="variableDeclaration" />
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <node concept="3$xsQk" id="7MdWNyBaMAl" role="3$ytzL">
              <node concept="3clFbS" id="7MdWNyBaMAm" role="2VODD2">
                <node concept="3clFbF" id="7MdWNyBaMFb" role="3cqZAp">
                  <node concept="2OqwBi" id="7MdWNyBaMPH" role="3clFbG">
                    <node concept="1iwH7S" id="7MdWNyBaMFa" role="2Oq$k0" />
                    <node concept="1iwH70" id="7MdWNyBaMV7" role="2OqNvi">
                      <ref role="1iwH77" node="5T7UbTDiT7z" resolve="PipelineRef" />
                      <node concept="2OqwBi" id="7MdWNyBaNhA" role="1iwH7V">
                        <node concept="30H73N" id="7MdWNyBaN4l" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7MdWNyBevCa" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="6MwzKcXUPDK" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:yQ4gUuO8YZ" resolve="SourcePipelinePointer" />
      <node concept="gft3U" id="Pq8sy4imjZ" role="1lVwrX">
        <node concept="37vLTw" id="Pq8sy4imk5" role="gfFT$">
          <node concept="1ZhdrF" id="Pq8sy4imk8" role="lGtFl">
            <property role="2qtEX8" value="variableDeclaration" />
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <node concept="3$xsQk" id="Pq8sy4imk9" role="3$ytzL">
              <node concept="3clFbS" id="Pq8sy4imka" role="2VODD2">
                <node concept="3cpWs8" id="6MwzKcXUSpk" role="3cqZAp">
                  <node concept="3cpWsn" id="6MwzKcXUSpn" role="3cpWs9">
                    <property role="TrG5h" value="ref" />
                    <node concept="3Tqbb2" id="6MwzKcXUSpi" role="1tU5fm">
                      <ref role="ehGHo" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
                    </node>
                    <node concept="2OqwBi" id="6MwzKcXURh$" role="33vP2m">
                      <node concept="30H73N" id="6MwzKcXUR6J" role="2Oq$k0" />
                      <node concept="2qgKlT" id="6MwzKcXURr8" role="2OqNvi">
                        <ref role="37wK5l" to="25xb:yQ4gUuQSDf" resolve="getPipelineRef" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="6MwzKcXUQHi" role="3cqZAp">
                  <node concept="2OqwBi" id="6MwzKcXUQRO" role="3clFbG">
                    <node concept="1iwH7S" id="6MwzKcXUQHh" role="2Oq$k0" />
                    <node concept="1iwH70" id="6MwzKcXUQXx" role="2OqNvi">
                      <ref role="1iwH77" node="4o2stJ1YWa9" resolve="InputCollectionRef" />
                      <node concept="37vLTw" id="6MwzKcXUSCG" role="1iwH7V">
                        <ref role="3cqZAo" node="6MwzKcXUSpn" resolve="ref" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="575gSEdb0wp" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:575gSEd8CXW" resolve="RunAllPipelines" />
      <node concept="1Koe21" id="575gSEdbtET" role="1lVwrX">
        <node concept="3clFbS" id="575gSEdbtF1" role="1Koe22">
          <node concept="3cpWs8" id="575gSEdbtFe" role="3cqZAp">
            <node concept="3cpWsn" id="575gSEdbtFf" role="3cpWs9">
              <property role="TrG5h" value="pipeline" />
              <node concept="3uibUv" id="575gSEdbtFg" role="1tU5fm">
                <ref role="3uigEE" to="5ssx:~Pipeline" resolve="Pipeline" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="575gSEdbtFw" role="3cqZAp">
            <node concept="2OqwBi" id="4vp2seWdikV" role="3clFbG">
              <node concept="2OqwBi" id="575gSEdbtNO" role="2Oq$k0">
                <node concept="37vLTw" id="575gSEdbtFu" role="2Oq$k0">
                  <ref role="3cqZAo" node="575gSEdbtFf" resolve="pipeline" />
                </node>
                <node concept="liA8E" id="575gSEdbtVT" role="2OqNvi">
                  <ref role="37wK5l" to="5ssx:~Pipeline.run()" resolve="run" />
                </node>
              </node>
              <node concept="liA8E" id="4vp2seWdiTv" role="2OqNvi">
                <ref role="37wK5l" to="5ssx:~PipelineResult.waitUntilFinish()" resolve="waitUntilFinish" />
              </node>
            </node>
            <node concept="raruj" id="575gSEdbtXJ" role="lGtFl" />
            <node concept="1W57fq" id="Jw6Yu7SNuP" role="lGtFl">
              <node concept="3IZrLx" id="Jw6Yu7SNuQ" role="3IZSJc">
                <node concept="3clFbS" id="Jw6Yu7SNuR" role="2VODD2">
                  <node concept="3clFbF" id="Jw6Yu7SNLG" role="3cqZAp">
                    <node concept="2OqwBi" id="Jw6Yu7SO34" role="3clFbG">
                      <node concept="30H73N" id="Jw6Yu7SNLF" role="2Oq$k0" />
                      <node concept="3TrcHB" id="Jw6Yu7SOiJ" role="2OqNvi">
                        <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="gft3U" id="Jw6Yu7SOu$" role="UU_$l">
                <node concept="3clFbF" id="Jw6Yu7SOUP" role="gfFT$">
                  <node concept="2OqwBi" id="Jw6Yu7SP36" role="3clFbG">
                    <node concept="37vLTw" id="Jw6Yu7SOUN" role="2Oq$k0">
                      <ref role="3cqZAo" node="575gSEdbtFf" resolve="pipeline" />
                    </node>
                    <node concept="liA8E" id="Jw6Yu7SPbb" role="2OqNvi">
                      <ref role="37wK5l" to="5ssx:~Pipeline.run()" resolve="run" />
                    </node>
                  </node>
                  <node concept="1WS0z7" id="4vp2seWd7me" role="lGtFl">
                    <node concept="3JmXsc" id="4vp2seWd7mf" role="3Jn$fo">
                      <node concept="3clFbS" id="4vp2seWd7mg" role="2VODD2">
                        <node concept="3clFbF" id="4vp2seWd7tM" role="3cqZAp">
                          <node concept="2OqwBi" id="4vp2seWdcX8" role="3clFbG">
                            <node concept="2OqwBi" id="4vp2seWd9QQ" role="2Oq$k0">
                              <node concept="2OqwBi" id="4vp2seWd8_g" role="2Oq$k0">
                                <node concept="2OqwBi" id="4vp2seWd7I3" role="2Oq$k0">
                                  <node concept="30H73N" id="4vp2seWd7tL" role="2Oq$k0" />
                                  <node concept="2Xjw5R" id="4vp2seWd7XS" role="2OqNvi">
                                    <node concept="1xMEDy" id="4vp2seWd7XU" role="1xVPHs">
                                      <node concept="chp4Y" id="4vp2seWd80v" role="ri$Ld">
                                        <ref role="cht4Q" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWd9Br" role="2OqNvi">
                                  <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                                </node>
                              </node>
                              <node concept="3Tsc0h" id="4vp2seWdagg" role="2OqNvi">
                                <ref role="3TtcxE" to="tpee:fzcqZ_x" resolve="statement" />
                              </node>
                            </node>
                            <node concept="v3k3i" id="4vp2seWdePo" role="2OqNvi">
                              <node concept="chp4Y" id="4vp2seWdeRk" role="v3oSu">
                                <ref role="cht4Q" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="5jKBG" id="4vp2seWdfg1" role="lGtFl">
                    <ref role="v9R2y" node="575gSEdb1hF" resolve="reduce_RunPipeline" />
                    <node concept="3NFfHV" id="4vp2seWdfrb" role="5jGum">
                      <node concept="3clFbS" id="4vp2seWdfrc" role="2VODD2">
                        <node concept="3cpWs8" id="4vp2seWdfrd" role="3cqZAp">
                          <node concept="3cpWsn" id="4vp2seWdfre" role="3cpWs9">
                            <property role="TrG5h" value="run" />
                            <node concept="3Tqbb2" id="4vp2seWdfrf" role="1tU5fm">
                              <ref role="ehGHo" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
                            </node>
                            <node concept="2ShNRf" id="4vp2seWdfrg" role="33vP2m">
                              <node concept="3zrR0B" id="4vp2seWdfrh" role="2ShVmc">
                                <node concept="3Tqbb2" id="4vp2seWdfri" role="3zrR0E">
                                  <ref role="ehGHo" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="4vp2seWdfrj" role="3cqZAp">
                          <node concept="37vLTI" id="4vp2seWdfrk" role="3clFbG">
                            <node concept="2OqwBi" id="4vp2seWdfrm" role="37vLTJ">
                              <node concept="37vLTw" id="4vp2seWdfrn" role="2Oq$k0">
                                <ref role="3cqZAo" node="4vp2seWdfre" resolve="run" />
                              </node>
                              <node concept="3TrEf2" id="4vp2seWft3V" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                              </node>
                            </node>
                            <node concept="2ShNRf" id="4vp2seWftbf" role="37vLTx">
                              <node concept="3zrR0B" id="4vp2seWftbd" role="2ShVmc">
                                <node concept="3Tqbb2" id="4vp2seWftbe" role="3zrR0E">
                                  <ref role="ehGHo" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="4vp2seWftcz" role="3cqZAp">
                          <node concept="37vLTI" id="4vp2seWfu4R" role="3clFbG">
                            <node concept="30H73N" id="4vp2seWfu6k" role="37vLTx" />
                            <node concept="2OqwBi" id="4vp2seWft$a" role="37vLTJ">
                              <node concept="2OqwBi" id="4vp2seWftoe" role="2Oq$k0">
                                <node concept="37vLTw" id="4vp2seWftcx" role="2Oq$k0">
                                  <ref role="3cqZAo" node="4vp2seWdfre" resolve="run" />
                                </node>
                                <node concept="3TrEf2" id="4vp2seWftpm" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                                </node>
                              </node>
                              <node concept="3TrEf2" id="4vp2seWftNH" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbF" id="4vp2seWdfrp" role="3cqZAp">
                          <node concept="37vLTI" id="4vp2seWdfrq" role="3clFbG">
                            <node concept="2OqwBi" id="4vp2seWdfrr" role="37vLTJ">
                              <node concept="37vLTw" id="4vp2seWdfrs" role="2Oq$k0">
                                <ref role="3cqZAo" node="4vp2seWdfre" resolve="run" />
                              </node>
                              <node concept="3TrcHB" id="4vp2seWdfrt" role="2OqNvi">
                                <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                              </node>
                            </node>
                            <node concept="3clFbT" id="4vp2seWdfru" role="37vLTx" />
                          </node>
                        </node>
                        <node concept="3cpWs6" id="4vp2seWdfrv" role="3cqZAp">
                          <node concept="37vLTw" id="4vp2seWdfrw" role="3cqZAk">
                            <ref role="3cqZAo" node="4vp2seWdfre" resolve="run" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1WS0z7" id="575gSEdbtXL" role="lGtFl">
              <node concept="3JmXsc" id="575gSEdbtXO" role="3Jn$fo">
                <node concept="3clFbS" id="575gSEdbtXP" role="2VODD2">
                  <node concept="3clFbF" id="575gSEdbKZQ" role="3cqZAp">
                    <node concept="2OqwBi" id="6$tYh_AhcnX" role="3clFbG">
                      <node concept="2OqwBi" id="6$tYh_Ah9tV" role="2Oq$k0">
                        <node concept="2OqwBi" id="575gSEdbIYT" role="2Oq$k0">
                          <node concept="2OqwBi" id="575gSEdbIdf" role="2Oq$k0">
                            <node concept="30H73N" id="575gSEdbHXN" role="2Oq$k0" />
                            <node concept="2Xjw5R" id="575gSEdbIrW" role="2OqNvi">
                              <node concept="1xMEDy" id="575gSEdbIrY" role="1xVPHs">
                                <node concept="chp4Y" id="575gSEdbIuD" role="ri$Ld">
                                  <ref role="cht4Q" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3TrEf2" id="6$tYh_Ah90d" role="2OqNvi">
                            <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                          </node>
                        </node>
                        <node concept="3Tsc0h" id="6$tYh_Ah9SW" role="2OqNvi">
                          <ref role="3TtcxE" to="tpee:fzcqZ_x" resolve="statement" />
                        </node>
                      </node>
                      <node concept="v3k3i" id="6$tYh_Ahexy" role="2OqNvi">
                        <node concept="chp4Y" id="6$tYh_AheAo" role="v3oSu">
                          <ref role="cht4Q" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="5jKBG" id="575gSEdbwZQ" role="lGtFl">
              <ref role="v9R2y" node="575gSEdb1hF" resolve="reduce_RunPipeline" />
              <node concept="3NFfHV" id="575gSEdbL3O" role="5jGum">
                <node concept="3clFbS" id="575gSEdbL3P" role="2VODD2">
                  <node concept="3cpWs8" id="575gSEdbL79" role="3cqZAp">
                    <node concept="3cpWsn" id="575gSEdbL7c" role="3cpWs9">
                      <property role="TrG5h" value="run" />
                      <node concept="3Tqbb2" id="575gSEdbL78" role="1tU5fm">
                        <ref role="ehGHo" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
                      </node>
                      <node concept="2ShNRf" id="575gSEdbLE0" role="33vP2m">
                        <node concept="3zrR0B" id="575gSEdbLDY" role="2ShVmc">
                          <node concept="3Tqbb2" id="575gSEdbLDZ" role="3zrR0E">
                            <ref role="ehGHo" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="575gSEdbLGk" role="3cqZAp">
                    <node concept="37vLTI" id="575gSEdbMpp" role="3clFbG">
                      <node concept="2ShNRf" id="4vp2seWfrsE" role="37vLTx">
                        <node concept="3zrR0B" id="4vp2seWfrsC" role="2ShVmc">
                          <node concept="3Tqbb2" id="4vp2seWfrsD" role="3zrR0E">
                            <ref role="ehGHo" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
                          </node>
                        </node>
                      </node>
                      <node concept="2OqwBi" id="575gSEdbLSn" role="37vLTJ">
                        <node concept="37vLTw" id="575gSEdbLGi" role="2Oq$k0">
                          <ref role="3cqZAo" node="575gSEdbL7c" resolve="run" />
                        </node>
                        <node concept="3TrEf2" id="4vp2seWfqJO" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="4vp2seWfr_o" role="3cqZAp">
                    <node concept="37vLTI" id="4vp2seWfsFM" role="3clFbG">
                      <node concept="30H73N" id="4vp2seWfsIL" role="37vLTx" />
                      <node concept="2OqwBi" id="4vp2seWfsdm" role="37vLTJ">
                        <node concept="2OqwBi" id="4vp2seWfrLH" role="2Oq$k0">
                          <node concept="37vLTw" id="4vp2seWfr_m" role="2Oq$k0">
                            <ref role="3cqZAo" node="575gSEdbL7c" resolve="run" />
                          </node>
                          <node concept="3TrEf2" id="4vp2seWfs1I" role="2OqNvi">
                            <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="4vp2seWfssT" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="Jw6Yu7SLqI" role="3cqZAp">
                    <node concept="37vLTI" id="Jw6Yu7SM1c" role="3clFbG">
                      <node concept="2OqwBi" id="Jw6Yu7SLBp" role="37vLTJ">
                        <node concept="37vLTw" id="Jw6Yu7SLqG" role="2Oq$k0">
                          <ref role="3cqZAo" node="575gSEdbL7c" resolve="run" />
                        </node>
                        <node concept="3TrcHB" id="Jw6Yu7SLSb" role="2OqNvi">
                          <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                        </node>
                      </node>
                      <node concept="3clFbT" id="Jw6Yu7SN3N" role="37vLTx">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="3cpWs6" id="575gSEdbMz$" role="3cqZAp">
                    <node concept="37vLTw" id="575gSEdbM_9" role="3cqZAk">
                      <ref role="3cqZAo" node="575gSEdbL7c" resolve="run" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="575gSEdb16S" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
      <node concept="j$656" id="575gSEdb1hH" role="1lVwrX">
        <ref role="v9R2y" node="575gSEdb1hF" resolve="reduce_RunPipeline" />
      </node>
    </node>
    <node concept="3aamgX" id="Pq8sy4imzC" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:1CpVIWKRvIg" resolve="EventSinkRef" />
      <node concept="gft3U" id="Pq8sy4imzA" role="1lVwrX">
        <property role="3GE5qa" value="Sink" />
        <node concept="37vLTw" id="Pq8sy4imD7" role="gfFT$">
          <node concept="1ZhdrF" id="Pq8sy4imDh" role="lGtFl">
            <property role="2qtEX8" value="variableDeclaration" />
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <node concept="3$xsQk" id="Pq8sy4imDi" role="3$ytzL">
              <node concept="3clFbS" id="Pq8sy4imDj" role="2VODD2">
                <node concept="3clFbF" id="Pq8sy4imEC" role="3cqZAp">
                  <node concept="2OqwBi" id="Pq8sy4imPa" role="3clFbG">
                    <node concept="1iwH7S" id="Pq8sy4imEB" role="2Oq$k0" />
                    <node concept="1iwH70" id="Pq8sy4imVk" role="2OqNvi">
                      <ref role="1iwH77" node="4o2stJ22OtC" resolve="SinkRef" />
                      <node concept="2OqwBi" id="Pq8sy4inkN" role="1iwH7V">
                        <node concept="30H73N" id="Pq8sy4in9Z" role="2Oq$k0" />
                        <node concept="3TrEf2" id="Pq8sy4inw5" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:RoIJycd2vh" resolve="ref" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="Pq8sy4ioG2" role="3acgRq">
      <ref role="30HIoZ" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
      <node concept="1Koe21" id="Pq8sy4ioLr" role="1lVwrX">
        <node concept="3clFbS" id="Pq8sy4ioLx" role="1Koe22">
          <node concept="3cpWs8" id="Pq8sy4ioLC" role="3cqZAp">
            <node concept="3cpWsn" id="Pq8sy4ioLD" role="3cpWs9">
              <property role="TrG5h" value="collectionTuple" />
              <node concept="3uibUv" id="Pq8sy4irc5" role="1tU5fm">
                <ref role="3uigEE" to="g9lr:~PCollectionTuple" resolve="PCollectionTuple" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="Pq8sy4irHj" role="3cqZAp">
            <node concept="3cpWsn" id="Pq8sy4irHk" role="3cpWs9">
              <property role="TrG5h" value="tag" />
              <node concept="3uibUv" id="Pq8sy4irHl" role="1tU5fm">
                <ref role="3uigEE" to="g9lr:~TupleTag" resolve="TupleTag" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="Pq8sy4ioLT" role="3cqZAp">
            <node concept="2OqwBi" id="Pq8sy4ip5j" role="3clFbG">
              <node concept="37vLTw" id="Pq8sy4ioLR" role="2Oq$k0">
                <ref role="3cqZAo" node="Pq8sy4ioLD" resolve="collectionTuple" />
                <node concept="1ZhdrF" id="Pq8sy4is61" role="lGtFl">
                  <property role="2qtEX8" value="variableDeclaration" />
                  <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                  <node concept="3$xsQk" id="Pq8sy4is62" role="3$ytzL">
                    <node concept="3clFbS" id="Pq8sy4is63" role="2VODD2">
                      <node concept="3clFbF" id="Pq8sy4isdF" role="3cqZAp">
                        <node concept="2OqwBi" id="Pq8sy4isru" role="3clFbG">
                          <node concept="1iwH7S" id="Pq8sy4isdE" role="2Oq$k0" />
                          <node concept="1iwH70" id="Pq8sy4isxb" role="2OqNvi">
                            <ref role="1iwH77" node="4o2stJ22OtC" resolve="SinkRef" />
                            <node concept="2OqwBi" id="Pq8sy4isPZ" role="1iwH7V">
                              <node concept="30H73N" id="Pq8sy4isEp" role="2Oq$k0" />
                              <node concept="3TrEf2" id="Pq8sy4it20" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:RoIJycd2vh" resolve="ref" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="Pq8sy4irq5" role="2OqNvi">
                <ref role="37wK5l" to="g9lr:~PCollectionTuple.get(java.lang.String)" resolve="get" />
                <node concept="37vLTw" id="Pq8sy4irI6" role="37wK5m">
                  <ref role="3cqZAo" node="Pq8sy4irHk" resolve="tag" />
                  <node concept="29HgVG" id="Pq8sy4irYk" role="lGtFl">
                    <node concept="3NFfHV" id="Pq8sy4irYl" role="3NFExx">
                      <node concept="3clFbS" id="Pq8sy4irYm" role="2VODD2">
                        <node concept="3clFbJ" id="6kk4K$6yfoQ" role="3cqZAp">
                          <node concept="3clFbS" id="6kk4K$6yfoS" role="3clFbx">
                            <node concept="2xdQw9" id="6kk4K$6yiIr" role="3cqZAp">
                              <property role="2xdLsb" value="gZ5fh_4/error" />
                              <node concept="Xl_RD" id="6kk4K$6yiIt" role="9lYJi">
                                <property role="Xl_RC" value="No tuple tag provided in partial sink for assert" />
                              </node>
                            </node>
                          </node>
                          <node concept="2OqwBi" id="6kk4K$6yhsP" role="3clFbw">
                            <node concept="2OqwBi" id="6kk4K$6yf_F" role="2Oq$k0">
                              <node concept="30H73N" id="6kk4K$6yfp9" role="2Oq$k0" />
                              <node concept="3Tsc0h" id="6kk4K$6yfN6" role="2OqNvi">
                                <ref role="3TtcxE" to="qyjc:Pq8sy4bYUb" resolve="splitter" />
                              </node>
                            </node>
                            <node concept="1v1jN8" id="6kk4K$6yiIl" role="2OqNvi" />
                          </node>
                          <node concept="3eNFk2" id="6kk4K$6yiOk" role="3eNLev">
                            <node concept="3eOSWO" id="6kk4K$6ynKB" role="3eO9$A">
                              <node concept="3cmrfG" id="6kk4K$6ynKT" role="3uHU7w">
                                <property role="3cmrfH" value="1" />
                              </node>
                              <node concept="2OqwBi" id="6kk4K$6ylgi" role="3uHU7B">
                                <node concept="2OqwBi" id="6kk4K$6yjqe" role="2Oq$k0">
                                  <node concept="30H73N" id="6kk4K$6yjb6" role="2Oq$k0" />
                                  <node concept="3Tsc0h" id="6kk4K$6yj_a" role="2OqNvi">
                                    <ref role="3TtcxE" to="qyjc:Pq8sy4bYUb" resolve="splitter" />
                                  </node>
                                </node>
                                <node concept="34oBXx" id="6kk4K$6ymy2" role="2OqNvi" />
                              </node>
                            </node>
                            <node concept="3clFbS" id="6kk4K$6yiOm" role="3eOfB_">
                              <node concept="2xdQw9" id="6kk4K$6ynKY" role="3cqZAp">
                                <property role="2xdLsb" value="gZ5fh_4/error" />
                                <node concept="Xl_RD" id="6kk4K$6ynKZ" role="9lYJi">
                                  <property role="Xl_RC" value="To many elements for partial sink in assert" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="9aQIb" id="6kk4K$6yiOy" role="9aQIa">
                            <node concept="3clFbS" id="6kk4K$6yiOz" role="9aQI4">
                              <node concept="3cpWs6" id="6kk4K$6yj7d" role="3cqZAp">
                                <node concept="2OqwBi" id="6kk4K$6ybvh" role="3cqZAk">
                                  <node concept="2OqwBi" id="Pq8sy4irYn" role="2Oq$k0">
                                    <node concept="30H73N" id="Pq8sy4irYr" role="2Oq$k0" />
                                    <node concept="3Tsc0h" id="6kk4K$6y9Cf" role="2OqNvi">
                                      <ref role="3TtcxE" to="qyjc:Pq8sy4bYUb" resolve="splitter" />
                                    </node>
                                  </node>
                                  <node concept="34jXtK" id="6kk4K$6ycLk" role="2OqNvi">
                                    <node concept="3cmrfG" id="6kk4K$6yfjq" role="25WWJ7">
                                      <property role="3cmrfH" value="0" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="3cpWs6" id="6kk4K$6yjas" role="3cqZAp">
                          <node concept="10Nm6u" id="6kk4K$6yjb0" role="3cqZAk" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="raruj" id="Pq8sy4is4f" role="lGtFl" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="jVnub" id="4o2stJ2awe9">
    <property role="TrG5h" value="AssertionScope" />
    <node concept="3aamgX" id="4o2stJ2awea" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="1Koe21" id="4o2stJ2axc1" role="1lVwrX">
        <node concept="3clFbS" id="4o2stJ2axcF" role="1Koe22">
          <node concept="3cpWs8" id="4o2stJ2axcP" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2axcQ" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="4o2stJ2axcR" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4o2stJ2axpc" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2axpd" role="3cpWs9">
              <property role="TrG5h" value="window" />
              <node concept="3uibUv" id="4o2stJ2axpe" role="1tU5fm">
                <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4o2stJ2axd7" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2axiE" role="3clFbG">
              <node concept="37vLTw" id="4o2stJ2axd5" role="2Oq$k0">
                <ref role="3cqZAo" node="4o2stJ2axcQ" resolve="ass" />
              </node>
              <node concept="liA8E" id="4o2stJ2axn2" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.inWindow(org.apache.beam.sdk.transforms.windowing.BoundedWindow)" resolve="inWindow" />
                <node concept="37vLTw" id="4o2stJ2axq0" role="37wK5m">
                  <ref role="3cqZAo" node="4o2stJ2axpd" resolve="window" />
                  <node concept="1ZhdrF" id="4o2stJ2axtX" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="4o2stJ2axtY" role="3$ytzL">
                      <node concept="3clFbS" id="4o2stJ2axtZ" role="2VODD2">
                        <node concept="3clFbF" id="4o2stJ2axxr" role="3cqZAp">
                          <node concept="2OqwBi" id="4o2stJ2ayER" role="3clFbG">
                            <node concept="1iwH7S" id="4o2stJ2aywm" role="2Oq$k0" />
                            <node concept="1iwH70" id="4o2stJ2azp9" role="2OqNvi">
                              <ref role="1iwH77" node="4o2stJ24hGS" resolve="WindowRef" />
                              <node concept="2OqwBi" id="4vp2seWkf0P" role="1iwH7V">
                                <node concept="2OqwBi" id="4o2stJ2azGR" role="2Oq$k0">
                                  <node concept="30H73N" id="4o2stJ2azyn" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4vp2seWkeNd" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:4vp2seWke2c" resolve="window" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWkfaE" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWfjgK" resolve="ref" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="4o2stJ2axt6" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="4o2stJ2awee" role="30HLyM">
        <node concept="3clFbS" id="4o2stJ2awef" role="2VODD2">
          <node concept="3clFbF" id="4o2stJ2awib" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2ax00" role="3clFbG">
              <node concept="2OqwBi" id="4o2stJ2awuT" role="2Oq$k0">
                <node concept="30H73N" id="4o2stJ2awia" role="2Oq$k0" />
                <node concept="3TrcHB" id="4o2stJ2aw_I" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                </node>
              </node>
              <node concept="21noJN" id="4o2stJ2ax6Y" role="2OqNvi">
                <node concept="21nZrQ" id="4o2stJ2axbk" role="21noJM">
                  <ref role="21nZrZ" to="qyjc:4o2stJ24AP0" resolve="Window" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4o2stJ2bqAq" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="30G5F_" id="4o2stJ2bqAr" role="30HLyM">
        <node concept="3clFbS" id="4o2stJ2bqAs" role="2VODD2">
          <node concept="3clFbF" id="4o2stJ2bqAt" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2bqAu" role="3clFbG">
              <node concept="2OqwBi" id="4o2stJ2bqAv" role="2Oq$k0">
                <node concept="30H73N" id="4o2stJ2bqAw" role="2Oq$k0" />
                <node concept="3TrcHB" id="4o2stJ2bqAx" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                </node>
              </node>
              <node concept="21noJN" id="4o2stJ2bqAy" role="2OqNvi">
                <node concept="21nZrQ" id="4o2stJ2bqAz" role="21noJM">
                  <ref role="21nZrZ" to="qyjc:4o2stJ22OS6" resolve="EarlyPane" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Koe21" id="4o2stJ2bqA$" role="1lVwrX">
        <node concept="3clFbS" id="4o2stJ2bqA_" role="1Koe22">
          <node concept="3cpWs8" id="4o2stJ2bqAA" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2bqAB" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="4o2stJ2bqAC" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4o2stJ2bqAD" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2bqAE" role="3cpWs9">
              <property role="TrG5h" value="window" />
              <node concept="3uibUv" id="4o2stJ2bqAF" role="1tU5fm">
                <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4o2stJ2bqAG" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2bqAH" role="3clFbG">
              <node concept="37vLTw" id="4o2stJ2bqAI" role="2Oq$k0">
                <ref role="3cqZAo" node="4o2stJ2bqAB" resolve="ass" />
              </node>
              <node concept="liA8E" id="4o2stJ2bqAJ" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.inEarlyPane(org.apache.beam.sdk.transforms.windowing.BoundedWindow)" resolve="inEarlyPane" />
                <node concept="37vLTw" id="4o2stJ2bqAK" role="37wK5m">
                  <ref role="3cqZAo" node="4o2stJ2bqAE" resolve="window" />
                  <node concept="1ZhdrF" id="4o2stJ2bqAL" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="4o2stJ2bqAM" role="3$ytzL">
                      <node concept="3clFbS" id="4o2stJ2bqAN" role="2VODD2">
                        <node concept="3clFbF" id="4o2stJ2bqAO" role="3cqZAp">
                          <node concept="2OqwBi" id="4o2stJ2bqAP" role="3clFbG">
                            <node concept="1iwH7S" id="4o2stJ2bqAQ" role="2Oq$k0" />
                            <node concept="1iwH70" id="4o2stJ2bqAR" role="2OqNvi">
                              <ref role="1iwH77" node="4o2stJ24hGS" resolve="WindowRef" />
                              <node concept="2OqwBi" id="4vp2seWkfH1" role="1iwH7V">
                                <node concept="2OqwBi" id="4o2stJ2bqAS" role="2Oq$k0">
                                  <node concept="30H73N" id="4o2stJ2bqAT" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4vp2seWkfyD" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:4vp2seWke2c" resolve="window" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWkfRb" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWfjgK" resolve="ref" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="4o2stJ2bqAV" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4o2stJ2bpBr" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="30G5F_" id="4o2stJ2bpH9" role="30HLyM">
        <node concept="3clFbS" id="4o2stJ2bpHa" role="2VODD2">
          <node concept="3clFbF" id="4o2stJ2bpHz" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2bqhB" role="3clFbG">
              <node concept="2OqwBi" id="4o2stJ2bpUf" role="2Oq$k0">
                <node concept="30H73N" id="4o2stJ2bpHy" role="2Oq$k0" />
                <node concept="3TrcHB" id="4o2stJ2bq4Y" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                </node>
              </node>
              <node concept="21noJN" id="4o2stJ2bqpa" role="2OqNvi">
                <node concept="21nZrQ" id="4o2stJ2brzW" role="21noJM">
                  <ref role="21nZrZ" to="qyjc:4o2stJ22OS7" resolve="OnTimePane" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Koe21" id="4o2stJ2bqxN" role="1lVwrX">
        <node concept="3clFbS" id="4o2stJ2bqxO" role="1Koe22">
          <node concept="3cpWs8" id="4o2stJ2bqxP" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2bqxQ" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="4o2stJ2bqxR" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4o2stJ2bqxS" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2bqxT" role="3cpWs9">
              <property role="TrG5h" value="window" />
              <node concept="3uibUv" id="4o2stJ2bqxU" role="1tU5fm">
                <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4o2stJ2bqxV" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2bqxW" role="3clFbG">
              <node concept="37vLTw" id="4o2stJ2bqxX" role="2Oq$k0">
                <ref role="3cqZAo" node="4o2stJ2bqxQ" resolve="ass" />
              </node>
              <node concept="liA8E" id="4o2stJ2bqxY" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.inOnTimePane(org.apache.beam.sdk.transforms.windowing.BoundedWindow)" resolve="inOnTimePane" />
                <node concept="37vLTw" id="4o2stJ2bqxZ" role="37wK5m">
                  <ref role="3cqZAo" node="4o2stJ2bqxT" resolve="window" />
                  <node concept="1ZhdrF" id="4o2stJ2bqy0" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="4o2stJ2bqy1" role="3$ytzL">
                      <node concept="3clFbS" id="4o2stJ2bqy2" role="2VODD2">
                        <node concept="3clFbF" id="4o2stJ2bqy3" role="3cqZAp">
                          <node concept="2OqwBi" id="4o2stJ2bqy4" role="3clFbG">
                            <node concept="1iwH7S" id="4o2stJ2bqy5" role="2Oq$k0" />
                            <node concept="1iwH70" id="4o2stJ2bqy6" role="2OqNvi">
                              <ref role="1iwH77" node="4o2stJ24hGS" resolve="WindowRef" />
                              <node concept="2OqwBi" id="4vp2seWkgoi" role="1iwH7V">
                                <node concept="2OqwBi" id="4o2stJ2bqy7" role="2Oq$k0">
                                  <node concept="30H73N" id="4o2stJ2bqy8" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4vp2seWkgb4" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:4vp2seWke2c" resolve="window" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWkgys" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWfjgK" resolve="ref" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="4o2stJ2bqya" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4o2stJ2br5a" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="30G5F_" id="4o2stJ2br5b" role="30HLyM">
        <node concept="3clFbS" id="4o2stJ2br5c" role="2VODD2">
          <node concept="3clFbF" id="4o2stJ2br5d" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2br5e" role="3clFbG">
              <node concept="2OqwBi" id="4o2stJ2br5f" role="2Oq$k0">
                <node concept="30H73N" id="4o2stJ2br5g" role="2Oq$k0" />
                <node concept="3TrcHB" id="4o2stJ2br5h" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                </node>
              </node>
              <node concept="21noJN" id="4o2stJ2br5i" role="2OqNvi">
                <node concept="21nZrQ" id="4o2stJ2brvD" role="21noJM">
                  <ref role="21nZrZ" to="qyjc:4o2stJ22OSa" resolve="LatePane" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1Koe21" id="4o2stJ2br5k" role="1lVwrX">
        <node concept="3clFbS" id="4o2stJ2br5l" role="1Koe22">
          <node concept="3cpWs8" id="4o2stJ2br5m" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2br5n" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="4o2stJ2br5o" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="4o2stJ2br5p" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2br5q" role="3cpWs9">
              <property role="TrG5h" value="window" />
              <node concept="3uibUv" id="4o2stJ2br5r" role="1tU5fm">
                <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4o2stJ2br5s" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2br5t" role="3clFbG">
              <node concept="37vLTw" id="4o2stJ2br5u" role="2Oq$k0">
                <ref role="3cqZAo" node="4o2stJ2br5n" resolve="ass" />
              </node>
              <node concept="liA8E" id="4o2stJ2br5v" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.inLatePane(org.apache.beam.sdk.transforms.windowing.BoundedWindow)" resolve="inLatePane" />
                <node concept="37vLTw" id="4o2stJ2br5w" role="37wK5m">
                  <ref role="3cqZAo" node="4o2stJ2br5q" resolve="window" />
                  <node concept="1ZhdrF" id="4o2stJ2br5x" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="4o2stJ2br5y" role="3$ytzL">
                      <node concept="3clFbS" id="4o2stJ2br5z" role="2VODD2">
                        <node concept="3clFbF" id="4o2stJ2br5$" role="3cqZAp">
                          <node concept="2OqwBi" id="4o2stJ2br5_" role="3clFbG">
                            <node concept="1iwH7S" id="4o2stJ2br5A" role="2Oq$k0" />
                            <node concept="1iwH70" id="4o2stJ2br5B" role="2OqNvi">
                              <ref role="1iwH77" node="4o2stJ24hGS" resolve="WindowRef" />
                              <node concept="2OqwBi" id="4vp2seWkhVw" role="1iwH7V">
                                <node concept="2OqwBi" id="4o2stJ2br5C" role="2Oq$k0">
                                  <node concept="30H73N" id="4o2stJ2br5D" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4vp2seWkhIi" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:4vp2seWke2c" resolve="window" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWki5E" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:4vp2seWfjgK" resolve="ref" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="4o2stJ2br5F" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="b5Tf3" id="4o2stJ2bpvz" role="jxRDz" />
  </node>
  <node concept="jVnub" id="4o2stJ29Emh">
    <property role="TrG5h" value="AssertionOrder" />
    <node concept="3aamgX" id="575gSEdkPyr" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="1Koe21" id="575gSEdkTgh" role="1lVwrX">
        <node concept="3clFbS" id="575gSEdkThs" role="1Koe22">
          <node concept="3cpWs8" id="575gSEdkThZ" role="3cqZAp">
            <node concept="3cpWsn" id="575gSEdkTi0" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="575gSEdkTi1" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="575gSEdkTii" role="3cqZAp">
            <node concept="2OqwBi" id="575gSEdkTnP" role="3clFbG">
              <node concept="37vLTw" id="575gSEdkTig" role="2Oq$k0">
                <ref role="3cqZAo" node="575gSEdkTi0" resolve="ass" />
              </node>
              <node concept="liA8E" id="575gSEdkTtx" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.empty()" resolve="empty" />
                <node concept="raruj" id="575gSEdkTzm" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="575gSEdkPIO" role="30HLyM">
        <node concept="3clFbS" id="575gSEdkPIP" role="2VODD2">
          <node concept="3clFbF" id="575gSEdkPJe" role="3cqZAp">
            <node concept="2OqwBi" id="575gSEdkRUJ" role="3clFbG">
              <node concept="2OqwBi" id="575gSEdkPZO" role="2Oq$k0">
                <node concept="30H73N" id="575gSEdkPJd" role="2Oq$k0" />
                <node concept="3Tsc0h" id="575gSEdkQhB" role="2OqNvi">
                  <ref role="3TtcxE" to="qyjc:4o2stJ24APr" resolve="expectedEntries" />
                </node>
              </node>
              <node concept="1v1jN8" id="575gSEdkTbw" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="4o2stJ29Emi" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
      <node concept="1Koe21" id="4o2stJ2ah7N" role="1lVwrX">
        <node concept="3clFbS" id="4o2stJ2ah8p" role="1Koe22">
          <node concept="3cpWs8" id="4o2stJ2ah8z" role="3cqZAp">
            <node concept="3cpWsn" id="4o2stJ2ah8$" role="3cpWs9">
              <property role="TrG5h" value="ass" />
              <node concept="3uibUv" id="4o2stJ2ah8_" role="1tU5fm">
                <ref role="3uigEE" to="cqpf:~PAssert$IterableAssert" resolve="PAssert.IterableAssert" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="4o2stJ2ah8P" role="3cqZAp">
            <node concept="2OqwBi" id="4o2stJ2aheo" role="3clFbG">
              <node concept="37vLTw" id="4o2stJ2ah8N" role="2Oq$k0">
                <ref role="3cqZAo" node="4o2stJ2ah8$" resolve="ass" />
              </node>
              <node concept="liA8E" id="4o2stJ2ahk4" role="2OqNvi">
                <ref role="37wK5l" to="cqpf:~PAssert$IterableAssert.containsInAnyOrder(java.lang.Object...)" resolve="containsInAnyOrder" />
                <node concept="10Nm6u" id="4o2stJ2ahr2" role="37wK5m">
                  <node concept="2b32R4" id="4o2stJ2ahAv" role="lGtFl">
                    <node concept="3JmXsc" id="4o2stJ2ahAy" role="2P8S$">
                      <node concept="3clFbS" id="4o2stJ2ahAz" role="2VODD2">
                        <node concept="3clFbF" id="4o2stJ2ahAD" role="3cqZAp">
                          <node concept="2OqwBi" id="4o2stJ2aiQu" role="3clFbG">
                            <node concept="30H73N" id="4o2stJ2aikJ" role="2Oq$k0" />
                            <node concept="3Tsc0h" id="4o2stJ2aj1b" role="2OqNvi">
                              <ref role="3TtcxE" to="qyjc:4o2stJ24APr" resolve="expectedEntries" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="raruj" id="4o2stJ2ah$y" role="lGtFl" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="4o2stJ29Emm" role="30HLyM">
        <node concept="3clFbS" id="4o2stJ29Emn" role="2VODD2">
          <node concept="3clFbF" id="4o2stJ2j4_K" role="3cqZAp">
            <node concept="3fqX7Q" id="4o2stJ2j4_I" role="3clFbG">
              <node concept="1eOMI4" id="4o2stJ2j4At" role="3fr31v">
                <node concept="2OqwBi" id="4o2stJ2j4Rz" role="1eOMHV">
                  <node concept="30H73N" id="4o2stJ2j4EI" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4o2stJ2j4YT" role="2OqNvi">
                    <ref role="3TsBF5" to="qyjc:4o2stJ29Ywl" resolve="inSameOrder" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="b5Tf3" id="4o2stJ2ahpX" role="jxRDz" />
  </node>
  <node concept="13MO4I" id="4o2stJ2geKm">
    <property role="TrG5h" value="reduce_EventSink" />
    <property role="3GE5qa" value="Sink" />
    <ref role="3gUMe" to="qyjc:1CpVIWKKpwu" resolve="EventSink" />
    <node concept="3clFbS" id="4o2stJ2geKD" role="13RCb5">
      <node concept="3cpWs8" id="4o2stJ2gePr" role="3cqZAp">
        <node concept="3cpWsn" id="4o2stJ2gePs" role="3cpWs9">
          <property role="TrG5h" value="collection" />
          <node concept="3uibUv" id="4o2stJ2gePt" role="1tU5fm">
            <ref role="3uigEE" to="g9lr:~PCollection" resolve="PCollection" />
            <node concept="3uibUv" id="4o2stJ2gf54" role="11_B2D">
              <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
              <node concept="29HgVG" id="4o2stJ2gf5S" role="lGtFl">
                <node concept="3NFfHV" id="4o2stJ2gf5T" role="3NFExx">
                  <node concept="3clFbS" id="4o2stJ2gf5U" role="2VODD2">
                    <node concept="3clFbF" id="4o2stJ2gf60" role="3cqZAp">
                      <node concept="2OqwBi" id="4o2stJ2gf5V" role="3clFbG">
                        <node concept="3TrEf2" id="4o2stJ2gf5Y" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:4o2stJ20FRT" resolve="type" />
                        </node>
                        <node concept="30H73N" id="4o2stJ2gf5Z" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="10Nm6u" id="4o2stJ2gePO" role="33vP2m">
            <node concept="29HgVG" id="1SqhhvM0o_C" role="lGtFl">
              <node concept="3NFfHV" id="1SqhhvM0o_E" role="3NFExx">
                <node concept="3clFbS" id="1SqhhvM0o_F" role="2VODD2">
                  <node concept="3clFbF" id="1SqhhvM0o_M" role="3cqZAp">
                    <node concept="2OqwBi" id="1SqhhvM0oTL" role="3clFbG">
                      <node concept="30H73N" id="1SqhhvM0o_L" role="2Oq$k0" />
                      <node concept="3TrEf2" id="1SqhhvM0p3y" role="2OqNvi">
                        <ref role="3Tt5mk" to="qyjc:1SqhhvLZL3$" resolve="from" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="17Uvod" id="4o2stJ2gePZ" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="4o2stJ2geQ2" role="3zH0cK">
              <node concept="3clFbS" id="4o2stJ2geQ3" role="2VODD2">
                <node concept="3clFbF" id="4o2stJ2geQ9" role="3cqZAp">
                  <node concept="2OqwBi" id="4o2stJ2geQ4" role="3clFbG">
                    <node concept="3TrcHB" id="4o2stJ2geQ7" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                    <node concept="30H73N" id="4o2stJ2geQ8" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="4o2stJ2gfbD" role="lGtFl">
            <ref role="2rW$FS" node="4o2stJ22OtC" resolve="SinkRef" />
          </node>
          <node concept="1W57fq" id="Pq8sy4iWLu" role="lGtFl">
            <node concept="3IZrLx" id="Pq8sy4iWLv" role="3IZSJc">
              <node concept="3clFbS" id="Pq8sy4iWLw" role="2VODD2">
                <node concept="3clFbF" id="Pq8sy4iWTJ" role="3cqZAp">
                  <node concept="3fqX7Q" id="Pq8sy4iXB8" role="3clFbG">
                    <node concept="1eOMI4" id="Pq8sy4iYq0" role="3fr31v">
                      <node concept="2OqwBi" id="Pq8sy4iXBa" role="1eOMHV">
                        <node concept="30H73N" id="Pq8sy4iXBb" role="2Oq$k0" />
                        <node concept="3TrcHB" id="Pq8sy4iXBc" role="2OqNvi">
                          <ref role="3TsBF5" to="qyjc:Pq8sy4ipnW" resolve="multipleOutputs" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="gft3U" id="Pq8sy4iYuw" role="UU_$l">
              <node concept="3cpWsn" id="Pq8sy4j0zz" role="gfFT$">
                <property role="TrG5h" value="tuple" />
                <node concept="10Nm6u" id="Pq8sy4j0zH" role="33vP2m">
                  <node concept="29HgVG" id="Pq8sy4j0zI" role="lGtFl">
                    <node concept="3NFfHV" id="Pq8sy4j0zJ" role="3NFExx">
                      <node concept="3clFbS" id="Pq8sy4j0zK" role="2VODD2">
                        <node concept="3clFbF" id="Pq8sy4j0zL" role="3cqZAp">
                          <node concept="2OqwBi" id="Pq8sy4j0zM" role="3clFbG">
                            <node concept="30H73N" id="Pq8sy4j0zN" role="2Oq$k0" />
                            <node concept="3TrEf2" id="Pq8sy4j0zO" role="2OqNvi">
                              <ref role="3Tt5mk" to="qyjc:1SqhhvLZL3$" resolve="from" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="17Uvod" id="Pq8sy4j0zP" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="Pq8sy4j0zQ" role="3zH0cK">
                    <node concept="3clFbS" id="Pq8sy4j0zR" role="2VODD2">
                      <node concept="3clFbF" id="Pq8sy4j0zS" role="3cqZAp">
                        <node concept="2OqwBi" id="Pq8sy4j0zT" role="3clFbG">
                          <node concept="3TrcHB" id="Pq8sy4j0zU" role="2OqNvi">
                            <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                          </node>
                          <node concept="30H73N" id="Pq8sy4j0zV" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3uibUv" id="Pq8sy4j1f_" role="1tU5fm">
                  <ref role="3uigEE" to="g9lr:~PCollectionTuple" resolve="PCollectionTuple" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="raruj" id="4o2stJ2geXl" role="lGtFl" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="4o2stJ243sw">
    <property role="TrG5h" value="reduce_Window" />
    <property role="3GE5qa" value="Window" />
    <ref role="3gUMe" to="qyjc:4o2stJ22OSe" resolve="Window" />
    <node concept="3clFbS" id="4o2stJ243sJ" role="13RCb5">
      <node concept="3cpWs8" id="4o2stJ243sT" role="3cqZAp">
        <node concept="3cpWsn" id="4o2stJ243sU" role="3cpWs9">
          <property role="TrG5h" value="window" />
          <node concept="3uibUv" id="4o2stJ243sV" role="1tU5fm">
            <ref role="3uigEE" to="hqtk:~BoundedWindow" resolve="BoundedWindow" />
          </node>
          <node concept="2ShNRf" id="4o2stJ243td" role="33vP2m">
            <node concept="1pGfFk" id="4o2stJ245Qg" role="2ShVmc">
              <ref role="37wK5l" to="hqtk:~IntervalWindow.&lt;init&gt;(org.joda.time.Instant,org.joda.time.Instant)" resolve="IntervalWindow" />
              <node concept="2OqwBi" id="4o2stJ24fVT" role="37wK5m">
                <node concept="2ShNRf" id="4o2stJ245Rb" role="2Oq$k0">
                  <node concept="1pGfFk" id="4o2stJ246gk" role="2ShVmc">
                    <ref role="37wK5l" to="w08f:~Instant.&lt;init&gt;(long)" resolve="Instant" />
                    <node concept="3cmrfG" id="4o2stJ246hn" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="4o2stJ24gfQ" role="2OqNvi">
                  <ref role="37wK5l" to="w08f:~Instant.plus(org.joda.time.ReadableDuration)" resolve="plus" />
                  <node concept="2YIFZM" id="4o2stJ24gzw" role="37wK5m">
                    <ref role="37wK5l" to="w08f:~Duration.millis(long)" resolve="millis" />
                    <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
                    <node concept="3cmrfG" id="4o2stJ24gBF" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="29HgVG" id="Jw6Yu7MRca" role="lGtFl">
                      <node concept="3NFfHV" id="Jw6Yu7MRgh" role="3NFExx">
                        <node concept="3clFbS" id="Jw6Yu7MRgi" role="2VODD2">
                          <node concept="3clFbF" id="Jw6Yu7MRkK" role="3cqZAp">
                            <node concept="2OqwBi" id="Jw6Yu7MRzi" role="3clFbG">
                              <node concept="30H73N" id="Jw6Yu7MRkJ" role="2Oq$k0" />
                              <node concept="3TrEf2" id="Jw6Yu7MROo" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4o2stJ22XF8" resolve="from" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="4o2stJ24gNZ" role="37wK5m">
                <node concept="2ShNRf" id="4o2stJ246jz" role="2Oq$k0">
                  <node concept="1pGfFk" id="4o2stJ246Hu" role="2ShVmc">
                    <ref role="37wK5l" to="w08f:~Instant.&lt;init&gt;(long)" resolve="Instant" />
                    <node concept="3cmrfG" id="4o2stJ246Iy" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="4o2stJ24gWI" role="2OqNvi">
                  <ref role="37wK5l" to="w08f:~Instant.plus(org.joda.time.ReadableDuration)" resolve="plus" />
                  <node concept="2YIFZM" id="4o2stJ24hlX" role="37wK5m">
                    <ref role="37wK5l" to="w08f:~Duration.millis(long)" resolve="millis" />
                    <ref role="1Pybhc" to="w08f:~Duration" resolve="Duration" />
                    <node concept="3cmrfG" id="4o2stJ24hzA" role="37wK5m">
                      <property role="3cmrfH" value="0" />
                    </node>
                    <node concept="29HgVG" id="Jw6Yu7MSd_" role="lGtFl">
                      <node concept="3NFfHV" id="Jw6Yu7MShG" role="3NFExx">
                        <node concept="3clFbS" id="Jw6Yu7MShH" role="2VODD2">
                          <node concept="3clFbF" id="Jw6Yu7MSmb" role="3cqZAp">
                            <node concept="2OqwBi" id="Jw6Yu7MSrD" role="3clFbG">
                              <node concept="30H73N" id="Jw6Yu7MSma" role="2Oq$k0" />
                              <node concept="3TrEf2" id="Jw6Yu7MSuV" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4o2stJ22XFa" resolve="to" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="17Uvod" id="4o2stJ246K5" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="4o2stJ246K6" role="3zH0cK">
              <node concept="3clFbS" id="4o2stJ246K7" role="2VODD2">
                <node concept="3clFbF" id="4o2stJ246Pz" role="3cqZAp">
                  <node concept="2OqwBi" id="4o2stJ2474I" role="3clFbG">
                    <node concept="30H73N" id="4o2stJ246Py" role="2Oq$k0" />
                    <node concept="3TrcHB" id="4o2stJ247iQ" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="4o2stJ24hGT" role="lGtFl">
            <ref role="2rW$FS" node="4o2stJ24hGS" resolve="WindowRef" />
          </node>
        </node>
        <node concept="raruj" id="4o2stJ246K0" role="lGtFl" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="78izV9gX8CT">
    <property role="TrG5h" value="reduce_EventSource" />
    <property role="3GE5qa" value="Source" />
    <ref role="3gUMe" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
    <node concept="3clFbS" id="78izV9gXfNy" role="13RCb5">
      <node concept="3clFbH" id="7MdWNyBgWDR" role="3cqZAp" />
      <node concept="3cpWs8" id="78izV9gXgTV" role="3cqZAp">
        <node concept="3cpWsn" id="78izV9gXgTW" role="3cpWs9">
          <property role="TrG5h" value="source" />
          <node concept="3uibUv" id="78izV9gXgTX" role="1tU5fm">
            <ref role="3uigEE" to="cqpf:~TestStream$Builder" resolve="TestStream.Builder" />
            <node concept="3uibUv" id="78izV9gXgTY" role="11_B2D">
              <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
              <node concept="29HgVG" id="78izV9gXgTZ" role="lGtFl">
                <node concept="3NFfHV" id="78izV9gXgU0" role="3NFExx">
                  <node concept="3clFbS" id="78izV9gXgU1" role="2VODD2">
                    <node concept="3clFbF" id="78izV9gXgU2" role="3cqZAp">
                      <node concept="2OqwBi" id="78izV9gXgU3" role="3clFbG">
                        <node concept="30H73N" id="78izV9gXgU4" role="2Oq$k0" />
                        <node concept="3TrEf2" id="78izV9gXgU5" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:1CpVIWKP9wo" resolve="eventType" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2YIFZM" id="78izV9gXgU6" role="33vP2m">
            <ref role="1Pybhc" to="cqpf:~TestStream" resolve="TestStream" />
            <ref role="37wK5l" to="cqpf:~TestStream.create(org.apache.beam.sdk.coders.Coder)" resolve="create" />
            <node concept="2YIFZM" id="78izV9gXgU7" role="37wK5m">
              <ref role="1Pybhc" to="54yz:~AvroCoder" resolve="AvroCoder" />
              <ref role="37wK5l" to="54yz:~AvroCoder.of(java.lang.Class)" resolve="of" />
              <node concept="3VsKOn" id="78izV9gXgU8" role="37wK5m">
                <ref role="3VsUkX" to="wyt6:~Object" resolve="Object" />
              </node>
              <node concept="29HgVG" id="78izV9gXgU9" role="lGtFl">
                <node concept="3NFfHV" id="78izV9gXgUa" role="3NFExx">
                  <node concept="3clFbS" id="78izV9gXgUb" role="2VODD2">
                    <node concept="3clFbF" id="78izV9gXgUc" role="3cqZAp">
                      <node concept="2OqwBi" id="78izV9gXgUd" role="3clFbG">
                        <node concept="3TrEf2" id="78izV9gXgUe" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:1CpVIWKP9wl" resolve="coder" />
                        </node>
                        <node concept="30H73N" id="78izV9gXgUf" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="17Uvod" id="78izV9gXgUg" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="78izV9gXgUh" role="3zH0cK">
              <node concept="3clFbS" id="78izV9gXgUi" role="2VODD2">
                <node concept="3clFbF" id="78izV9gXgUj" role="3cqZAp">
                  <node concept="3cpWs3" id="78izV9gXgUk" role="3clFbG">
                    <node concept="Xl_RD" id="78izV9gXgUl" role="3uHU7w">
                      <property role="Xl_RC" value="Source" />
                    </node>
                    <node concept="2OqwBi" id="78izV9gXgUm" role="3uHU7B">
                      <node concept="3TrcHB" id="78izV9gXgUn" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                      </node>
                      <node concept="30H73N" id="78izV9gXgUo" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="78izV9gXgUp" role="lGtFl">
            <ref role="2rW$FS" node="1CpVIWKTt2l" resolve="SourceRef" />
          </node>
        </node>
        <node concept="raruj" id="78izV9gXhjT" role="lGtFl" />
      </node>
      <node concept="3cpWs8" id="78izV9gXisj" role="3cqZAp">
        <node concept="3cpWsn" id="78izV9gXisk" role="3cpWs9">
          <property role="TrG5h" value="testInput" />
          <node concept="3uibUv" id="78izV9gXisl" role="1tU5fm">
            <ref role="3uigEE" to="cqpf:~TestStream" resolve="TestStream" />
            <node concept="3uibUv" id="78izV9gXism" role="11_B2D">
              <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
              <node concept="29HgVG" id="78izV9gXisn" role="lGtFl">
                <node concept="3NFfHV" id="78izV9gXiso" role="3NFExx">
                  <node concept="3clFbS" id="78izV9gXisp" role="2VODD2">
                    <node concept="3clFbF" id="78izV9gXisq" role="3cqZAp">
                      <node concept="2OqwBi" id="78izV9gXisr" role="3clFbG">
                        <node concept="3TrEf2" id="78izV9gXiss" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:1CpVIWKP9wo" resolve="eventType" />
                        </node>
                        <node concept="30H73N" id="78izV9gXist" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="17Uvod" id="78izV9gXisu" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="78izV9gXisv" role="3zH0cK">
              <node concept="3clFbS" id="78izV9gXisw" role="2VODD2">
                <node concept="3clFbF" id="78izV9gXisx" role="3cqZAp">
                  <node concept="3cpWs3" id="78izV9gXisy" role="3clFbG">
                    <node concept="Xl_RD" id="78izV9gXisz" role="3uHU7w">
                      <property role="Xl_RC" value="Input" />
                    </node>
                    <node concept="2OqwBi" id="78izV9gXis$" role="3uHU7B">
                      <node concept="30H73N" id="78izV9gXis_" role="2Oq$k0" />
                      <node concept="3TrcHB" id="78izV9gXisA" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="78izV9gXisB" role="lGtFl">
            <ref role="2rW$FS" node="78izV9gWuw_" resolve="InputRef" />
          </node>
        </node>
        <node concept="raruj" id="78izV9gXiZ_" role="lGtFl" />
      </node>
      <node concept="3clFbF" id="78izV9gXjuQ" role="3cqZAp">
        <node concept="37vLTI" id="78izV9gXjuR" role="3clFbG">
          <node concept="2OqwBi" id="78izV9gXjuS" role="37vLTx">
            <node concept="37vLTw" id="78izV9gXjuT" role="2Oq$k0">
              <ref role="3cqZAo" node="78izV9gXgTW" resolve="source" />
            </node>
            <node concept="liA8E" id="78izV9gXjuU" role="2OqNvi">
              <ref role="37wK5l" to="cqpf:~TestStream$Builder.addElements(java.lang.Object,java.lang.Object...)" resolve="addElements" />
              <node concept="2ShNRf" id="78izV9gXjuV" role="37wK5m">
                <node concept="1pGfFk" id="78izV9gXjuW" role="2ShVmc">
                  <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="78izV9gXjuX" role="37vLTJ">
            <ref role="3cqZAo" node="78izV9gXgTW" resolve="source" />
          </node>
        </node>
        <node concept="raruj" id="78izV9gXmSc" role="lGtFl" />
        <node concept="1WS0z7" id="78izV9gXmSe" role="lGtFl">
          <node concept="3JmXsc" id="78izV9gXmSh" role="3Jn$fo">
            <node concept="3clFbS" id="78izV9gXmSi" role="2VODD2">
              <node concept="3clFbF" id="78izV9gXmSo" role="3cqZAp">
                <node concept="2OqwBi" id="78izV9gXmSj" role="3clFbG">
                  <node concept="3Tsc0h" id="78izV9gXmSm" role="2OqNvi">
                    <ref role="3TtcxE" to="qyjc:RoIJycd2v9" resolve="processes" />
                  </node>
                  <node concept="30H73N" id="78izV9gXmSn" role="2Oq$k0" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1sPUBX" id="78izV9gXjv5" role="lGtFl">
          <ref role="v9R2y" node="1CpVIWKTjvh" resolve="PipeProcess" />
        </node>
      </node>
      <node concept="3clFbH" id="5p3IOnLNkPa" role="3cqZAp">
        <node concept="raruj" id="5p3IOnLNlms" role="lGtFl" />
      </node>
      <node concept="3cpWs8" id="5p3IOnLO0sx" role="3cqZAp">
        <node concept="3cpWsn" id="5p3IOnLO0sy" role="3cpWs9">
          <property role="TrG5h" value="pipeline" />
          <node concept="3uibUv" id="5p3IOnLO0sz" role="1tU5fm">
            <ref role="3uigEE" to="5ssx:~Pipeline" resolve="Pipeline" />
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="5p3IOnLNWEZ" role="3cqZAp">
        <node concept="3cpWsn" id="5p3IOnLNWF0" role="3cpWs9">
          <property role="TrG5h" value="name" />
          <node concept="3uibUv" id="5p3IOnLNWEX" role="1tU5fm">
            <ref role="3uigEE" to="g9lr:~PCollection" resolve="PCollection" />
            <node concept="3uibUv" id="5p3IOnLNXgN" role="11_B2D">
              <ref role="3uigEE" to="wyt6:~Object" resolve="Object" />
              <node concept="29HgVG" id="5p3IOnLOyRQ" role="lGtFl">
                <node concept="3NFfHV" id="5p3IOnLOyRR" role="3NFExx">
                  <node concept="3clFbS" id="5p3IOnLOyRS" role="2VODD2">
                    <node concept="3clFbF" id="Jw6Yu7RV9w" role="3cqZAp">
                      <node concept="2OqwBi" id="Jw6Yu7S1q6" role="3clFbG">
                        <node concept="1PxgMI" id="Jw6Yu7S0gq" role="2Oq$k0">
                          <node concept="chp4Y" id="Jw6Yu7S0Jm" role="3oSUPX">
                            <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                          </node>
                          <node concept="2OqwBi" id="Jw6Yu7RXyv" role="1m5AlR">
                            <node concept="30H73N" id="Jw6Yu7RX7p" role="2Oq$k0" />
                            <node concept="1mfA1w" id="Jw6Yu7RYbE" role="2OqNvi" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="Jw6Yu7S2bI" role="2OqNvi">
                          <ref role="3Tt5mk" to="qyjc:1CpVIWKP9wo" resolve="eventType" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5p3IOnLO1y3" role="33vP2m">
            <node concept="37vLTw" id="5p3IOnLO1p_" role="2Oq$k0">
              <ref role="3cqZAo" node="5p3IOnLO0sy" resolve="pipeline" />
              <node concept="1ZhdrF" id="5p3IOnLOMjH" role="lGtFl">
                <property role="2qtEX8" value="variableDeclaration" />
                <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                <node concept="3$xsQk" id="5p3IOnLOMjI" role="3$ytzL">
                  <node concept="3clFbS" id="5p3IOnLOMjJ" role="2VODD2">
                    <node concept="3clFbF" id="5p3IOnLON1g" role="3cqZAp">
                      <node concept="2OqwBi" id="5p3IOnLONbM" role="3clFbG">
                        <node concept="1iwH7S" id="5p3IOnLON1f" role="2Oq$k0" />
                        <node concept="1iwH70" id="5p3IOnLONhc" role="2OqNvi">
                          <ref role="1iwH77" node="5T7UbTDiT7z" resolve="PipelineRef" />
                          <node concept="2OqwBi" id="5Q9IfCXJfHf" role="1iwH7V">
                            <node concept="30H73N" id="5Q9IfCXJcgX" role="2Oq$k0" />
                            <node concept="3TrEf2" id="5Q9IfCXJfWY" role="2OqNvi">
                              <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="liA8E" id="5p3IOnLO1Ed" role="2OqNvi">
              <ref role="37wK5l" to="5ssx:~Pipeline.apply(org.apache.beam.sdk.transforms.PTransform)" resolve="apply" />
              <node concept="37vLTw" id="5p3IOnLO1EN" role="37wK5m">
                <ref role="3cqZAo" node="78izV9gXisk" resolve="testInput" />
                <node concept="1ZhdrF" id="5p3IOnLOO3t" role="lGtFl">
                  <property role="2qtEX8" value="variableDeclaration" />
                  <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                  <node concept="3$xsQk" id="5p3IOnLOO3u" role="3$ytzL">
                    <node concept="3clFbS" id="5p3IOnLOO3v" role="2VODD2">
                      <node concept="3clFbF" id="5p3IOnLOOLi" role="3cqZAp">
                        <node concept="2OqwBi" id="5p3IOnLOOVO" role="3clFbG">
                          <node concept="1iwH7S" id="5p3IOnLOOLh" role="2Oq$k0" />
                          <node concept="1iwH70" id="5p3IOnLOP1e" role="2OqNvi">
                            <ref role="1iwH77" node="78izV9gWuw_" resolve="InputRef" />
                            <node concept="1PxgMI" id="Jw6Yu7S6X1" role="1iwH7V">
                              <node concept="chp4Y" id="Jw6Yu7S6Z_" role="3oSUPX">
                                <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                              </node>
                              <node concept="2OqwBi" id="Jw6Yu7S6$D" role="1m5AlR">
                                <node concept="30H73N" id="Jw6Yu7S6pj" role="2Oq$k0" />
                                <node concept="1mfA1w" id="Jw6Yu7S6Mx" role="2OqNvi" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="17Uvod" id="5p3IOnLOKlA" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="5p3IOnLOKlB" role="3zH0cK">
              <node concept="3clFbS" id="5p3IOnLOKlC" role="2VODD2">
                <node concept="3clFbF" id="5p3IOnLOKOu" role="3cqZAp">
                  <node concept="3cpWs3" id="5p3IOnLOM4V" role="3clFbG">
                    <node concept="Xl_RD" id="5p3IOnLOM51" role="3uHU7w">
                      <property role="Xl_RC" value="Input" />
                    </node>
                    <node concept="3cpWs3" id="5p3IOnLPGxS" role="3uHU7B">
                      <node concept="Xl_RD" id="5p3IOnLPGxY" role="3uHU7w">
                        <property role="Xl_RC" value="_" />
                      </node>
                      <node concept="3cpWs3" id="5p3IOnLPE1N" role="3uHU7B">
                        <node concept="3cpWs3" id="5p3IOnLPFXb" role="3uHU7B">
                          <node concept="Xl_RD" id="5p3IOnLPFXh" role="3uHU7w">
                            <property role="Xl_RC" value="_" />
                          </node>
                          <node concept="2OqwBi" id="5p3IOnLOLoY" role="3uHU7B">
                            <node concept="1PxgMI" id="Jw6Yu7S3r8" role="2Oq$k0">
                              <node concept="chp4Y" id="Jw6Yu7S3u7" role="3oSUPX">
                                <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                              </node>
                              <node concept="2OqwBi" id="5p3IOnLOL0J" role="1m5AlR">
                                <node concept="30H73N" id="5p3IOnLOKOt" role="2Oq$k0" />
                                <node concept="1mfA1w" id="Jw6Yu7S3gp" role="2OqNvi" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="Jw6Yu7S4dM" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="5p3IOnLPEMI" role="3uHU7w">
                          <node concept="2OqwBi" id="5p3IOnLPEjA" role="2Oq$k0">
                            <node concept="30H73N" id="5p3IOnLPE25" role="2Oq$k0" />
                            <node concept="3TrEf2" id="Jw6Yu7S588" role="2OqNvi">
                              <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                            </node>
                          </node>
                          <node concept="3TrcHB" id="Jw6Yu7S5Mm" role="2OqNvi">
                            <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2ZBi8u" id="6MwzKcXWRdI" role="lGtFl">
            <ref role="2rW$FS" node="4o2stJ1YWa9" resolve="InputCollectionRef" />
          </node>
        </node>
        <node concept="raruj" id="5p3IOnLO3ZB" role="lGtFl" />
        <node concept="1WS0z7" id="5p3IOnLO3ZD" role="lGtFl">
          <node concept="3JmXsc" id="5p3IOnLO3ZG" role="3Jn$fo">
            <node concept="3clFbS" id="5p3IOnLO3ZH" role="2VODD2">
              <node concept="3clFbF" id="Jw6Yu7RG2k" role="3cqZAp">
                <node concept="2OqwBi" id="Jw6Yu7RHba" role="3clFbG">
                  <node concept="30H73N" id="Jw6Yu7RG2j" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="Jw6Yu7RI03" role="2OqNvi">
                    <ref role="3TtcxE" to="qyjc:Jw6Yu7OXjr" resolve="attachTo" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="5p3IOnLNlmu" role="3cqZAp">
        <node concept="raruj" id="5p3IOnLNm_8" role="lGtFl" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="575gSEdb1hF">
    <property role="TrG5h" value="reduce_RunPipeline" />
    <ref role="3gUMe" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
    <node concept="3clFbS" id="575gSEdb1Cd" role="13RCb5">
      <node concept="3cpWs8" id="575gSEdb1iV" role="3cqZAp">
        <node concept="3cpWsn" id="575gSEdb1iW" role="3cpWs9">
          <property role="TrG5h" value="pipeline" />
          <node concept="3uibUv" id="575gSEdb1iX" role="1tU5fm">
            <ref role="3uigEE" to="5ssx:~Pipeline" resolve="Pipeline" />
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="575gSEdb1iY" role="3cqZAp">
        <node concept="2OqwBi" id="575gSEdb1iZ" role="3clFbG">
          <node concept="2OqwBi" id="575gSEdb1j0" role="2Oq$k0">
            <node concept="37vLTw" id="575gSEdb1j1" role="2Oq$k0">
              <ref role="3cqZAo" node="575gSEdb1iW" resolve="pipeline" />
              <node concept="1ZhdrF" id="575gSEdb1j2" role="lGtFl">
                <property role="2qtEX8" value="variableDeclaration" />
                <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                <node concept="3$xsQk" id="575gSEdb1j3" role="3$ytzL">
                  <node concept="3clFbS" id="575gSEdb1j4" role="2VODD2">
                    <node concept="3clFbF" id="575gSEdb1j5" role="3cqZAp">
                      <node concept="2OqwBi" id="575gSEdb1j6" role="3clFbG">
                        <node concept="1iwH7S" id="575gSEdb1j7" role="2Oq$k0" />
                        <node concept="1iwH70" id="575gSEdb1j8" role="2OqNvi">
                          <ref role="1iwH77" node="5T7UbTDiT7z" resolve="PipelineRef" />
                          <node concept="2OqwBi" id="4vp2seWfkfz" role="1iwH7V">
                            <node concept="2OqwBi" id="575gSEdb1j9" role="2Oq$k0">
                              <node concept="30H73N" id="575gSEdb1ja" role="2Oq$k0" />
                              <node concept="3TrEf2" id="4vp2seWfk3U" role="2OqNvi">
                                <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4vp2seWfkqQ" role="2OqNvi">
                              <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="liA8E" id="575gSEdb1jc" role="2OqNvi">
              <ref role="37wK5l" to="5ssx:~Pipeline.run()" resolve="run" />
            </node>
          </node>
          <node concept="liA8E" id="575gSEdb1jd" role="2OqNvi">
            <ref role="37wK5l" to="5ssx:~PipelineResult.waitUntilFinish()" resolve="waitUntilFinish" />
          </node>
        </node>
        <node concept="raruj" id="575gSEdb1je" role="lGtFl" />
        <node concept="1W57fq" id="575gSEdb1jf" role="lGtFl">
          <node concept="3IZrLx" id="575gSEdb1jg" role="3IZSJc">
            <node concept="3clFbS" id="575gSEdb1jh" role="2VODD2">
              <node concept="3clFbF" id="575gSEdb1ji" role="3cqZAp">
                <node concept="2OqwBi" id="575gSEdb1jj" role="3clFbG">
                  <node concept="3TrcHB" id="Jw6Yu7Sm2M" role="2OqNvi">
                    <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                  </node>
                  <node concept="30H73N" id="575gSEdb1jm" role="2Oq$k0" />
                </node>
              </node>
            </node>
          </node>
          <node concept="gft3U" id="575gSEdb1jo" role="UU_$l">
            <node concept="3clFbF" id="575gSEdb1jp" role="gfFT$">
              <node concept="2OqwBi" id="575gSEdb1jq" role="3clFbG">
                <node concept="37vLTw" id="575gSEdb1jr" role="2Oq$k0">
                  <ref role="3cqZAo" node="575gSEdb1iW" resolve="pipeline" />
                  <node concept="1ZhdrF" id="575gSEdb1js" role="lGtFl">
                    <property role="2qtEX8" value="variableDeclaration" />
                    <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
                    <node concept="3$xsQk" id="575gSEdb1jt" role="3$ytzL">
                      <node concept="3clFbS" id="575gSEdb1ju" role="2VODD2">
                        <node concept="3clFbF" id="4vp2seWc6E5" role="3cqZAp">
                          <node concept="2OqwBi" id="4vp2seWc6OB" role="3clFbG">
                            <node concept="1iwH7S" id="4vp2seWc6E4" role="2Oq$k0" />
                            <node concept="1iwH70" id="4vp2seWc6U1" role="2OqNvi">
                              <ref role="1iwH77" node="5T7UbTDiT7z" resolve="PipelineRef" />
                              <node concept="2OqwBi" id="4vp2seWfkZU" role="1iwH7V">
                                <node concept="2OqwBi" id="4vp2seWc7hX" role="2Oq$k0">
                                  <node concept="30H73N" id="4vp2seWc73f" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="4vp2seWfkOK" role="2OqNvi">
                                    <ref role="3Tt5mk" to="qyjc:4vp2seWeOvG" resolve="ref" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vp2seWflby" role="2OqNvi">
                                  <ref role="3Tt5mk" to="qyjc:7MdWNyBagaw" resolve="ref" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="575gSEdb1jA" role="2OqNvi">
                  <ref role="37wK5l" to="5ssx:~Pipeline.run()" resolve="run" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

