<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:49623eb5-8fcc-4b98-b3d5-9ff5dd2c7228(hfu.mps.StreamSysT.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="13" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpen" ref="r:00000000-0000-4000-0000-011c895902c3(jetbrains.mps.baseLanguage.editor)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpco" ref="r:00000000-0000-4000-0000-011c89590284(jetbrains.mps.lang.core.editor)" />
    <import index="qyjc" ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1597643335227097138" name="jetbrains.mps.lang.editor.structure.QueryFunctionParameter_TransformationMenu_node" flags="ng" index="7Obwk" />
      <concept id="6516520003787916624" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Condition" flags="ig" index="27VH4U" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
        <child id="1140524464359" name="emptyCellModel" index="2czzBI" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1142886221719" name="jetbrains.mps.lang.editor.structure.QueryFunction_NodeCondition" flags="in" index="pkWqt" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="6718020819487620876" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Default" flags="ng" index="A1WHr" />
      <concept id="6718020819487620873" name="jetbrains.mps.lang.editor.structure.TransformationMenuReference_Named" flags="ng" index="A1WHu">
        <reference id="6718020819487620874" name="menu" index="A1WHt" />
      </concept>
      <concept id="3473224453637651916" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform_PlaceInCellHolder" flags="ng" index="CtIbL">
        <property id="3473224453637651917" name="placeInCell" index="CtIbK" />
      </concept>
      <concept id="1638911550608571617" name="jetbrains.mps.lang.editor.structure.TransformationMenu_Default" flags="ng" index="IW6AY" />
      <concept id="1638911550608610798" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_Execute" flags="ig" index="IWg2L" />
      <concept id="1638911550608610278" name="jetbrains.mps.lang.editor.structure.TransformationMenuPart_Action" flags="ng" index="IWgqT">
        <child id="6202297022026447496" name="canExecuteFunction" index="2jiSrf" />
        <child id="1638911550608610281" name="executeFunction" index="IWgqQ" />
        <child id="5692353713941573325" name="textFunction" index="1hCUd6" />
      </concept>
      <concept id="1078938745671" name="jetbrains.mps.lang.editor.structure.EditorComponentDeclaration" flags="ig" index="PKFIW" />
      <concept id="1078939183254" name="jetbrains.mps.lang.editor.structure.CellModel_Component" flags="sg" stub="3162947552742194261" index="PMmxH">
        <reference id="1078939183255" name="editorComponent" index="PMmxG" />
      </concept>
      <concept id="4323500428121233431" name="jetbrains.mps.lang.editor.structure.EditorCellId" flags="ng" index="2SqB2G" />
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styles" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1186414976055" name="jetbrains.mps.lang.editor.structure.DrawBorderStyleClassItem" flags="ln" index="VPXOz" />
      <concept id="1630016958697718209" name="jetbrains.mps.lang.editor.structure.IMenuReference_Default" flags="ng" index="2Z_bC8">
        <reference id="1630016958698373342" name="concept" index="2ZyFGn" />
      </concept>
      <concept id="1630016958697344083" name="jetbrains.mps.lang.editor.structure.IMenu_Concept" flags="ng" index="2ZABuq">
        <reference id="6591946374543067572" name="conceptDeclaration" index="aqKnT" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1233759184865" name="jetbrains.mps.lang.editor.structure.PunctuationRightStyleClassItem" flags="ln" index="11LMrY" />
      <concept id="3383245079137382180" name="jetbrains.mps.lang.editor.structure.StyleClass" flags="ig" index="14StLt" />
      <concept id="2896773699153795590" name="jetbrains.mps.lang.editor.structure.TransformationLocation_SideTransform" flags="ng" index="3cWJ9i">
        <child id="3473224453637651919" name="placeInCell" index="CtIbM" />
      </concept>
      <concept id="5692353713941573329" name="jetbrains.mps.lang.editor.structure.QueryFunction_TransformationMenu_ActionLabelText" flags="ig" index="1hCUdq" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="1223386653097" name="jetbrains.mps.lang.editor.structure.StrikeOutStyleSheet" flags="ln" index="3nxI2P" />
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1219226236603" name="jetbrains.mps.lang.editor.structure.DrawBracketsStyleClassItem" flags="ln" index="3vyZuw" />
      <concept id="1215007762405" name="jetbrains.mps.lang.editor.structure.FloatStyleClassItem" flags="ln" index="3$6MrZ">
        <property id="1215007802031" name="value" index="3$6WeP" />
      </concept>
      <concept id="1215007883204" name="jetbrains.mps.lang.editor.structure.PaddingLeftStyleClassItem" flags="ln" index="3$7fVu" />
      <concept id="1215007897487" name="jetbrains.mps.lang.editor.structure.PaddingRightStyleClassItem" flags="ln" index="3$7jql" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <property id="1140114345053" name="allowEmptyText" index="1O74Pk" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389214265" name="jetbrains.mps.lang.editor.structure.EditorCellModel" flags="ng" index="3EYTF0">
        <property id="1130859485024" name="attractsFocus" index="1cu_pB" />
        <reference id="1139959269582" name="actionMap" index="1ERwB7" />
        <child id="1142887637401" name="renderingCondition" index="pqm2j" />
        <child id="4323500428121274054" name="id" index="2SqHTX" />
        <child id="4202667662392416064" name="transformationMenu" index="3vIgyS" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <property id="1160590353935" name="usesFolding" index="S$Qs1" />
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="7723470090030138869" name="foldedCellModel" index="AHCbl" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="5624877018228267058" name="jetbrains.mps.lang.editor.structure.ITransformationMenu" flags="ng" index="3INCJE">
        <child id="1638911550608572412" name="sections" index="IW6Ez" />
      </concept>
      <concept id="5624877018228264944" name="jetbrains.mps.lang.editor.structure.TransformationMenuContribution" flags="ng" index="3INDKC">
        <child id="6718020819489956031" name="menuReference" index="AmTjC" />
      </concept>
      <concept id="7980428675268276156" name="jetbrains.mps.lang.editor.structure.TransformationMenuSection" flags="ng" index="1Qtc8_">
        <child id="7980428675268276157" name="locations" index="1Qtc8$" />
        <child id="7980428675268276159" name="parts" index="1Qtc8A" />
      </concept>
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1204851882688" name="jetbrains.mps.lang.smodel.structure.LinkRefQualifier" flags="ng" index="26LbJo">
        <reference id="1204851882689" name="link" index="26LbJp" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC" />
      <concept id="7835263205327057228" name="jetbrains.mps.lang.smodel.structure.Node_GetChildrenAndChildAttributesOperation" flags="ng" index="Bykcj" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="5168775467716640652" name="jetbrains.mps.lang.smodel.structure.OperationParm_LinkQualifier" flags="ng" index="1aIX9F">
        <child id="5168775467716640653" name="linkQualifier" index="1aIX9E" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1139867745658" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithNewOperation" flags="nn" index="1_qnLN">
        <reference id="1139867957129" name="concept" index="1_rbq0" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="24kQdi" id="5T7UbTDhmXO">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="1XX52x" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
    <node concept="3EZMnI" id="5T7UbTDhoWA" role="2wV5jI">
      <node concept="3F0ifn" id="5T7UbTDhoWH" role="3EZMnx">
        <property role="3F0ifm" value="Pipeline" />
      </node>
      <node concept="3F0A7n" id="5T7UbTDhoWN" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="5T7UbTDhoWD" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1CpVIWKNcrl">
    <property role="3GE5qa" value="SourceProcess" />
    <ref role="1XX52x" to="qyjc:1CpVIWKKzQq" resolve="AdvanceWatermark" />
    <node concept="3EZMnI" id="1CpVIWKNcrt" role="2wV5jI">
      <node concept="3F0ifn" id="1CpVIWKNcrE" role="3EZMnx">
        <property role="3F0ifm" value="Advance Watermark" />
      </node>
      <node concept="3F0A7n" id="QA4204BsDm" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:QA4204BsDk" resolve="advanceOption" />
      </node>
      <node concept="3F0ifn" id="QA4204BsDe" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F1sOY" id="4o2stJ22OSO" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:4o2stJ22OSx" resolve="time" />
      </node>
      <node concept="l2Vlx" id="1CpVIWKNcrw" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="1CpVIWKNoWv">
    <property role="3GE5qa" value="SourceProcess" />
    <ref role="1XX52x" to="qyjc:1CpVIWKKpFP" resolve="Event" />
    <node concept="3EZMnI" id="5$bixu1fJeX" role="2wV5jI">
      <node concept="3F0ifn" id="7Xz8r_MWd$X" role="3EZMnx">
        <property role="3F0ifm" value="Event:" />
      </node>
      <node concept="2iRfu4" id="5$bixu1fJeY" role="2iSdaV" />
      <node concept="3F1sOY" id="1CpVIWKNoWF" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:1CpVIWKNoWl" resolve="creation" />
      </node>
      <node concept="3EZMnI" id="5$bixu1fJf6" role="3EZMnx">
        <node concept="2iRfu4" id="5$bixu1fJf7" role="2iSdaV" />
        <node concept="3F0ifn" id="5$bixu1fKxE" role="3EZMnx">
          <property role="3F0ifm" value="on TS:" />
        </node>
        <node concept="3F1sOY" id="5$bixu1fKL8" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:5$bixu1fKD1" resolve="timestamp" />
        </node>
        <node concept="pkWqt" id="5$bixu1fJfd" role="pqm2j">
          <node concept="3clFbS" id="5$bixu1fJfe" role="2VODD2">
            <node concept="3clFbF" id="5$bixu1fJja" role="3cqZAp">
              <node concept="2OqwBi" id="5$bixu1fK6_" role="3clFbG">
                <node concept="2OqwBi" id="5$bixu1fJwE" role="2Oq$k0">
                  <node concept="pncrf" id="5$bixu1fJj9" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="5$bixu1fJJ6" role="2OqNvi">
                    <node concept="1xMEDy" id="5$bixu1fJJ8" role="1xVPHs">
                      <node concept="chp4Y" id="5$bixu1fJQJ" role="ri$Ld">
                        <ref role="cht4Q" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3TrcHB" id="5$bixu1fKnE" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:5$bixu1fJ4N" resolve="eventsWithTimestamp" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1CpVIWKP9w$">
    <property role="3GE5qa" value="Source" />
    <ref role="1XX52x" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
    <node concept="3EZMnI" id="1CpVIWKP9wG" role="2wV5jI">
      <node concept="3F0ifn" id="7Xz8r_N0oAo" role="3EZMnx">
        <property role="3F0ifm" value="Source" />
      </node>
      <node concept="3F0A7n" id="1CpVIWKP9wN" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3EZMnI" id="7Xz8r_N0oC8" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <node concept="l2Vlx" id="7Xz8r_N0oC9" role="2iSdaV" />
        <node concept="3F0ifn" id="7Xz8r_N0oBy" role="3EZMnx">
          <property role="3F0ifm" value="{" />
          <node concept="3mYdg7" id="7Xz8r_N0DOk" role="3F10Kt">
            <property role="1413C4" value="source" />
          </node>
        </node>
        <node concept="3F0ifn" id="7Xz8r_N0oDZ" role="3EZMnx">
          <property role="3F0ifm" value="takes:" />
          <node concept="pVoyu" id="7Xz8r_N0oE3" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_N0oLG" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="7Xz8r_N0oEa" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:1CpVIWKP9wo" resolve="eventType" />
        </node>
        <node concept="3F0ifn" id="7Xz8r_N0oEm" role="3EZMnx">
          <property role="3F0ifm" value="coder:" />
          <node concept="pVoyu" id="7Xz8r_N0oEt" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_N0oLJ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="7Xz8r_N0oEB" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:1CpVIWKP9wl" resolve="coder" />
        </node>
        <node concept="3F0ifn" id="Jw6Yu7OXk7" role="3EZMnx">
          <property role="3F0ifm" value="attach to:" />
          <node concept="pVoyu" id="Jw6Yu7OXkJ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="Jw6Yu7OXkL" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="Jw6Yu7OXlu" role="3EZMnx">
          <property role="2czwfO" value="," />
          <ref role="1NtTu8" to="qyjc:Jw6Yu7OXjr" resolve="attachTo" />
          <node concept="l2Vlx" id="Jw6Yu7OXlw" role="2czzBx" />
        </node>
        <node concept="PMmxH" id="7Xz8r_N0oLb" role="3EZMnx">
          <ref role="PMmxG" node="7Xz8r_MXjoe" resolve="SinkSource_ExtendedSerDes" />
          <node concept="pVoyu" id="7Xz8r_N0oLk" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_N0oLM" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3EZMnI" id="7Xz8r_N0oPv" role="3EZMnx">
          <node concept="l2Vlx" id="7Xz8r_N0oPw" role="2iSdaV" />
          <node concept="3EZMnI" id="7Xz8r_N0oPQ" role="3EZMnx">
            <node concept="2iRkQZ" id="7Xz8r_N0oPR" role="2iSdaV" />
            <node concept="3EZMnI" id="7Xz8r_N0oPW" role="3EZMnx">
              <node concept="l2Vlx" id="7Xz8r_N0oPX" role="2iSdaV" />
              <node concept="3F0ifn" id="7Xz8r_N0oQ2" role="3EZMnx">
                <property role="3F0ifm" value="and processes with" />
              </node>
              <node concept="3XFhqQ" id="7Xz8r_N0oQM" role="3EZMnx" />
              <node concept="3F0ifn" id="7Xz8r_N0oQU" role="3EZMnx">
                <property role="3F0ifm" value="---&gt;" />
              </node>
            </node>
            <node concept="3EZMnI" id="7Xz8r_N0oQe" role="3EZMnx">
              <node concept="l2Vlx" id="7Xz8r_N0oQf" role="2iSdaV" />
              <node concept="3XFhqQ" id="7Xz8r_N0oQp" role="3EZMnx" />
              <node concept="3F0ifn" id="7Xz8r_N0oQE" role="3EZMnx">
                <property role="3F0ifm" value="timestamp:" />
              </node>
              <node concept="3F0A7n" id="7Xz8r_N0oQy" role="3EZMnx">
                <ref role="1NtTu8" to="qyjc:5$bixu1fJ4N" resolve="eventsWithTimestamp" />
              </node>
            </node>
          </node>
          <node concept="3F2HdR" id="7Xz8r_N0oRd" role="3EZMnx">
            <ref role="1NtTu8" to="qyjc:RoIJycd2v9" resolve="processes" />
            <node concept="3vyZuw" id="7Xz8r_N0oRv" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="3$7fVu" id="7Xz8r_N0oRw" role="3F10Kt">
              <property role="3$6WeP" value="5" />
            </node>
            <node concept="3$7jql" id="7Xz8r_N0oRx" role="3F10Kt">
              <property role="3$6WeP" value="5" />
            </node>
            <node concept="2iRkQZ" id="7Xz8r_N0oRg" role="2czzBx" />
          </node>
          <node concept="pVoyu" id="7Xz8r_N0oPL" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_N0oPN" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="7Xz8r_N0DNI" role="3EZMnx">
          <property role="3F0ifm" value="}" />
          <node concept="3mYdg7" id="7Xz8r_N0DOp" role="3F10Kt">
            <property role="1413C4" value="source" />
          </node>
          <node concept="pVoyu" id="7Xz8r_N0DOi" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="7Xz8r_N0DOs" role="AHCbl">
          <property role="3F0ifm" value="{...}" />
          <ref role="1k5W1q" to="tpen:4vxLnq9T43C" resolve="FoldedCell" />
        </node>
      </node>
      <node concept="l2Vlx" id="1CpVIWKP9wJ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RoIJycg4Iq">
    <property role="3GE5qa" value="Source" />
    <ref role="1XX52x" to="qyjc:1CpVIWKRvId" resolve="EventSourceRef" />
    <node concept="1iCGBv" id="RoIJycg4Is" role="2wV5jI">
      <ref role="1NtTu8" to="qyjc:RoIJycd2v7" resolve="ref" />
      <node concept="1sVBvm" id="RoIJycg4Iu" role="1sWHZn">
        <node concept="3F0A7n" id="RoIJycg4I_" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="RoIJycgdhh">
    <property role="3GE5qa" value="Sink" />
    <ref role="1XX52x" to="qyjc:1CpVIWKRvIg" resolve="EventSinkRef" />
    <node concept="1iCGBv" id="RoIJycgdhj" role="2wV5jI">
      <ref role="1NtTu8" to="qyjc:RoIJycd2vh" resolve="ref" />
      <node concept="1sVBvm" id="RoIJycgdhl" role="1sWHZn">
        <node concept="3F0A7n" id="RoIJycgdhs" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4o2stJ20FSH">
    <property role="3GE5qa" value="Sink" />
    <ref role="1XX52x" to="qyjc:1CpVIWKKpwu" resolve="EventSink" />
    <node concept="3EZMnI" id="4o2stJ20FSJ" role="2wV5jI">
      <node concept="3F0ifn" id="1SqhhvLZL4b" role="3EZMnx">
        <property role="3F0ifm" value="Sink" />
      </node>
      <node concept="3F0A7n" id="4o2stJ20FSQ" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3EZMnI" id="7Xz8r_MX$BI" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <node concept="VPM3Z" id="7Xz8r_MX$BK" role="3F10Kt" />
        <node concept="3F0ifn" id="7Xz8r_MX$D4" role="3EZMnx">
          <property role="3F0ifm" value="{" />
          <node concept="3mYdg7" id="7Xz8r_MYGT7" role="3F10Kt">
            <property role="1413C4" value="sink" />
          </node>
        </node>
        <node concept="3F0ifn" id="7Xz8r_MX$Dw" role="3EZMnx">
          <property role="3F0ifm" value="receives:" />
          <node concept="pVoyu" id="7Xz8r_MX$DB" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_MX$T1" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="7Xz8r_MX$DL" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:4o2stJ20FRT" resolve="type" />
        </node>
        <node concept="PMmxH" id="7Xz8r_MX_E0" role="3EZMnx">
          <ref role="PMmxG" node="7Xz8r_MXjoe" resolve="SinkSource_ExtendedSerDes" />
          <node concept="pVoyu" id="7Xz8r_MX_Ea" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_MX_Ec" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="7Xz8r_MX_RO" role="3EZMnx">
          <property role="3F0ifm" value="Coder:" />
          <node concept="pVoyu" id="7Xz8r_MX_RZ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_MX_S1" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="7Xz8r_MX_Sh" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:5$bixu1eN9b" resolve="coder" />
        </node>
        <node concept="3F0ifn" id="7Xz8r_MX_SH" role="3EZMnx">
          <property role="3F0ifm" value="Get from:" />
          <node concept="pVoyu" id="7Xz8r_MX_SW" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="7Xz8r_MX_SY" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F1sOY" id="7Xz8r_MX_Ti" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:1SqhhvLZL3$" resolve="from" />
        </node>
        <node concept="3F0ifn" id="Pq8sy4ipoo" role="3EZMnx">
          <property role="3F0ifm" value="Has multiple outputs:" />
          <node concept="pVoyu" id="Pq8sy4ipoM" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="Pq8sy4ipoO" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0A7n" id="Pq8sy4ippj" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:Pq8sy4ipnW" resolve="multipleOutputs" />
        </node>
        <node concept="3F0ifn" id="7Xz8r_MXA6M" role="3EZMnx">
          <property role="3F0ifm" value="}" />
          <node concept="pVoyu" id="7Xz8r_MXA77" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="3mYdg7" id="7Xz8r_MYGTc" role="3F10Kt">
            <property role="1413C4" value="sink" />
          </node>
        </node>
        <node concept="l2Vlx" id="7Xz8r_MX$BN" role="2iSdaV" />
        <node concept="3F0ifn" id="7Xz8r_MXA79" role="AHCbl">
          <property role="3F0ifm" value="{...}" />
          <ref role="1k5W1q" to="tpen:4vxLnq9T43C" resolve="FoldedCell" />
        </node>
      </node>
      <node concept="l2Vlx" id="4o2stJ20FSM" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4o2stJ22XFl">
    <property role="3GE5qa" value="Window" />
    <ref role="1XX52x" to="qyjc:4o2stJ22OSe" resolve="Window" />
    <node concept="3EZMnI" id="4o2stJ22XFn" role="2wV5jI">
      <node concept="3F0ifn" id="4o2stJ22XFu" role="3EZMnx">
        <property role="3F0ifm" value="Window" />
      </node>
      <node concept="3F0A7n" id="4o2stJ22XKH" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="4o2stJ22XKV" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F1sOY" id="4o2stJ22XF$" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:4o2stJ22XF8" resolve="from" />
      </node>
      <node concept="3F0ifn" id="4o2stJ22XFG" role="3EZMnx">
        <property role="3F0ifm" value="-&gt;" />
      </node>
      <node concept="3F1sOY" id="4o2stJ22XLh" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:4o2stJ22XFa" resolve="to" />
      </node>
      <node concept="l2Vlx" id="4o2stJ22XFq" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="4o2stJ24APC">
    <property role="3GE5qa" value="Validation" />
    <ref role="1XX52x" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
    <node concept="3EZMnI" id="7Xz8r_N1tM8" role="2wV5jI">
      <node concept="VPXOz" id="4o2stJ27nAq" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="3F0ifn" id="4o2stJ28Yn3" role="3EZMnx">
        <property role="3F0ifm" value="Assert:" />
      </node>
      <node concept="l2Vlx" id="7Xz8r_N1tM9" role="2iSdaV" />
      <node concept="3EZMnI" id="4o2stJ24APK" role="3EZMnx">
        <property role="S$Qs1" value="true" />
        <node concept="3F0A7n" id="4o2stJ28Ykl" role="3EZMnx">
          <property role="1$x2rV" value="assertion message" />
          <property role="1O74Pk" value="true" />
          <ref role="1NtTu8" to="qyjc:4o2stJ28Y9V" resolve="description" />
        </node>
        <node concept="3EZMnI" id="4o2stJ274oQ" role="3EZMnx">
          <node concept="2iRkQZ" id="4o2stJ274oR" role="2iSdaV" />
          <node concept="3EZMnI" id="4o2stJ274rc" role="3EZMnx">
            <node concept="l2Vlx" id="4o2stJ274rd" role="2iSdaV" />
            <node concept="3F0ifn" id="4o2stJ24APR" role="3EZMnx">
              <property role="3F0ifm" value="expect" />
            </node>
            <node concept="3F0A7n" id="4o2stJ24APX" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:4o2stJ24APt" resolve="condition" />
            </node>
          </node>
          <node concept="3EZMnI" id="4o2stJ274rt" role="3EZMnx">
            <node concept="l2Vlx" id="4o2stJ274ru" role="2iSdaV" />
            <node concept="3XFhqQ" id="4o2stJ274rB" role="3EZMnx" />
            <node concept="3F0ifn" id="4o2stJ274rH" role="3EZMnx">
              <property role="3F0ifm" value="from" />
            </node>
            <node concept="3F1sOY" id="4vp2seWkean" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:4vp2seWke22" resolve="sink" />
            </node>
          </node>
          <node concept="pVoyu" id="4o2stJ28YiU" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3EZMnI" id="7MdWNyBf7Mb" role="3EZMnx">
          <node concept="3EZMnI" id="7MdWNyBf7TG" role="3EZMnx">
            <node concept="l2Vlx" id="7MdWNyBf7TH" role="2iSdaV" />
            <node concept="3F0ifn" id="7MdWNyBf7T8" role="3EZMnx">
              <property role="3F0ifm" value="apply function:" />
            </node>
            <node concept="3F0A7n" id="7MdWNyBf7TZ" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:7MdWNyBf7G4" resolve="applyFunction" />
            </node>
          </node>
          <node concept="2iRkQZ" id="7MdWNyBf7Mc" role="2iSdaV" />
          <node concept="3EZMnI" id="7MdWNyBf9ue" role="3EZMnx">
            <node concept="l2Vlx" id="7MdWNyBf9uf" role="2iSdaV" />
            <node concept="3F0ifn" id="7MdWNyBf9ta" role="3EZMnx">
              <property role="3F0ifm" value="check with:" />
            </node>
            <node concept="3F1sOY" id="7MdWNyBf9s6" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:7MdWNyBf7FZ" resolve="satisfiesFunc" />
            </node>
            <node concept="pkWqt" id="7MdWNyBf9_R" role="pqm2j">
              <node concept="3clFbS" id="7MdWNyBf9_S" role="2VODD2">
                <node concept="3clFbF" id="7MdWNyBf9DS" role="3cqZAp">
                  <node concept="2OqwBi" id="7MdWNyBf9Q$" role="3clFbG">
                    <node concept="pncrf" id="7MdWNyBf9DR" role="2Oq$k0" />
                    <node concept="3TrcHB" id="7MdWNyBf9ZA" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:7MdWNyBf7G4" resolve="applyFunction" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3EZMnI" id="5$bixu1egT0" role="3EZMnx">
            <node concept="3EZMnI" id="5$bixu1egUG" role="3EZMnx">
              <node concept="2iRfu4" id="5$bixu1egUH" role="2iSdaV" />
              <node concept="3F0ifn" id="5$bixu1egUt" role="3EZMnx">
                <property role="3F0ifm" value="define quantity:" />
              </node>
              <node concept="3F0A7n" id="5$bixu1egUS" role="3EZMnx">
                <ref role="1NtTu8" to="qyjc:5$bixu1egpR" resolve="defineQuantity" />
              </node>
            </node>
            <node concept="3F0ifn" id="5$bixu1exE8" role="3EZMnx" />
            <node concept="2iRkQZ" id="5$bixu1egT1" role="2iSdaV" />
            <node concept="3F2HdR" id="4o2stJ24AQf" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:4o2stJ24APr" resolve="expectedEntries" />
              <node concept="2iRkQZ" id="4o2stJ24AQi" role="2czzBx" />
            </node>
            <node concept="3vyZuw" id="5$bixu1exE6" role="3F10Kt">
              <property role="VOm3f" value="true" />
            </node>
            <node concept="pkWqt" id="7MdWNyBf7Ux" role="pqm2j">
              <node concept="3clFbS" id="7MdWNyBf7Uy" role="2VODD2">
                <node concept="3clFbF" id="7MdWNyBf7Yy" role="3cqZAp">
                  <node concept="3fqX7Q" id="7MdWNyBf8xJ" role="3clFbG">
                    <node concept="1eOMI4" id="7MdWNyBf9dU" role="3fr31v">
                      <node concept="2OqwBi" id="7MdWNyBf8xL" role="1eOMHV">
                        <node concept="pncrf" id="7MdWNyBf8xM" role="2Oq$k0" />
                        <node concept="3TrcHB" id="7MdWNyBf8xN" role="2OqNvi">
                          <ref role="3TsBF5" to="qyjc:7MdWNyBf7G4" resolve="applyFunction" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3EZMnI" id="4o2stJ26qL0" role="3EZMnx">
          <node concept="2iRkQZ" id="4o2stJ26qL1" role="2iSdaV" />
          <node concept="3EZMnI" id="4o2stJ26r3a" role="3EZMnx">
            <node concept="l2Vlx" id="4o2stJ26r3b" role="2iSdaV" />
            <node concept="3F0ifn" id="4o2stJ24AQH" role="3EZMnx">
              <property role="3F0ifm" value="in" />
            </node>
            <node concept="3F0A7n" id="4o2stJ24AQX" role="3EZMnx">
              <property role="1O74Pk" value="true" />
              <property role="1$x2rV" value="global window" />
              <ref role="1NtTu8" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
            </node>
          </node>
          <node concept="3EZMnI" id="4o2stJ26r3E" role="3EZMnx">
            <node concept="l2Vlx" id="4o2stJ26r3F" role="2iSdaV" />
            <node concept="3XFhqQ" id="7Xz8r_N21u8" role="3EZMnx" />
            <node concept="3F0ifn" id="4o2stJ24PBB" role="3EZMnx">
              <property role="3F0ifm" value="of" />
            </node>
            <node concept="3F1sOY" id="4vp2seWkehM" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:4vp2seWke2c" resolve="window" />
            </node>
            <node concept="pkWqt" id="4o2stJ26roo" role="pqm2j">
              <node concept="3clFbS" id="4o2stJ26rop" role="2VODD2">
                <node concept="3clFbF" id="4o2stJ26rq8" role="3cqZAp">
                  <node concept="3y3z36" id="4o2stJ26s0p" role="3clFbG">
                    <node concept="10Nm6u" id="4o2stJ26s93" role="3uHU7w" />
                    <node concept="2OqwBi" id="4o2stJ26rAE" role="3uHU7B">
                      <node concept="pncrf" id="4o2stJ26rq7" role="2Oq$k0" />
                      <node concept="3TrcHB" id="4o2stJ26rFp" role="2OqNvi">
                        <ref role="3TsBF5" to="qyjc:4o2stJ24AP6" resolve="assertionScope" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3EZMnI" id="4o2stJ26rmv" role="3EZMnx">
            <node concept="l2Vlx" id="4o2stJ26rmw" role="2iSdaV" />
            <node concept="3F0ifn" id="4o2stJ26rkH" role="3EZMnx">
              <property role="3F0ifm" value="in same order:" />
            </node>
            <node concept="3F0A7n" id="4o2stJ26tx7" role="3EZMnx">
              <ref role="1NtTu8" to="qyjc:4o2stJ29Ywl" resolve="inSameOrder" />
            </node>
          </node>
        </node>
        <node concept="l2Vlx" id="4o2stJ24APN" role="2iSdaV" />
        <node concept="3F0A7n" id="7Xz8r_N1c4u" role="AHCbl">
          <ref role="1NtTu8" to="qyjc:4o2stJ28Y9V" resolve="description" />
          <ref role="1k5W1q" to="tpen:4vxLnq9T43C" resolve="FoldedCell" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5$bixu1d_96">
    <property role="3GE5qa" value="Validation" />
    <ref role="1XX52x" to="qyjc:5$bixu1d_8P" resolve="ExpectedEntry" />
    <node concept="3EZMnI" id="5$bixu1d_9b" role="2wV5jI">
      <node concept="3EZMnI" id="7Xz8r_N1Jwq" role="3EZMnx">
        <node concept="l2Vlx" id="7Xz8r_N1Jwr" role="2iSdaV" />
        <node concept="3F0A7n" id="5$bixu1d_98" role="3EZMnx">
          <property role="39s7Ar" value="true" />
          <ref role="1NtTu8" to="qyjc:5$bixu1d_8W" resolve="count" />
        </node>
        <node concept="3F0ifn" id="5$bixu1d_9k" role="3EZMnx">
          <property role="3F0ifm" value="times" />
        </node>
        <node concept="pkWqt" id="5$bixu1egDk" role="pqm2j">
          <node concept="3clFbS" id="5$bixu1egDl" role="2VODD2">
            <node concept="3clFbF" id="5$bixu1egDr" role="3cqZAp">
              <node concept="2OqwBi" id="5$bixu1egDs" role="3clFbG">
                <node concept="2OqwBi" id="5$bixu1egDt" role="2Oq$k0">
                  <node concept="pncrf" id="5$bixu1egDu" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="5$bixu1egDv" role="2OqNvi">
                    <node concept="1xMEDy" id="5$bixu1egDw" role="1xVPHs">
                      <node concept="chp4Y" id="5$bixu1egDx" role="ri$Ld">
                        <ref role="cht4Q" to="qyjc:4o2stJ24AP5" resolve="StreamAssert" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3TrcHB" id="5$bixu1egDy" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:5$bixu1egpR" resolve="defineQuantity" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F1sOY" id="5$bixu1d_9s" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:5$bixu1d_8Q" resolve="expression" />
      </node>
      <node concept="l2Vlx" id="7Xz8r_N1Jun" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="yQ4gUuO8Zc">
    <property role="3GE5qa" value="Pointer" />
    <ref role="1XX52x" to="qyjc:yQ4gUuO8YZ" resolve="SourcePipelinePointer" />
    <node concept="3EZMnI" id="7Xz8r_MWL68" role="2wV5jI">
      <node concept="3EZMnI" id="7Xz8r_MX2ba" role="3EZMnx">
        <node concept="l2Vlx" id="7Xz8r_MX2bb" role="2iSdaV" />
        <node concept="3F0ifn" id="yQ4gUuRxce" role="3EZMnx">
          <property role="3F0ifm" value="Source:" />
        </node>
        <node concept="3F1sOY" id="4vp2seWjP4g" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:4vp2seWfeuM" resolve="sourceRef" />
        </node>
      </node>
      <node concept="3EZMnI" id="7Xz8r_MX2b$" role="3EZMnx">
        <node concept="l2Vlx" id="7Xz8r_MX2b_" role="2iSdaV" />
        <node concept="3F0ifn" id="yQ4gUuRxcI" role="3EZMnx">
          <property role="3F0ifm" value="Pipeline:" />
        </node>
        <node concept="3F1sOY" id="4vp2seWjP4o" role="3EZMnx">
          <ref role="1NtTu8" to="qyjc:4vp2seWfeuV" resolve="pipelineRef" />
        </node>
      </node>
      <node concept="2iRkQZ" id="7Xz8r_MWL69" role="2iSdaV" />
      <node concept="3vyZuw" id="7Xz8r_MWL6l" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="V5hpn" id="7Xz8r_MCNRY">
    <property role="TrG5h" value="StreamSysT" />
    <property role="3GE5qa" value="Styles" />
    <node concept="14StLt" id="7Xz8r_MCNRZ" role="V601i">
      <property role="TrG5h" value="ChildrenList" />
      <node concept="pj6Ft" id="7Xz8r_MCNTI" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="pVoyu" id="7Xz8r_MCNTJ" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="lj46D" id="7Xz8r_MCNTK" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
    <node concept="14StLt" id="5fBz7OUZOju" role="V601i">
      <property role="TrG5h" value="EmptyCell" />
      <node concept="VPM3Z" id="hT0KxlT" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="Vb9p2" id="hT0KxlU" role="3F10Kt">
        <property role="Vbekb" value="ITALIC" />
      </node>
      <node concept="VPxyj" id="hT0KxlV" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="VechU" id="hT0KxlW" role="3F10Kt">
        <property role="Vb096" value="darkGray" />
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="7Xz8r_MXjoe">
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="SinkSource_ExtendedSerDes" />
    <ref role="1XX52x" to="qyjc:7Xz8r_MZwX0" resolve="IKVSerdes" />
    <node concept="3EZMnI" id="7Xz8r_MX$To" role="2wV5jI">
      <node concept="l2Vlx" id="7Xz8r_MX$Tp" role="2iSdaV" />
      <node concept="3F0ifn" id="7Xz8r_MX$TZ" role="3EZMnx">
        <property role="3F0ifm" value="requires extended (de)serialization:" />
      </node>
      <node concept="3F0A7n" id="7Xz8r_MX$WL" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:5$bixu1ePp9" resolve="extendedSerialization" />
      </node>
      <node concept="3EZMnI" id="7Xz8r_MXjpD" role="3EZMnx">
        <node concept="2iRkQZ" id="7Xz8r_MXjpE" role="2iSdaV" />
        <node concept="3EZMnI" id="7Xz8r_MXjpJ" role="3EZMnx">
          <node concept="l2Vlx" id="7Xz8r_MXjpK" role="2iSdaV" />
          <node concept="3F0ifn" id="7Xz8r_MXjpP" role="3EZMnx">
            <property role="3F0ifm" value="Key (de)serializer:" />
          </node>
          <node concept="3F1sOY" id="7Xz8r_MXjpV" role="3EZMnx">
            <ref role="1NtTu8" to="qyjc:7Xz8r_MZwX1" resolve="keySerdes" />
          </node>
        </node>
        <node concept="3EZMnI" id="7Xz8r_MXjqb" role="3EZMnx">
          <node concept="VPM3Z" id="7Xz8r_MXjqd" role="3F10Kt" />
          <node concept="3F0ifn" id="7Xz8r_MXjqf" role="3EZMnx">
            <property role="3F0ifm" value="Value (de)serializer:" />
          </node>
          <node concept="3F1sOY" id="7Xz8r_MXjqt" role="3EZMnx">
            <ref role="1NtTu8" to="qyjc:7Xz8r_MZwX3" resolve="valueSerdes" />
          </node>
          <node concept="l2Vlx" id="7Xz8r_MXjqg" role="2iSdaV" />
        </node>
        <node concept="pVoyu" id="7Xz8r_MX$UI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="7Xz8r_MX$UK" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pkWqt" id="7Xz8r_MX$X2" role="pqm2j">
          <node concept="3clFbS" id="7Xz8r_MX$X3" role="2VODD2">
            <node concept="3clFbF" id="7Xz8r_MX_1f" role="3cqZAp">
              <node concept="2OqwBi" id="7Xz8r_MZOxX" role="3clFbG">
                <node concept="pncrf" id="7Xz8r_MZOly" role="2Oq$k0" />
                <node concept="3TrcHB" id="7Xz8r_MZOz8" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:5$bixu1ePp9" resolve="extendedSerialization" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="QA4204A5U3">
    <property role="3GE5qa" value="SourceProcess" />
    <ref role="1XX52x" to="qyjc:QA4204A5TJ" resolve="AdvanceProcessingTime" />
    <node concept="3EZMnI" id="QA4204A5U5" role="2wV5jI">
      <node concept="3F0ifn" id="QA4204A5Uc" role="3EZMnx">
        <property role="3F0ifm" value="Advance Processing time" />
      </node>
      <node concept="3F0A7n" id="QA4204BrTq" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:QA4204BsD3" resolve="advanceOption" />
      </node>
      <node concept="3F0ifn" id="QA4204BrTe" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F1sOY" id="QA4204A5Ui" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:QA4204A5TK" resolve="time" />
      </node>
      <node concept="l2Vlx" id="QA4204A5U8" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="7MdWNyBagaE">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="1XX52x" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
    <node concept="1iCGBv" id="7MdWNyBagaG" role="2wV5jI">
      <ref role="1NtTu8" to="qyjc:7MdWNyBagaw" resolve="ref" />
      <node concept="1sVBvm" id="7MdWNyBagaI" role="1sWHZn">
        <node concept="3F0A7n" id="7MdWNyBagaT" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          <ref role="1k5W1q" to="tpen:hFD0yD_" resolve="VariableName" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="575gSEd8U9I">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="1XX52x" to="qyjc:575gSEd5WFG" resolve="RunPipeline" />
    <node concept="3EZMnI" id="575gSEd8U9N" role="2wV5jI">
      <node concept="l2Vlx" id="575gSEd8U9O" role="2iSdaV" />
      <node concept="3F0ifn" id="575gSEd9Z2k" role="3EZMnx">
        <property role="3F0ifm" value="run" />
      </node>
      <node concept="3F1sOY" id="wsIXK0V8Aj" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:4vp2seWeOvG" resolve="ref" />
      </node>
      <node concept="3F0ifn" id="wsIXK0W5aV" role="3EZMnx">
        <property role="3F0ifm" value="pipeline" />
      </node>
      <node concept="PMmxH" id="wsIXK0VKur" role="3EZMnx">
        <ref role="PMmxG" node="wsIXK0REP8" resolve="ICanWait_Component" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="575gSEd9GYI">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="1XX52x" to="qyjc:575gSEd8CXW" resolve="RunAllPipelines" />
    <node concept="3EZMnI" id="575gSEd9Z24" role="2wV5jI">
      <node concept="l2Vlx" id="575gSEd9Z25" role="2iSdaV" />
      <node concept="3F0ifn" id="575gSEd9Z2c" role="3EZMnx">
        <property role="3F0ifm" value="run all pipelines" />
      </node>
      <node concept="PMmxH" id="wsIXK0RH4s" role="3EZMnx">
        <ref role="PMmxG" node="wsIXK0REP8" resolve="ICanWait_Component" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="Pq8sy4bYUv">
    <property role="3GE5qa" value="Sink" />
    <ref role="1XX52x" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
    <node concept="3EZMnI" id="Pq8sy4bYUx" role="2wV5jI">
      <node concept="1iCGBv" id="Pq8sy4bYUC" role="3EZMnx">
        <ref role="1NtTu8" to="qyjc:RoIJycd2vh" resolve="ref" />
        <node concept="1sVBvm" id="Pq8sy4bYUE" role="1sWHZn">
          <node concept="3F0A7n" id="Pq8sy4bYUL" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
        <node concept="11LMrY" id="Pq8sy4bYV8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="Pq8sy4bYUT" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <node concept="11LMrY" id="Pq8sy4bYUZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="11L4FC" id="Pq8sy4bYV4" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6kk4K$6xZI0" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="qyjc:Pq8sy4bYUb" resolve="splitter" />
        <node concept="l2Vlx" id="6kk4K$6xZI2" role="2czzBx" />
      </node>
      <node concept="l2Vlx" id="Pq8sy4bYU$" role="2iSdaV" />
      <node concept="3F0ifn" id="Pq8sy4bYVB" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <node concept="11L4FC" id="Pq8sy4eJuU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="IW6AY" id="Pq8sy4dnSi">
    <property role="3GE5qa" value="Sink" />
    <ref role="aqKnT" to="qyjc:1CpVIWKRvIg" resolve="EventSinkRef" />
    <node concept="1Qtc8_" id="Pq8sy4dnSn" role="IW6Ez">
      <node concept="3cWJ9i" id="Pq8sy4dnSr" role="1Qtc8$">
        <node concept="CtIbL" id="Pq8sy4dnSt" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
      <node concept="IWgqT" id="Pq8sy4dnSz" role="1Qtc8A">
        <node concept="1hCUdq" id="Pq8sy4dnS$" role="1hCUd6">
          <node concept="3clFbS" id="Pq8sy4dnS_" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4dnXs" role="3cqZAp">
              <node concept="Xl_RD" id="Pq8sy4dnXr" role="3clFbG">
                <property role="Xl_RC" value="(" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="Pq8sy4dnSA" role="IWgqQ">
          <node concept="3clFbS" id="Pq8sy4dnSB" role="2VODD2">
            <node concept="3cpWs8" id="Pq8sy4do2R" role="3cqZAp">
              <node concept="3cpWsn" id="Pq8sy4do2U" role="3cpWs9">
                <property role="TrG5h" value="partial" />
                <node concept="3Tqbb2" id="Pq8sy4do2Q" role="1tU5fm">
                  <ref role="ehGHo" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
                </node>
                <node concept="2OqwBi" id="Pq8sy4dods" role="33vP2m">
                  <node concept="7Obwk" id="Pq8sy4do3K" role="2Oq$k0" />
                  <node concept="1_qnLN" id="Pq8sy4doo5" role="2OqNvi">
                    <ref role="1_rbq0" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="Pq8sy4dora" role="3cqZAp">
              <node concept="37vLTI" id="Pq8sy4dp8F" role="3clFbG">
                <node concept="2OqwBi" id="Pq8sy4dpr6" role="37vLTx">
                  <node concept="7Obwk" id="Pq8sy4dpex" role="2Oq$k0" />
                  <node concept="3TrEf2" id="Pq8sy4dTgh" role="2OqNvi">
                    <ref role="3Tt5mk" to="qyjc:RoIJycd2vh" resolve="ref" />
                  </node>
                </node>
                <node concept="2OqwBi" id="Pq8sy4doyJ" role="37vLTJ">
                  <node concept="37vLTw" id="Pq8sy4dor8" role="2Oq$k0">
                    <ref role="3cqZAo" node="Pq8sy4do2U" resolve="partial" />
                  </node>
                  <node concept="3TrEf2" id="Pq8sy4dT2$" role="2OqNvi">
                    <ref role="3Tt5mk" to="qyjc:RoIJycd2vh" resolve="ref" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="27VH4U" id="Pq8sy4dpJE" role="2jiSrf">
          <node concept="3clFbS" id="Pq8sy4dpJF" role="2VODD2">
            <node concept="3clFbF" id="Pq8sy4dpOq" role="3cqZAp">
              <node concept="3fqX7Q" id="Pq8sy4dpOo" role="3clFbG">
                <node concept="1eOMI4" id="Pq8sy4dpWg" role="3fr31v">
                  <node concept="2OqwBi" id="Pq8sy4dqhK" role="1eOMHV">
                    <node concept="7Obwk" id="Pq8sy4dq4a" role="2Oq$k0" />
                    <node concept="1mIQ4w" id="Pq8sy4dqxg" role="2OqNvi">
                      <node concept="chp4Y" id="Pq8sy4dqFl" role="cj9EA">
                        <ref role="cht4Q" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="wsIXK0KVr0">
    <ref role="1XX52x" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
    <node concept="3EZMnI" id="fDoU8NI" role="2wV5jI">
      <node concept="VPM3Z" id="hEU$PuE" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="PMmxH" id="hNAuy_3" role="3EZMnx">
        <ref role="PMmxG" to="tpen:hNAtxlY" resolve="_DeprecatedPart" />
      </node>
      <node concept="PMmxH" id="6aS1KHf_Efx" role="3EZMnx">
        <ref role="PMmxG" to="tpen:6aS1KHf_xVK" resolve="HasAnnotation_AnnotationComponent" />
      </node>
      <node concept="3F0ifn" id="wsIXK0KWYh" role="3EZMnx">
        <property role="3F0ifm" value="&lt;sst&gt;" />
        <ref role="1k5W1q" to="tpco:3VARyd8XcQs" resolve="Comment" />
      </node>
      <node concept="3EZMnI" id="hHIJFsl" role="3EZMnx">
        <node concept="VPM3Z" id="hHIJFsm" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="pkWqt" id="hHIJJm2" role="pqm2j">
          <node concept="3clFbS" id="hHIJJm3" role="2VODD2">
            <node concept="3clFbF" id="hHIJKhT" role="3cqZAp">
              <node concept="3fqX7Q" id="hHIJNlM" role="3clFbG">
                <node concept="2OqwBi" id="hHIJNlN" role="3fr31v">
                  <node concept="2OqwBi" id="hHIJNlO" role="2Oq$k0">
                    <node concept="pncrf" id="hHIJNlP" role="2Oq$k0" />
                    <node concept="1mfA1w" id="hHIJNlQ" role="2OqNvi" />
                  </node>
                  <node concept="1mIQ4w" id="hHIJNlR" role="2OqNvi">
                    <node concept="chp4Y" id="hHIJNlS" role="cj9EA">
                      <ref role="cht4Q" to="tpee:g7HP654" resolve="Interface" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="PMmxH" id="hHIJHyV" role="3EZMnx">
          <ref role="PMmxG" to="tpen:h9AUA0X" resolve="_Component_Visibility" />
        </node>
        <node concept="3F0ifn" id="i34__Y3" role="3EZMnx">
          <property role="3F0ifm" value="final" />
          <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
          <ref role="1ERwB7" to="tpen:78NyZDfmLGH" resolve="DeleteFinalInBaseMethod" />
          <node concept="2SqB2G" id="5kmCgHXRRIX" role="2SqHTX">
            <property role="TrG5h" value="finalModifier" />
          </node>
          <node concept="VPxyj" id="2bl07wEBuDO" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pkWqt" id="i34_E9O" role="pqm2j">
            <node concept="3clFbS" id="i34_E9P" role="2VODD2">
              <node concept="3clFbF" id="i34_FMG" role="3cqZAp">
                <node concept="2OqwBi" id="i34_G3T" role="3clFbG">
                  <node concept="pncrf" id="i34_FMH" role="2Oq$k0" />
                  <node concept="3TrcHB" id="i34_Hdl" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:hcDiZZi" resolve="isFinal" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="A1WHu" id="1wEcoXjJMIa" role="3vIgyS">
            <ref role="A1WHt" to="tpen:1wEcoXjJMD2" resolve="InstanceMethodDeclaration_ApplySideTransforms" />
          </node>
        </node>
        <node concept="3F0ifn" id="hHIJHyW" role="3EZMnx">
          <property role="3F0ifm" value="abstract" />
          <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
          <ref role="1ERwB7" to="tpen:h9EBNOl" resolve="_InstanceMethodDeclaration_RemoveAbstract" />
          <node concept="2SqB2G" id="5kmCgHXRUBB" role="2SqHTX">
            <property role="TrG5h" value="abstractModifier" />
          </node>
          <node concept="VPxyj" id="2bl07wEHQh2" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pkWqt" id="hHIJHyX" role="pqm2j">
            <node concept="3clFbS" id="hHIJHyY" role="2VODD2">
              <node concept="3cpWs6" id="hHIJHyZ" role="3cqZAp">
                <node concept="2OqwBi" id="hHIJHz0" role="3cqZAk">
                  <node concept="pncrf" id="hHIJHz1" role="2Oq$k0" />
                  <node concept="3TrcHB" id="hHIKSBf" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:h9EzhlX" resolve="isAbstract" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="A1WHu" id="1wEcoXjJMJ3" role="3vIgyS">
            <ref role="A1WHt" to="tpen:1wEcoXjJMD2" resolve="InstanceMethodDeclaration_ApplySideTransforms" />
          </node>
        </node>
        <node concept="3F0ifn" id="3HnrdCzohF1" role="3EZMnx">
          <property role="3F0ifm" value="synchronized" />
          <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
          <ref role="1ERwB7" to="tpen:3HnrdCzoiM1" resolve="DeleteSynchronizedInBaseMethod" />
          <node concept="2SqB2G" id="5kmCgHXRUBL" role="2SqHTX">
            <property role="TrG5h" value="synchronizedModifier" />
          </node>
          <node concept="VPxyj" id="2bl07wEHQnc" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pkWqt" id="3HnrdCzohF2" role="pqm2j">
            <node concept="3clFbS" id="3HnrdCzohF3" role="2VODD2">
              <node concept="3clFbF" id="3HnrdCzoiLO" role="3cqZAp">
                <node concept="2OqwBi" id="3HnrdCzoiLQ" role="3clFbG">
                  <node concept="pncrf" id="3HnrdCzoiLP" role="2Oq$k0" />
                  <node concept="3TrcHB" id="3HnrdCzoiLZ" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:3HnrdCzoiLU" resolve="isSynchronized" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="A1WHu" id="1wEcoXjJMFS" role="3vIgyS">
            <ref role="A1WHt" to="tpen:1wEcoXjJMD2" resolve="InstanceMethodDeclaration_ApplySideTransforms" />
          </node>
        </node>
        <node concept="3F0ifn" id="7fN3zRTn5dh" role="3EZMnx">
          <property role="3F0ifm" value="native" />
          <ref role="1k5W1q" to="tpen:hgVS8CF" resolve="KeyWord" />
          <ref role="1ERwB7" to="tpen:7fN3zRTnk76" resolve="DeleteNativeInMethod" />
          <node concept="2SqB2G" id="5kmCgHXRUX8" role="2SqHTX">
            <property role="TrG5h" value="nativeModifier" />
          </node>
          <node concept="VPxyj" id="7fN3zRTnhvH" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="pkWqt" id="7fN3zRTnhHs" role="pqm2j">
            <node concept="3clFbS" id="7fN3zRTnhHt" role="2VODD2">
              <node concept="3clFbF" id="7fN3zRTnhSb" role="3cqZAp">
                <node concept="2OqwBi" id="7fN3zRTni6m" role="3clFbG">
                  <node concept="pncrf" id="7fN3zRTnhSa" role="2Oq$k0" />
                  <node concept="3TrcHB" id="7fN3zRTnjwj" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:7fN3zRTn0HN" resolve="isNative" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="A1WHu" id="1wEcoXjJMDe" role="3vIgyS">
            <ref role="A1WHt" to="tpen:1wEcoXjJMD2" resolve="InstanceMethodDeclaration_ApplySideTransforms" />
          </node>
        </node>
        <node concept="l2Vlx" id="i0HIZvn" role="2iSdaV" />
      </node>
      <node concept="PMmxH" id="3$ZGCDjhC3L" role="3EZMnx">
        <ref role="PMmxG" to="tpen:3$ZGCDjhC3z" resolve="ModifiersComponent" />
        <node concept="pkWqt" id="6Cx7QMisSAd" role="pqm2j">
          <node concept="3clFbS" id="6Cx7QMisSAe" role="2VODD2">
            <node concept="3clFbF" id="6Cx7QMisSAk" role="3cqZAp">
              <node concept="2OqwBi" id="6Cx7QMisSAl" role="3clFbG">
                <node concept="2OqwBi" id="6Cx7QMisSAm" role="2Oq$k0">
                  <node concept="pncrf" id="6Cx7QMisSAn" role="2Oq$k0" />
                  <node concept="Bykcj" id="6Cx7QMisSAo" role="2OqNvi">
                    <node concept="1aIX9F" id="6Cx7QMisSAp" role="1xVPHs">
                      <node concept="26LbJo" id="6Cx7QMisSAq" role="1aIX9E">
                        <ref role="26LbJp" to="tpee:20YUQaJkyYL" resolve="modifiers" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3GX2aA" id="6Cx7QMisSAr" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="PMmxH" id="hwL1BXI" role="3EZMnx">
        <ref role="PMmxG" to="tpen:g96ft$4" resolve="_GenericDeclaration_TypeVariables_Component" />
        <node concept="pkWqt" id="hwL1BXJ" role="pqm2j">
          <node concept="3clFbS" id="hwL1BXK" role="2VODD2">
            <node concept="3cpWs6" id="hwL1BXL" role="3cqZAp">
              <node concept="2OqwBi" id="5eo3iW5feek" role="3cqZAk">
                <node concept="2OqwBi" id="hxiFsjd" role="2Oq$k0">
                  <node concept="pncrf" id="hwL1BXQ" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="hwL1BXR" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:g96eVAe" resolve="typeVariableDeclaration" />
                  </node>
                </node>
                <node concept="3GX2aA" id="5eo3iW5feel" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F1sOY" id="fDoUsrO" role="3EZMnx">
        <property role="1$x2rV" value="&lt;no return type&gt;" />
        <property role="1cu_pB" value="2" />
        <ref role="1NtTu8" to="tpee:fzclF7X" resolve="returnType" />
        <node concept="A1WHu" id="43H3v3INDz_" role="3vIgyS">
          <ref role="A1WHt" to="tpen:3$ZGCDhcCx4" resolve="ForReturnTypeOfTheMethod" />
        </node>
      </node>
      <node concept="PMmxH" id="hfRTI2S" role="3EZMnx">
        <ref role="PMmxG" to="tpen:hfRTih$" resolve="BaseMethodDeclaration_NameCellComponent" />
        <node concept="3nxI2P" id="hO0Csuf" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="fDoU8NM" role="3EZMnx">
        <property role="3F0ifm" value="(" />
        <ref role="1k5W1q" to="tpen:hY9fg1G" resolve="LeftParenAfterName" />
        <node concept="2SqB2G" id="2VygjZZ9zbe" role="2SqHTX">
          <property role="TrG5h" value="leftParen" />
        </node>
      </node>
      <node concept="3F2HdR" id="g$abzDm" role="3EZMnx">
        <property role="2czwfO" value="," />
        <ref role="1NtTu8" to="tpee:fzclF7Y" resolve="parameter" />
        <node concept="3F0ifn" id="g$abzDn" role="2czzBI">
          <node concept="VPM3Z" id="hEU$Ppc" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="VPxyj" id="hEZKQ$A" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="l2Vlx" id="i0NJYCV" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="fDoU8NP" role="3EZMnx">
        <property role="3F0ifm" value=")" />
        <ref role="1k5W1q" to="tpen:hFCSUmN" resolve="RightParen" />
        <node concept="2SqB2G" id="2VygjZZ9_bd" role="2SqHTX">
          <property role="TrG5h" value="rightParen" />
        </node>
        <node concept="VPM3Z" id="hEU$PaU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="A1WHu" id="1wEcoXjJMHs" role="3vIgyS">
          <ref role="A1WHt" to="tpen:1wEcoXjJMHl" resolve="InstanceMethodDeclaration_ApplySideTransforms_1" />
        </node>
      </node>
      <node concept="PMmxH" id="4ZFm$8SP73$" role="3EZMnx">
        <ref role="PMmxG" to="tpen:4ZFm$8SP4Ko" resolve="BaseMethodDeclaration_ThrowsCollection_Component" />
      </node>
      <node concept="3F0ifn" id="h9E_8mS" role="3EZMnx">
        <property role="3F0ifm" value=";" />
        <ref role="1k5W1q" to="tpen:hFDgi_W" resolve="Semicolon" />
        <ref role="1ERwB7" to="tpen:64WA21b_Rbu" resolve="DeleteClassifierMember" />
        <node concept="pkWqt" id="h9E_9ea" role="pqm2j">
          <node concept="3clFbS" id="h9E_9eb" role="2VODD2">
            <node concept="3cpWs6" id="h9E_9Cm" role="3cqZAp">
              <node concept="3fqX7Q" id="4SpJmwPM3Ef" role="3cqZAk">
                <node concept="2OqwBi" id="4SpJmwPM3Eh" role="3fr31v">
                  <node concept="pncrf" id="4SpJmwPM3Ei" role="2Oq$k0" />
                  <node concept="2qgKlT" id="4SpJmwPM3Ej" role="2OqNvi">
                    <ref role="37wK5l" to="tpek:10BRnhak8m8" resolve="hasBody" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="ljvvj" id="i0HIZvq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="PMmxH" id="5UYpxeVajUe" role="3EZMnx">
        <ref role="PMmxG" to="tpen:5UYpxeVafB6" resolve="BaseMethodDeclaration_BodyComponent" />
        <node concept="pkWqt" id="5UYpxeVajUh" role="pqm2j">
          <node concept="3clFbS" id="5UYpxeVajUi" role="2VODD2">
            <node concept="3cpWs6" id="5UYpxeVajUj" role="3cqZAp">
              <node concept="2OqwBi" id="7fnnP3fG0In" role="3cqZAk">
                <node concept="pncrf" id="7fnnP3fG0vY" role="2Oq$k0" />
                <node concept="2qgKlT" id="4SpJmwPM6Rt" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:10BRnhak8m8" resolve="hasBody" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="l2Vlx" id="i0HIZv_" role="2iSdaV" />
    </node>
  </node>
  <node concept="3INDKC" id="wsIXK0LH8V">
    <property role="TrG5h" value="InstanceMethod_to_StreamMethod" />
    <node concept="1Qtc8_" id="wsIXK0LH90" role="IW6Ez">
      <node concept="3cWJ9i" id="wsIXK0LH94" role="1Qtc8$">
        <node concept="CtIbL" id="wsIXK0LH96" role="CtIbM">
          <property role="CtIbK" value="1A4kJjlVmVt/LEFT" />
        </node>
      </node>
      <node concept="IWgqT" id="wsIXK0LH9a" role="1Qtc8A">
        <node concept="1hCUdq" id="wsIXK0LH9b" role="1hCUd6">
          <node concept="3clFbS" id="wsIXK0LH9c" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0LHe7" role="3cqZAp">
              <node concept="Xl_RD" id="wsIXK0LHe6" role="3clFbG">
                <property role="Xl_RC" value="&lt;sst&gt;" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="wsIXK0LH9d" role="IWgqQ">
          <node concept="3clFbS" id="wsIXK0LH9e" role="2VODD2">
            <node concept="3cpWs8" id="wsIXK0LJdZ" role="3cqZAp">
              <node concept="3cpWsn" id="wsIXK0LJe2" role="3cpWs9">
                <property role="TrG5h" value="streamMethod" />
                <node concept="3Tqbb2" id="wsIXK0LJdY" role="1tU5fm">
                  <ref role="ehGHo" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                </node>
                <node concept="2ShNRf" id="wsIXK0LJeU" role="33vP2m">
                  <node concept="3zrR0B" id="wsIXK0LJeS" role="2ShVmc">
                    <node concept="3Tqbb2" id="wsIXK0LJeT" role="3zrR0E">
                      <ref role="ehGHo" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0POec" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0Q2KC" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0Q1BQ" role="2Oq$k0">
                  <node concept="37vLTw" id="wsIXK0POea" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrEf2" id="wsIXK0Q2tU" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                  </node>
                </node>
                <node concept="zfrQC" id="wsIXK0Q374" role="2OqNvi" />
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0LJfX" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0M7fi" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0LKL6" role="2Oq$k0">
                  <node concept="2OqwBi" id="wsIXK0LJDs" role="2Oq$k0">
                    <node concept="37vLTw" id="wsIXK0LJfV" role="2Oq$k0">
                      <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                    </node>
                    <node concept="3TrEf2" id="wsIXK0LKvw" role="2OqNvi">
                      <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                    </node>
                  </node>
                  <node concept="3Tsc0h" id="wsIXK0M524" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:fzcqZ_x" resolve="statement" />
                  </node>
                </node>
                <node concept="X8dFx" id="wsIXK0M9DQ" role="2OqNvi">
                  <node concept="2OqwBi" id="wsIXK0Mfwl" role="25WWJ7">
                    <node concept="2OqwBi" id="wsIXK0Md6V" role="2Oq$k0">
                      <node concept="7Obwk" id="wsIXK0Ma4U" role="2Oq$k0" />
                      <node concept="3TrEf2" id="wsIXK0MelF" role="2OqNvi">
                        <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                      </node>
                    </node>
                    <node concept="3Tsc0h" id="wsIXK0Mggy" role="2OqNvi">
                      <ref role="3TtcxE" to="tpee:fzcqZ_x" resolve="statement" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0LMAY" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0LOVE" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0LML2" role="2Oq$k0">
                  <node concept="37vLTw" id="wsIXK0LMAW" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3Tsc0h" id="wsIXK0LMYB" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:hiAJF2X" resolve="annotation" />
                  </node>
                </node>
                <node concept="X8dFx" id="wsIXK0LRNU" role="2OqNvi">
                  <node concept="2OqwBi" id="wsIXK0LYrC" role="25WWJ7">
                    <node concept="7Obwk" id="wsIXK0LV0c" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="wsIXK0M15Z" role="2OqNvi">
                      <ref role="3TtcxE" to="tpee:hiAJF2X" resolve="annotation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0MssL" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0Mxa4" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0Mxza" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0Mxat" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0Myna" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0MvFM" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0MssJ" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrcHB" id="wsIXK0MwRR" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0MyWe" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0MDEc" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0ME8f" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0MDE_" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0MFyW" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:h9EzhlX" resolve="isAbstract" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0MC6W" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0MyWc" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrcHB" id="wsIXK0MDgB" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:h9EzhlX" resolve="isAbstract" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0MLmK" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0MP27" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0MPwa" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0MP2w" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0MQsX" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:hcDiZZi" resolve="isFinal" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0MOk5" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0MLmI" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrcHB" id="wsIXK0MOC$" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:hcDiZZi" resolve="isFinal" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0MT8R" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0MXvw" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0MXXz" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0MXvT" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0MYPr" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:3HnrdCzoiLU" resolve="isSynchronized" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0MW3z" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0MT8P" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrcHB" id="wsIXK0MXd5" role="2OqNvi">
                    <ref role="3TsBF5" to="tpee:3HnrdCzoiLU" resolve="isSynchronized" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0N2a7" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0NaVF" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0N6ei" role="2Oq$k0">
                  <node concept="37vLTw" id="wsIXK0N2a5" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3Tsc0h" id="wsIXK0N74m" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:fzclF7Y" resolve="parameter" />
                  </node>
                </node>
                <node concept="X8dFx" id="wsIXK0NiUk" role="2OqNvi">
                  <node concept="2OqwBi" id="wsIXK0Nq5I" role="25WWJ7">
                    <node concept="7Obwk" id="wsIXK0Nmo9" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="wsIXK0Nyxv" role="2OqNvi">
                      <ref role="3TtcxE" to="tpee:fzclF7Y" resolve="parameter" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0NAiJ" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0NERR" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0NFlW" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0NESi" role="2Oq$k0" />
                  <node concept="3TrEf2" id="wsIXK0NGhp" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzclF7X" resolve="returnType" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0NEjT" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0NAiH" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrEf2" id="wsIXK0NEF0" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzclF7X" resolve="returnType" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0NPqD" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0OcmX" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0NWwG" role="2Oq$k0">
                  <node concept="37vLTw" id="wsIXK0NPqB" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3Tsc0h" id="wsIXK0NXNB" role="2OqNvi">
                    <ref role="3TtcxE" to="tpee:gWSfm_9" resolve="throwsItem" />
                  </node>
                </node>
                <node concept="X8dFx" id="wsIXK0OfMp" role="2OqNvi">
                  <node concept="2OqwBi" id="wsIXK0Ol4P" role="25WWJ7">
                    <node concept="7Obwk" id="wsIXK0Oilc" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="wsIXK0Oopb" role="2OqNvi">
                      <ref role="3TtcxE" to="tpee:gWSfm_9" resolve="throwsItem" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="wsIXK0O0Mj" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0O6N_" role="3clFbG">
                <node concept="2OqwBi" id="wsIXK0O7hE" role="37vLTx">
                  <node concept="7Obwk" id="wsIXK0O6O0" role="2Oq$k0" />
                  <node concept="3TrEf2" id="wsIXK0O87D" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:h9B3oxE" resolve="visibility" />
                  </node>
                </node>
                <node concept="2OqwBi" id="wsIXK0O5sU" role="37vLTJ">
                  <node concept="37vLTw" id="wsIXK0O0Mh" role="2Oq$k0">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                  <node concept="3TrEf2" id="wsIXK0O6FV" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:h9B3oxE" resolve="visibility" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="wsIXK0OrtM" role="3cqZAp" />
            <node concept="3clFbF" id="wsIXK0OGRF" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0ONfM" role="3clFbG">
                <node concept="7Obwk" id="wsIXK0OGRD" role="2Oq$k0" />
                <node concept="1P9Npp" id="wsIXK0OOz7" role="2OqNvi">
                  <node concept="37vLTw" id="wsIXK0OO_7" role="1P9ThW">
                    <ref role="3cqZAo" node="wsIXK0LJe2" resolve="streamMethod" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="27VH4U" id="wsIXK0LHeH" role="2jiSrf">
          <node concept="3clFbS" id="wsIXK0LHeI" role="2VODD2">
            <node concept="3cpWs6" id="wsIXK0LHiN" role="3cqZAp">
              <node concept="3fqX7Q" id="wsIXK0LISN" role="3cqZAk">
                <node concept="1eOMI4" id="wsIXK0LJcI" role="3fr31v">
                  <node concept="2OqwBi" id="wsIXK0LISP" role="1eOMHV">
                    <node concept="7Obwk" id="wsIXK0LISQ" role="2Oq$k0" />
                    <node concept="1mIQ4w" id="wsIXK0LISR" role="2OqNvi">
                      <node concept="chp4Y" id="wsIXK0LISS" role="cj9EA">
                        <ref role="cht4Q" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="A1WHr" id="wsIXK0LH8X" role="AmTjC">
      <ref role="2ZyFGn" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
    </node>
  </node>
  <node concept="IW6AY" id="wsIXK0RmJx">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="aqKnT" to="qyjc:Jw6Yu7QSWI" resolve="ICanWait" />
    <node concept="1Qtc8_" id="wsIXK0UvTX" role="IW6Ez">
      <node concept="3cWJ9i" id="wsIXK0UvTY" role="1Qtc8$">
        <node concept="CtIbL" id="wsIXK0UvTZ" role="CtIbM">
          <property role="CtIbK" value="1A4kJjlVmVt/LEFT" />
        </node>
      </node>
      <node concept="IWgqT" id="wsIXK0UvU0" role="1Qtc8A">
        <node concept="1hCUdq" id="wsIXK0UvU1" role="1hCUd6">
          <node concept="3clFbS" id="wsIXK0UvU2" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0UvU3" role="3cqZAp">
              <node concept="Xl_RD" id="wsIXK0UvU4" role="3clFbG">
                <property role="Xl_RC" value="wait" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="wsIXK0UvU5" role="IWgqQ">
          <node concept="3clFbS" id="wsIXK0UvU6" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0UvU7" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0UvU8" role="3clFbG">
                <node concept="3clFbT" id="wsIXK0UvU9" role="37vLTx">
                  <property role="3clFbU" value="true" />
                </node>
                <node concept="2OqwBi" id="wsIXK0UvUa" role="37vLTJ">
                  <node concept="7Obwk" id="wsIXK0UvUb" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0UvUc" role="2OqNvi">
                    <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="27VH4U" id="wsIXK0UvUd" role="2jiSrf">
          <node concept="3clFbS" id="wsIXK0UvUe" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0UvUf" role="3cqZAp">
              <node concept="3fqX7Q" id="wsIXK0UvUg" role="3clFbG">
                <node concept="1eOMI4" id="wsIXK0UvUh" role="3fr31v">
                  <node concept="2OqwBi" id="wsIXK0UvUi" role="1eOMHV">
                    <node concept="7Obwk" id="wsIXK0UvUj" role="2Oq$k0" />
                    <node concept="3TrcHB" id="wsIXK0UvUk" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1Qtc8_" id="wsIXK0QIw$" role="IW6Ez">
      <node concept="3cWJ9i" id="wsIXK0QIwC" role="1Qtc8$">
        <node concept="CtIbL" id="wsIXK0QIwE" role="CtIbM">
          <property role="CtIbK" value="30NnNOohrQL/RIGHT" />
        </node>
      </node>
      <node concept="IWgqT" id="wsIXK0QIwI" role="1Qtc8A">
        <node concept="1hCUdq" id="wsIXK0QIwJ" role="1hCUd6">
          <node concept="3clFbS" id="wsIXK0QIwK" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0QI_B" role="3cqZAp">
              <node concept="Xl_RD" id="wsIXK0QI_A" role="3clFbG">
                <property role="Xl_RC" value="wait" />
              </node>
            </node>
          </node>
        </node>
        <node concept="IWg2L" id="wsIXK0QIwL" role="IWgqQ">
          <node concept="3clFbS" id="wsIXK0QIwM" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0QIBk" role="3cqZAp">
              <node concept="37vLTI" id="wsIXK0QJb4" role="3clFbG">
                <node concept="3clFbT" id="wsIXK0QJbu" role="37vLTx">
                  <property role="3clFbU" value="true" />
                </node>
                <node concept="2OqwBi" id="wsIXK0QIIq" role="37vLTJ">
                  <node concept="7Obwk" id="wsIXK0QIBj" role="2Oq$k0" />
                  <node concept="3TrcHB" id="wsIXK0QIQT" role="2OqNvi">
                    <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="27VH4U" id="wsIXK0RFw4" role="2jiSrf">
          <node concept="3clFbS" id="wsIXK0RFw5" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0RFAg" role="3cqZAp">
              <node concept="3fqX7Q" id="wsIXK0RGJc" role="3clFbG">
                <node concept="1eOMI4" id="wsIXK0RGSD" role="3fr31v">
                  <node concept="2OqwBi" id="wsIXK0RGJe" role="1eOMHV">
                    <node concept="7Obwk" id="wsIXK0RGJf" role="2Oq$k0" />
                    <node concept="3TrcHB" id="wsIXK0RGJg" role="2OqNvi">
                      <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="PKFIW" id="wsIXK0REP8">
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="ICanWait_Component" />
    <ref role="1XX52x" to="qyjc:Jw6Yu7QSWI" resolve="ICanWait" />
    <node concept="3EZMnI" id="wsIXK0REPa" role="2wV5jI">
      <node concept="l2Vlx" id="wsIXK0REPd" role="2iSdaV" />
      <node concept="3F0ifn" id="wsIXK0REPk" role="3EZMnx">
        <property role="3F0ifm" value="and wait" />
        <node concept="pkWqt" id="wsIXK0REPm" role="pqm2j">
          <node concept="3clFbS" id="wsIXK0REPn" role="2VODD2">
            <node concept="3clFbF" id="wsIXK0RETn" role="3cqZAp">
              <node concept="2OqwBi" id="wsIXK0RF5W" role="3clFbG">
                <node concept="pncrf" id="wsIXK0RETm" role="2Oq$k0" />
                <node concept="3TrcHB" id="wsIXK0RFkW" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="1yapuqB6Rik">
    <property role="3GE5qa" value="Window" />
    <ref role="1XX52x" to="qyjc:4vp2seWfjgJ" resolve="WindowRef" />
    <node concept="1iCGBv" id="1yapuqB6Rim" role="2wV5jI">
      <ref role="1NtTu8" to="qyjc:4vp2seWfjgK" resolve="ref" />
      <node concept="1sVBvm" id="1yapuqB6Rio" role="1sWHZn">
        <node concept="3F0A7n" id="1yapuqB6Riv" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="67vTL6Xtzan">
    <property role="3GE5qa" value="EngineConfig" />
    <ref role="1XX52x" to="qyjc:2XsxYmZ1fp5" resolve="EngineConfigRef" />
    <node concept="1iCGBv" id="67vTL6Xtzap" role="2wV5jI">
      <ref role="1NtTu8" to="qyjc:2XsxYmZ1hR3" resolve="ref" />
      <node concept="1sVBvm" id="67vTL6Xtzar" role="1sWHZn">
        <node concept="3F0A7n" id="67vTL6Xtzay" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="67vTL6Xyhcu">
    <property role="3GE5qa" value="EngineConfig" />
    <ref role="1XX52x" to="qyjc:67vTL6Xyhcc" resolve="SparkSessionRef" />
    <node concept="3F0ifn" id="67vTL6Xyhcw" role="2wV5jI">
      <property role="3F0ifm" value="SparkSession" />
    </node>
  </node>
</model>

