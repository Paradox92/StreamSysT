<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:eba25f23-9b24-447c-bcf3-e769edd2f2ab(hfu.mps.StreamSysT.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="qyjc" ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1140725362528" name="jetbrains.mps.lang.smodel.structure.Link_SetTargetOperation" flags="nn" index="2oxUTD">
        <child id="1140725362529" name="linkTarget" index="2oxUTC" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1160666733551" name="jetbrains.mps.baseLanguage.collections.structure.AddAllElementsOperation" flags="nn" index="X8dFx" />
    </language>
  </registry>
  <node concept="2S6QgY" id="5fBz7OV5DUe">
    <property role="TrG5h" value="ConvertToStreamTest" />
    <ref role="2ZfgGC" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
    <node concept="2S6ZIM" id="5fBz7OV5DUf" role="2ZfVej">
      <node concept="3clFbS" id="5fBz7OV5DUg" role="2VODD2">
        <node concept="3cpWs6" id="5fBz7OV5DZz" role="3cqZAp">
          <node concept="Xl_RD" id="5fBz7OV5E4$" role="3cqZAk">
            <property role="Xl_RC" value="Convert to Stream Test Method" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="5fBz7OV5DUh" role="2ZfgGD">
      <node concept="3clFbS" id="5fBz7OV5DUi" role="2VODD2">
        <node concept="3cpWs8" id="5fBz7OV5EiQ" role="3cqZAp">
          <node concept="3cpWsn" id="5fBz7OV5EiT" role="3cpWs9">
            <property role="TrG5h" value="streamTest" />
            <node concept="3Tqbb2" id="5fBz7OV5EiP" role="1tU5fm">
              <ref role="ehGHo" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
            </node>
            <node concept="2ShNRf" id="5fBz7OV5EkN" role="33vP2m">
              <node concept="3zrR0B" id="5fBz7OV5E$$" role="2ShVmc">
                <node concept="3Tqbb2" id="5fBz7OV5E$A" role="3zrR0E">
                  <ref role="ehGHo" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV5E_J" role="3cqZAp">
          <node concept="2OqwBi" id="5fBz7OV5Gi6" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV5F73" role="2Oq$k0">
              <node concept="37vLTw" id="5fBz7OV5E_H" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrEf2" id="5fBz7OV5FWE" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
              </node>
            </node>
            <node concept="2oxUTD" id="5fBz7OV5H7C" role="2OqNvi">
              <node concept="2OqwBi" id="5fBz7OV5HGq" role="2oxUTC">
                <node concept="2Sf5sV" id="5fBz7OV9xdI" role="2Oq$k0" />
                <node concept="3TrEf2" id="5fBz7OV5IyX" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzclF7Z" resolve="body" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV65gv" role="3cqZAp">
          <node concept="2OqwBi" id="5fBz7OV69v3" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV65NQ" role="2Oq$k0">
              <node concept="37vLTw" id="5fBz7OV65gt" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3Tsc0h" id="5fBz7OV66Dt" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:hiAJF2X" resolve="annotation" />
              </node>
            </node>
            <node concept="X8dFx" id="5fBz7OV6b9j" role="2OqNvi">
              <node concept="2OqwBi" id="5fBz7OV6f0G" role="25WWJ7">
                <node concept="2Sf5sV" id="5fBz7OV9xnM" role="2Oq$k0" />
                <node concept="3Tsc0h" id="5fBz7OV6iz2" role="2OqNvi">
                  <ref role="3TtcxE" to="tpee:hiAJF2X" resolve="annotation" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6lJt" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6ohq" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6oPP" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zLS" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV6pMW" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:h9EzhlX" resolve="isAbstract" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6n3n" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6lJr" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV6nSY" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:h9EzhlX" resolve="isAbstract" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6sWv" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6wZc" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6xz$" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zMw" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV6yqQ" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:hOIEky$" resolve="isDeprecated" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6vuw" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6sWt" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV6vEd" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:hOIEky$" resolve="isDeprecated" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6yLM" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6A93" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6AD_" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zN8" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV6BAG" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:hcDiZZi" resolve="isFinal" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6_0P" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6yLK" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV6_Qs" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:hcDiZZi" resolve="isFinal" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6CII" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6FoN" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6FT_" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zNK" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV6GKR" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:7fN3zRTn0HN" resolve="isNative" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6DS7" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6CIG" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV6E3O" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:7fN3zRTn0HN" resolve="isNative" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6HaW" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6Ljy" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6LTD" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zW9" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV6MR3" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:3HnrdCzoiLU" resolve="isSynchronized" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6Kbk" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6HaU" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV6L0V" role="2OqNvi">
                <ref role="3TsBF5" to="tpee:3HnrdCzoiLU" resolve="isSynchronized" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV6NXX" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV6PQM" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6Qmv" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9zWL" role="2Oq$k0" />
              <node concept="3TrcHB" id="5p3IOnLqfZh" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV6Omn" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV6NXV" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5p3IOnLoaei" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV7wzv" role="3cqZAp">
          <node concept="2OqwBi" id="5fBz7OV76Ra" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV6Vwn" role="2Oq$k0">
              <node concept="37vLTw" id="5fBz7OV6SpF" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3Tsc0h" id="5fBz7OV71OX" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:fzclF7Y" resolve="parameter" />
              </node>
            </node>
            <node concept="X8dFx" id="5fBz7OV7agO" role="2OqNvi">
              <node concept="2OqwBi" id="5fBz7OV7jtW" role="25WWJ7">
                <node concept="2Sf5sV" id="5fBz7OV9zXp" role="2Oq$k0" />
                <node concept="3Tsc0h" id="5fBz7OV7obQ" role="2OqNvi">
                  <ref role="3TtcxE" to="tpee:fzclF7Y" resolve="parameter" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV7OEb" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OV7XPL" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OV7Yrj" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OV9_PA" role="2Oq$k0" />
              <node concept="3TrcHB" id="5fBz7OV7Zjf" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OV7VCL" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV7OE9" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrcHB" id="5fBz7OV7Wuo" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OV9Yqf" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OVa50z" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OVa5xl" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OVa51f" role="2Oq$k0" />
              <node concept="3TrEf2" id="5fBz7OVa6t6" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7X" resolve="returnType" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OVa41M" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OV9Yqd" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrEf2" id="5fBz7OVa4Rp" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7X" resolve="returnType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5fBz7OVagEj" role="3cqZAp">
          <node concept="37vLTI" id="5fBz7OVaiVp" role="3clFbG">
            <node concept="2OqwBi" id="5fBz7OVajzf" role="37vLTx">
              <node concept="2Sf5sV" id="5fBz7OVaiXO" role="2Oq$k0" />
              <node concept="3TrEf2" id="5fBz7OVakuK" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:h9B3oxE" resolve="visibility" />
              </node>
            </node>
            <node concept="2OqwBi" id="5fBz7OVahXW" role="37vLTJ">
              <node concept="37vLTw" id="5fBz7OVahyh" role="2Oq$k0">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
              <node concept="3TrEf2" id="5fBz7OVaiNz" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:h9B3oxE" resolve="visibility" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5fBz7OV8glf" role="3cqZAp" />
        <node concept="3clFbF" id="5fBz7OV5ILc" role="3cqZAp">
          <node concept="2OqwBi" id="5fBz7OV5JcO" role="3clFbG">
            <node concept="2Sf5sV" id="5fBz7OV5ILa" role="2Oq$k0" />
            <node concept="1P9Npp" id="5fBz7OV5K0o" role="2OqNvi">
              <node concept="37vLTw" id="5fBz7OV5K2y" role="1P9ThW">
                <ref role="3cqZAo" node="5fBz7OV5EiT" resolve="streamTest" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="5fBz7OVegTX" role="2ZfVeh">
      <node concept="3clFbS" id="5fBz7OVegTY" role="2VODD2">
        <node concept="3clFbF" id="5fBz7OVepm6" role="3cqZAp">
          <node concept="3fqX7Q" id="5fBz7OVeprl" role="3clFbG">
            <node concept="1eOMI4" id="5fBz7OVepza" role="3fr31v">
              <node concept="2OqwBi" id="5fBz7OVeq7Y" role="1eOMHV">
                <node concept="2Sf5sV" id="5fBz7OVepBu" role="2Oq$k0" />
                <node concept="1mIQ4w" id="5fBz7OVeqZs" role="2OqNvi">
                  <node concept="chp4Y" id="5p3IOnLo9I3" role="cj9EA">
                    <ref role="cht4Q" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="Jw6Yu7QQsJ">
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="RunAll_WaitFor" />
    <ref role="2ZfgGC" to="qyjc:Jw6Yu7QSWI" resolve="ICanWait" />
    <node concept="2S6ZIM" id="Jw6Yu7QQsK" role="2ZfVej">
      <node concept="3clFbS" id="Jw6Yu7QQsL" role="2VODD2">
        <node concept="3cpWs6" id="Jw6Yu7QQxK" role="3cqZAp">
          <node concept="Xl_RD" id="Jw6Yu7QQyd" role="3cqZAk">
            <property role="Xl_RC" value="Wait for Execution Finished" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="Jw6Yu7QQsM" role="2ZfgGD">
      <node concept="3clFbS" id="Jw6Yu7QQsN" role="2VODD2">
        <node concept="3clFbF" id="Jw6Yu7QRPl" role="3cqZAp">
          <node concept="37vLTI" id="Jw6Yu7QSCa" role="3clFbG">
            <node concept="3clFbT" id="Jw6Yu7QSCB" role="37vLTx">
              <property role="3clFbU" value="true" />
            </node>
            <node concept="2OqwBi" id="Jw6Yu7QRZT" role="37vLTJ">
              <node concept="2Sf5sV" id="Jw6Yu7QRPk" role="2Oq$k0" />
              <node concept="3TrcHB" id="Jw6Yu7QVTd" role="2OqNvi">
                <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="Jw6Yu7QQFb" role="2ZfVeh">
      <node concept="3clFbS" id="Jw6Yu7QQFc" role="2VODD2">
        <node concept="3clFbF" id="Jw6Yu7QQJg" role="3cqZAp">
          <node concept="3fqX7Q" id="Jw6Yu7QRsV" role="3clFbG">
            <node concept="1eOMI4" id="Jw6Yu7QROq" role="3fr31v">
              <node concept="2OqwBi" id="Jw6Yu7QRsX" role="1eOMHV">
                <node concept="2Sf5sV" id="Jw6Yu7QRsY" role="2Oq$k0" />
                <node concept="3TrcHB" id="Jw6Yu7QWdU" role="2OqNvi">
                  <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="Jw6Yu7QSR_">
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="RunAll_DontWait" />
    <ref role="2ZfgGC" to="qyjc:Jw6Yu7QSWI" resolve="ICanWait" />
    <node concept="2S6ZIM" id="Jw6Yu7QSRA" role="2ZfVej">
      <node concept="3clFbS" id="Jw6Yu7QSRB" role="2VODD2">
        <node concept="3cpWs6" id="Jw6Yu7QTWw" role="3cqZAp">
          <node concept="Xl_RD" id="Jw6Yu7QU1l" role="3cqZAk">
            <property role="Xl_RC" value="Don't Wait for Execution Finish" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="Jw6Yu7QSRC" role="2ZfgGD">
      <node concept="3clFbS" id="Jw6Yu7QSRD" role="2VODD2">
        <node concept="3clFbF" id="Jw6Yu7QV2m" role="3cqZAp">
          <node concept="37vLTI" id="Jw6Yu7QVmW" role="3clFbG">
            <node concept="3clFbT" id="Jw6Yu7QVnG" role="37vLTx" />
            <node concept="2OqwBi" id="Jw6Yu7QV9B" role="37vLTJ">
              <node concept="2Sf5sV" id="Jw6Yu7QV2l" role="2Oq$k0" />
              <node concept="3TrcHB" id="Jw6Yu7QVgA" role="2OqNvi">
                <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="Jw6Yu7QU6B" role="2ZfVeh">
      <node concept="3clFbS" id="Jw6Yu7QU6C" role="2VODD2">
        <node concept="3clFbF" id="Jw6Yu7QUaP" role="3cqZAp">
          <node concept="2OqwBi" id="Jw6Yu7QUkb" role="3clFbG">
            <node concept="2Sf5sV" id="Jw6Yu7QUaO" role="2Oq$k0" />
            <node concept="3TrcHB" id="Jw6Yu7QUv6" role="2OqNvi">
              <ref role="3TsBF5" to="qyjc:Jw6Yu7QSWJ" resolve="wait" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

