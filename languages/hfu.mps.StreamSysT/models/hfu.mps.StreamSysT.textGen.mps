<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4987659a-72e8-4cfc-b42e-e7dd27312649(hfu.mps.StreamSysT.textGen)">
  <persistence version="9" />
  <languages>
    <use id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen" version="1" />
    <devkit ref="fa73d85a-ac7f-447b-846c-fcdc41caa600(jetbrains.mps.devkit.aspect.textgen)" />
  </languages>
  <imports>
    <import index="dmyu" ref="r:c6eeedda-084d-4659-9c4d-80b7768f2bb2(jetbrains.mps.baseLanguage.textGen)" />
    <import index="qyjc" ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
    </language>
    <language id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen">
      <concept id="8931911391946696733" name="jetbrains.mps.lang.textGen.structure.ExtensionDeclaration" flags="in" index="9MYSb" />
      <concept id="1233670071145" name="jetbrains.mps.lang.textGen.structure.ConceptTextGenDeclaration" flags="ig" index="WtQ9Q">
        <reference id="1233670257997" name="conceptDeclaration" index="WuzLi" />
        <child id="7991274449437422201" name="extension" index="33IsuW" />
        <child id="3147320813467893228" name="layout" index="3znZDQ" />
        <child id="2160817178329904672" name="contextObjects" index="1J5FnA" />
      </concept>
      <concept id="1233921373471" name="jetbrains.mps.lang.textGen.structure.LanguageTextGenDeclaration" flags="ig" index="1bsvg0">
        <child id="3996543181682114090" name="contextObjects" index="1mRmNN" />
      </concept>
      <concept id="3996543181682151539" name="jetbrains.mps.lang.textGen.structure.ClassConceptUnitContext" flags="ng" index="1mQwaE">
        <reference id="3996543181682151542" name="classifier" index="1mQwaJ" />
      </concept>
      <concept id="3996543181682044537" name="jetbrains.mps.lang.textGen.structure.UnitContextDeclaration" flags="ng" index="1mR7Mw">
        <child id="3996543181682151544" name="instance" index="1mQwax" />
        <child id="3996543181682044542" name="type" index="1mR7MB" />
      </concept>
      <concept id="3147320813467893194" name="jetbrains.mps.lang.textGen.structure.LayoutPart" flags="ng" index="3znZDg" />
      <concept id="3147320813467893193" name="jetbrains.mps.lang.textGen.structure.TextUnitLayout" flags="ng" index="3znZDj">
        <reference id="3147320813467893197" name="active" index="3znZDn" />
        <child id="3147320813467893195" name="parts" index="3znZDh" />
      </concept>
      <concept id="2160817178329904684" name="jetbrains.mps.lang.textGen.structure.UnitContextObject" flags="ng" index="1J5FnE">
        <reference id="2160817178329904685" name="context" index="1J5FnF" />
      </concept>
      <concept id="2160817178329904683" name="jetbrains.mps.lang.textGen.structure.InstancePerUnitContextObject" flags="ng" index="1J5FnH" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="WtQ9Q" id="575gSEdiwil">
    <ref role="WuzLi" to="qyjc:6M3wHT4XYt" resolve="StreamTestCase" />
    <node concept="3znZDj" id="575gSEdiwxo" role="3znZDQ">
      <ref role="3znZDn" node="575gSEdiwxB" resolve="BODY" />
      <node concept="3znZDg" id="575gSEdiwxu" role="3znZDh">
        <property role="TrG5h" value="HEADER" />
      </node>
      <node concept="3znZDg" id="575gSEdiwxw" role="3znZDh">
        <property role="TrG5h" value="IMPORTS" />
      </node>
      <node concept="3znZDg" id="575gSEdiwxz" role="3znZDh">
        <property role="TrG5h" value="SEPARATOR" />
      </node>
      <node concept="3znZDg" id="575gSEdiwxB" role="3znZDh">
        <property role="TrG5h" value="BODY" />
      </node>
    </node>
    <node concept="9MYSb" id="575gSEdiM1e" role="33IsuW">
      <node concept="3clFbS" id="575gSEdiM1f" role="2VODD2">
        <node concept="3clFbF" id="575gSEdiM5V" role="3cqZAp">
          <node concept="Xl_RD" id="575gSEdiM5U" role="3clFbG">
            <property role="Xl_RC" value="java" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1J5FnH" id="575gSEdkncF" role="1J5FnA">
      <ref role="1J5FnF" node="575gSEdiwiy" resolve="ctx" />
    </node>
  </node>
  <node concept="1bsvg0" id="575gSEdiwix">
    <property role="TrG5h" value="TGComponent" />
    <node concept="1mR7Mw" id="575gSEdiwiy" role="1mRmNN">
      <property role="TrG5h" value="ctx" />
      <node concept="3uibUv" id="575gSEdiwxi" role="1mR7MB">
        <ref role="3uigEE" to="dmyu:23Rh9MB7812" resolve="ClassifierUnitContext" />
      </node>
      <node concept="1mQwaE" id="575gSEdiwxf" role="1mQwax">
        <ref role="1mQwaJ" to="dmyu:23Rh9MB7812" resolve="ClassifierUnitContext" />
      </node>
    </node>
  </node>
</model>

