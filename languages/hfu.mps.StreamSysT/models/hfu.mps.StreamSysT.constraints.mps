<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:a3e3104a-b422-4744-9f98-a5ecd1c176fd(hfu.mps.StreamSysT.constraints)">
  <persistence version="9" />
  <languages>
    <use id="5dae8159-ab99-46bb-a40d-0cee30ee7018" name="jetbrains.mps.lang.constraints.rules.kinds" version="0" />
    <use id="ea3159bf-f48e-4720-bde2-86dba75f0d34" name="jetbrains.mps.lang.context.defs" version="0" />
    <use id="e51810c5-7308-4642-bcb6-469e61b5dd18" name="jetbrains.mps.lang.constraints.msg.specification" version="0" />
    <use id="134c38d4-e3af-4d9e-b069-1c7df0a4005d" name="jetbrains.mps.lang.constraints.rules.skeleton" version="0" />
    <use id="b3551702-269c-4f05-ba61-58060cef4292" name="jetbrains.mps.lang.rulesAndMessages" version="0" />
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="6" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <use id="3ad5badc-1d9c-461c-b7b1-fa2fcd0a0ae7" name="jetbrains.mps.lang.context" version="0" />
    <use id="ad93155d-79b2-4759-b10c-55123e763903" name="jetbrains.mps.lang.messages" version="0" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="qyjc" ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="6702802731807351367" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_CanBeAChild" flags="in" index="9S07l" />
      <concept id="1202989658459" name="jetbrains.mps.lang.constraints.structure.ConstraintFunctionParameter_parentNode" flags="nn" index="nLn13" />
      <concept id="1147468365020" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_node" flags="nn" index="EsrRn" />
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="6702802731807737306" name="canBeChild" index="9Vyp8" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="4vp2seWezaq">
    <property role="3GE5qa" value="Pipeline" />
    <ref role="1M2myG" to="qyjc:7MdWNyBagav" resolve="PipelineRef" />
    <node concept="1N5Pfh" id="4vp2seWezar" role="1Mr941">
      <ref role="1N5Vy1" to="qyjc:7MdWNyBagaw" resolve="ref" />
      <node concept="1dDu$B" id="4vp2seWezbi" role="1N6uqs">
        <ref role="1dDu$A" to="qyjc:5T7UbTDhmXB" resolve="Pipeline" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="4vp2seWf7Dt">
    <property role="3GE5qa" value="EngineConfig" />
    <ref role="1M2myG" to="qyjc:2XsxYmZ1fp5" resolve="EngineConfigRef" />
    <node concept="1N5Pfh" id="4vp2seWf7Du" role="1Mr941">
      <ref role="1N5Vy1" to="qyjc:2XsxYmZ1hR3" resolve="ref" />
      <node concept="1dDu$B" id="4vp2seWf7Dw" role="1N6uqs">
        <ref role="1dDu$A" to="qyjc:5$bixu1eN8W" resolve="EngineConfig" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="4vp2seWfjgO">
    <property role="3GE5qa" value="Window" />
    <ref role="1M2myG" to="qyjc:4vp2seWfjgJ" resolve="WindowRef" />
    <node concept="1N5Pfh" id="4vp2seWfjgP" role="1Mr941">
      <ref role="1N5Vy1" to="qyjc:4vp2seWfjgK" resolve="ref" />
      <node concept="1dDu$B" id="4vp2seWfjhG" role="1N6uqs">
        <ref role="1dDu$A" to="qyjc:4o2stJ22OSe" resolve="Window" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="4vp2seWfjjq">
    <property role="3GE5qa" value="Source" />
    <ref role="1M2myG" to="qyjc:1CpVIWKRvId" resolve="EventSourceRef" />
    <node concept="1N5Pfh" id="4vp2seWfjjr" role="1Mr941">
      <ref role="1N5Vy1" to="qyjc:RoIJycd2v7" resolve="ref" />
      <node concept="1dDu$B" id="4vp2seWfjki" role="1N6uqs">
        <ref role="1dDu$A" to="qyjc:1CpVIWKKpfT" resolve="EventSource" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="4vp2seWfjm0">
    <property role="3GE5qa" value="Sink" />
    <ref role="1M2myG" to="qyjc:1CpVIWKRvIg" resolve="EventSinkRef" />
    <node concept="1N5Pfh" id="4vp2seWfjm1" role="1Mr941">
      <ref role="1N5Vy1" to="qyjc:RoIJycd2vh" resolve="ref" />
      <node concept="1dDu$B" id="4vp2seWfjm3" role="1N6uqs">
        <ref role="1dDu$A" to="qyjc:1CpVIWKKpwu" resolve="EventSink" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="Pq8sy4ipPD">
    <property role="3GE5qa" value="Sink" />
    <ref role="1M2myG" to="qyjc:Pq8sy4bYU8" resolve="PartialSink" />
    <node concept="9S07l" id="Pq8sy4ipPE" role="9Vyp8">
      <node concept="3clFbS" id="Pq8sy4ipPF" role="2VODD2">
        <node concept="3cpWs6" id="Pq8sy4ipUs" role="3cqZAp">
          <node concept="2OqwBi" id="Pq8sy4iqyZ" role="3cqZAk">
            <node concept="2OqwBi" id="Pq8sy4iq9g" role="2Oq$k0">
              <node concept="EsrRn" id="Pq8sy4ipUY" role="2Oq$k0" />
              <node concept="3TrEf2" id="Pq8sy4iqjb" role="2OqNvi">
                <ref role="3Tt5mk" to="qyjc:RoIJycd2vh" resolve="ref" />
              </node>
            </node>
            <node concept="3TrcHB" id="Pq8sy4iqTF" role="2OqNvi">
              <ref role="3TsBF5" to="qyjc:Pq8sy4ipnW" resolve="multipleOutputs" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="wsIXK0Lz6M">
    <ref role="1M2myG" to="qyjc:575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="9S07l" id="wsIXK0Lz6N" role="9Vyp8">
      <node concept="3clFbS" id="wsIXK0Lz6O" role="2VODD2">
        <node concept="3clFbF" id="wsIXK0LzaK" role="3cqZAp">
          <node concept="22lmx$" id="wsIXK0LBwi" role="3clFbG">
            <node concept="2OqwBi" id="wsIXK0L$s$" role="3uHU7B">
              <node concept="2OqwBi" id="wsIXK0LzuH" role="2Oq$k0">
                <node concept="nLn13" id="wsIXK0LEYV" role="2Oq$k0" />
                <node concept="2Xjw5R" id="wsIXK0LzJ4" role="2OqNvi">
                  <node concept="1xMEDy" id="wsIXK0LzJ6" role="1xVPHs">
                    <node concept="chp4Y" id="wsIXK0LzMZ" role="ri$Ld">
                      <ref role="cht4Q" to="qyjc:5T7UbTDgCbd" resolve="StreamTestMethod" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3x8VRR" id="wsIXK0L_ci" role="2OqNvi" />
            </node>
            <node concept="2OqwBi" id="wsIXK0LBVD" role="3uHU7w">
              <node concept="nLn13" id="wsIXK0LBN1" role="2Oq$k0" />
              <node concept="1mIQ4w" id="wsIXK0LD7B" role="2OqNvi">
                <node concept="chp4Y" id="wsIXK0LDdR" role="cj9EA">
                  <ref role="cht4Q" to="qyjc:6M3wHT4XYt" resolve="StreamTestCase" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

