<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c6b19f28-306e-466c-a8f5-aa6eea606f72(hfu.mps.StreamSysT.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="jyas" ref="r:55f07890-1eb3-4198-b15d-e4bd575dc4d1(hfu.mps.Time.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
        <property id="672037151186491528" name="presentation" index="1L1pqM" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5T7UbTDgCbd">
    <property role="EcuMT" value="6793654467419079373" />
    <property role="TrG5h" value="StreamTestMethod" />
    <property role="34LRSv" value="Stream Test Method" />
    <ref role="1TJDcQ" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
    <node concept="PrWs8" id="5T7UbTDgCbe" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
    <node concept="PrWs8" id="1SqhhvM0HH1" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="5T7UbTDhmXB">
    <property role="EcuMT" value="6793654467419271015" />
    <property role="TrG5h" value="Pipeline" />
    <property role="3GE5qa" value="Pipeline" />
    <property role="34LRSv" value="Pipeline" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="PrWs8" id="5T7UbTDhmXC" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKKpfT">
    <property role="EcuMT" value="1880797030486479865" />
    <property role="TrG5h" value="EventSource" />
    <property role="3GE5qa" value="Source" />
    <property role="34LRSv" value="Source" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="PrWs8" id="1CpVIWKKvUK" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
    <node concept="PrWs8" id="7Xz8r_MZwX9" role="PzmwI">
      <ref role="PrY4T" node="7Xz8r_MZwX0" resolve="IKVSerdes" />
    </node>
    <node concept="1TJgyj" id="1CpVIWKP9wl" role="1TKVEi">
      <property role="IQ2ns" value="1880797030487726101" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="coder" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="1CpVIWKP9wo" role="1TKVEi">
      <property role="IQ2ns" value="1880797030487726104" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="eventType" />
      <ref role="20lvS9" to="tpee:g7uibYu" resolve="ClassifierType" />
    </node>
    <node concept="1TJgyj" id="RoIJycd2v9" role="1TKVEi">
      <property role="IQ2ns" value="997752894129448905" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="processes" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="1CpVIWKLwZ4" resolve="SourceProcess" />
    </node>
    <node concept="1TJgyj" id="Jw6Yu7OXjr" role="1TKVEi">
      <property role="IQ2ns" value="855714610430596315" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="attachTo" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="7MdWNyBagav" resolve="PipelineRef" />
    </node>
    <node concept="1TJgyi" id="5$bixu1fJ4N" role="1TKVEl">
      <property role="IQ2nx" value="6416303558932492595" />
      <property role="TrG5h" value="eventsWithTimestamp" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKKpwu">
    <property role="EcuMT" value="1880797030486480926" />
    <property role="TrG5h" value="EventSink" />
    <property role="3GE5qa" value="Sink" />
    <property role="34LRSv" value="Sink" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="PrWs8" id="1CpVIWKKvUI" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
    <node concept="PrWs8" id="7Xz8r_MZwXg" role="PzmwI">
      <ref role="PrY4T" node="7Xz8r_MZwX0" resolve="IKVSerdes" />
    </node>
    <node concept="1TJgyj" id="5$bixu1eN9b" role="1TKVEi">
      <property role="IQ2ns" value="6416303558932247115" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="coder" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="4o2stJ20FRT" role="1TKVEi">
      <property role="IQ2ns" value="5044719721275112953" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="type" />
      <ref role="20lvS9" to="tpee:g7uibYu" resolve="ClassifierType" />
    </node>
    <node concept="1TJgyj" id="1SqhhvLZL3$" role="1TKVEi">
      <property role="IQ2ns" value="2169122139678904548" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="from" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="5$bixu1glGY" role="1TKVEi">
      <property role="IQ2ns" value="6416303558932650814" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="columnsToDrop" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyi" id="Pq8sy4ipnW" role="1TKVEl">
      <property role="IQ2nx" value="962118615493613052" />
      <property role="TrG5h" value="multipleOutputs" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKKpFP">
    <property role="EcuMT" value="1880797030486481653" />
    <property role="TrG5h" value="Event" />
    <property role="3GE5qa" value="SourceProcess" />
    <ref role="1TJDcQ" node="1CpVIWKLwZ4" resolve="SourceProcess" />
    <node concept="1TJgyj" id="1CpVIWKNoWl" role="1TKVEi">
      <property role="IQ2ns" value="1880797030487265045" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="creation" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="5$bixu1fKD1" role="1TKVEi">
      <property role="IQ2ns" value="6416303558932499009" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="timestamp" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKKzQq">
    <property role="EcuMT" value="1880797030486523290" />
    <property role="TrG5h" value="AdvanceWatermark" />
    <property role="3GE5qa" value="SourceProcess" />
    <ref role="1TJDcQ" node="1CpVIWKLwZ4" resolve="SourceProcess" />
    <node concept="1TJgyj" id="4o2stJ22OSx" role="1TKVEi">
      <property role="IQ2ns" value="5044719721275674145" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="time" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyi" id="QA4204BsDk" role="1TKVEl">
      <property role="IQ2nx" value="983491298329479764" />
      <property role="TrG5h" value="advanceOption" />
      <ref role="AX2Wp" node="QA4204BsCY" resolve="AdvanceOption" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKLwZ4">
    <property role="TrG5h" value="SourceProcess" />
    <property role="EcuMT" value="1880797030486632334" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="SourceProcess" />
  </node>
  <node concept="1TIwiD" id="1CpVIWKRvId">
    <property role="TrG5h" value="EventSourceRef" />
    <property role="EcuMT" value="1880797030488341389" />
    <property role="3GE5qa" value="Source" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="RoIJycd2v7" role="1TKVEi">
      <property role="IQ2ns" value="997752894129448903" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1CpVIWKKpfT" resolve="EventSource" />
    </node>
  </node>
  <node concept="1TIwiD" id="1CpVIWKRvIg">
    <property role="TrG5h" value="EventSinkRef" />
    <property role="EcuMT" value="1880797030488341392" />
    <property role="3GE5qa" value="Sink" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="RoIJycd2vh" role="1TKVEi">
      <property role="IQ2ns" value="997752894129448913" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1CpVIWKKpwu" resolve="EventSink" />
    </node>
  </node>
  <node concept="25R3W" id="4o2stJ22OS5">
    <property role="3F6X1D" value="5044719721275674117" />
    <property role="3GE5qa" value="Validation" />
    <property role="TrG5h" value="AssertionScope" />
    <node concept="25R33" id="4o2stJ22OS6" role="25R1y">
      <property role="3tVfz5" value="5044719721275674118" />
      <property role="TrG5h" value="EarlyPane" />
      <property role="1L1pqM" value="early pane" />
    </node>
    <node concept="25R33" id="4o2stJ22OS7" role="25R1y">
      <property role="3tVfz5" value="5044719721275674119" />
      <property role="TrG5h" value="OnTimePane" />
      <property role="1L1pqM" value="on time pane" />
    </node>
    <node concept="25R33" id="4o2stJ22OSa" role="25R1y">
      <property role="3tVfz5" value="5044719721275674122" />
      <property role="TrG5h" value="LatePane" />
      <property role="1L1pqM" value="late pane" />
    </node>
    <node concept="25R33" id="4o2stJ24AP0" role="25R1y">
      <property role="3tVfz5" value="5044719721276140864" />
      <property role="TrG5h" value="Window" />
      <property role="1L1pqM" value="window" />
    </node>
  </node>
  <node concept="1TIwiD" id="4o2stJ22OSe">
    <property role="EcuMT" value="5044719721275674126" />
    <property role="TrG5h" value="Window" />
    <property role="3GE5qa" value="Window" />
    <property role="34LRSv" value="Window" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="1TJgyj" id="4o2stJ22XF8" role="1TKVEi">
      <property role="IQ2ns" value="5044719721275710152" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="from" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyj" id="4o2stJ22XFa" role="1TKVEi">
      <property role="IQ2ns" value="5044719721275710154" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="to" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="PrWs8" id="4o2stJ22XFY" role="PzmwI">
      <ref role="PrY4T" to="tpee:hCUYCKd" resolve="IValidIdentifier" />
    </node>
  </node>
  <node concept="1TIwiD" id="4o2stJ24AP5">
    <property role="EcuMT" value="5044719721276140869" />
    <property role="3GE5qa" value="Validation" />
    <property role="TrG5h" value="StreamAssert" />
    <property role="34LRSv" value="StreamAssert" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="1TJgyi" id="4o2stJ24AP6" role="1TKVEl">
      <property role="IQ2nx" value="5044719721276140870" />
      <property role="TrG5h" value="assertionScope" />
      <ref role="AX2Wp" node="4o2stJ22OS5" resolve="AssertionScope" />
    </node>
    <node concept="1TJgyi" id="4o2stJ24APt" role="1TKVEl">
      <property role="IQ2nx" value="5044719721276140893" />
      <property role="TrG5h" value="condition" />
      <ref role="AX2Wp" node="4o2stJ24APd" resolve="AssertionCondition" />
    </node>
    <node concept="1TJgyi" id="4o2stJ28Y9V" role="1TKVEl">
      <property role="IQ2nx" value="5044719721277284987" />
      <property role="TrG5h" value="description" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4o2stJ29Ywl" role="1TKVEl">
      <property role="IQ2nx" value="5044719721277548565" />
      <property role="TrG5h" value="inSameOrder" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="5$bixu1egpR" role="1TKVEl">
      <property role="IQ2nx" value="6416303558932104823" />
      <property role="TrG5h" value="defineQuantity" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="7MdWNyBf7G4" role="1TKVEl">
      <property role="IQ2nx" value="8975097057068743428" />
      <property role="TrG5h" value="applyFunction" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyj" id="7MdWNyBf7FZ" role="1TKVEi">
      <property role="IQ2ns" value="8975097057068743423" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="satisfiesFunc" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="4o2stJ24APr" role="1TKVEi">
      <property role="IQ2ns" value="5044719721276140891" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expectedEntries" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="5$bixu1d_8P" resolve="ExpectedEntry" />
    </node>
    <node concept="1TJgyj" id="4vp2seWke22" role="1TKVEi">
      <property role="IQ2ns" value="5177179982921785474" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="sink" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1CpVIWKRvIg" resolve="EventSinkRef" />
    </node>
    <node concept="1TJgyj" id="4vp2seWke2c" role="1TKVEi">
      <property role="IQ2ns" value="5177179982921785484" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="window" />
      <ref role="20lvS9" node="4vp2seWfjgJ" resolve="WindowRef" />
    </node>
  </node>
  <node concept="25R3W" id="4o2stJ24APd">
    <property role="3F6X1D" value="5044719721276140877" />
    <property role="3GE5qa" value="Validation" />
    <property role="TrG5h" value="AssertionCondition" />
    <node concept="25R33" id="4o2stJ24APe" role="25R1y">
      <property role="3tVfz5" value="5044719721276140878" />
      <property role="TrG5h" value="Exactly" />
    </node>
    <node concept="25R33" id="4o2stJ24APf" role="25R1y">
      <property role="3tVfz5" value="5044719721276140879" />
      <property role="TrG5h" value="AtLeast" />
    </node>
    <node concept="25R33" id="4o2stJ24APi" role="25R1y">
      <property role="3tVfz5" value="5044719721276140882" />
      <property role="TrG5h" value="AtMost" />
    </node>
    <node concept="25R33" id="4o2stJ24APm" role="25R1y">
      <property role="3tVfz5" value="5044719721276140886" />
      <property role="TrG5h" value="Exclude" />
    </node>
  </node>
  <node concept="1TIwiD" id="5$bixu1d_8P">
    <property role="EcuMT" value="6416303558931927605" />
    <property role="3GE5qa" value="Validation" />
    <property role="TrG5h" value="ExpectedEntry" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5$bixu1d_8Q" role="1TKVEi">
      <property role="IQ2ns" value="6416303558931927606" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyi" id="5$bixu1d_8W" role="1TKVEl">
      <property role="IQ2nx" value="6416303558931927612" />
      <property role="TrG5h" value="count" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="5$bixu1eN8W">
    <property role="EcuMT" value="6416303558932247100" />
    <property role="TrG5h" value="EngineConfig" />
    <property role="3GE5qa" value="EngineConfig" />
    <property role="34LRSv" value="Config" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2XsxYmZ1fpa" role="PzmwI">
      <ref role="PrY4T" node="2XsxYmZ1fp6" resolve="IEngineConfig" />
    </node>
    <node concept="PrWs8" id="67vTL6Xt5QM" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="67vTL6Xq54a" role="1TKVEl">
      <property role="IQ2nx" value="7052609604863676682" />
      <property role="TrG5h" value="applicationName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="67vTL6Xq54c" role="1TKVEl">
      <property role="IQ2nx" value="7052609604863676684" />
      <property role="TrG5h" value="bootstrapServer" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="67vTL6Xyhc9" role="1TKVEi">
      <property role="IQ2ns" value="7052609604865823497" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="sparkSession" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="67vTL6Xyhcb" resolve="SparkSession" />
    </node>
  </node>
  <node concept="1TIwiD" id="yQ4gUuO8YZ">
    <property role="EcuMT" value="627707952556445631" />
    <property role="3GE5qa" value="Pointer" />
    <property role="TrG5h" value="SourcePipelinePointer" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="4vp2seWfeuM" role="1TKVEi">
      <property role="IQ2ns" value="5177179982920476594" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="sourceRef" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="1CpVIWKRvId" resolve="EventSourceRef" />
    </node>
    <node concept="1TJgyj" id="4vp2seWfeuV" role="1TKVEi">
      <property role="IQ2ns" value="5177179982920476603" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="pipelineRef" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="7MdWNyBagav" resolve="PipelineRef" />
    </node>
  </node>
  <node concept="PlHQZ" id="7Xz8r_MZwX0">
    <property role="TrG5h" value="IKVSerdes" />
    <property role="EcuMT" value="9179217545893121855" />
    <node concept="1TJgyi" id="5$bixu1ePp9" role="1TKVEl">
      <property role="IQ2nx" value="6416303558932256329" />
      <property role="TrG5h" value="extendedSerialization" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyj" id="7Xz8r_MZwX1" role="1TKVEi">
      <property role="IQ2ns" value="9179217545893121857" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="keySerdes" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="7Xz8r_MZwX3" role="1TKVEi">
      <property role="IQ2ns" value="9179217545893121859" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="valueSerdes" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="2XsxYmZ1fp5">
    <property role="EcuMT" value="3412752038462027333" />
    <property role="TrG5h" value="EngineConfigRef" />
    <property role="3GE5qa" value="EngineConfig" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="PrWs8" id="2XsxYmZ1fp8" role="PzmwI">
      <ref role="PrY4T" node="2XsxYmZ1fp6" resolve="IEngineConfig" />
    </node>
    <node concept="1TJgyj" id="2XsxYmZ1hR3" role="1TKVEi">
      <property role="IQ2ns" value="3412752038462037443" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5$bixu1eN8W" resolve="EngineConfig" />
    </node>
  </node>
  <node concept="PlHQZ" id="2XsxYmZ1fp6">
    <property role="TrG5h" value="IEngineConfig" />
    <property role="EcuMT" value="3412752038462027330" />
    <property role="3GE5qa" value="EngineConfig" />
    <node concept="PrWs8" id="2XsxYmZ5_5V" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="6M3wHT4XYt">
    <property role="EcuMT" value="122175582331199389" />
    <property role="TrG5h" value="StreamTestCase" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Stream Test Case" />
    <ref role="1TJDcQ" to="tpee:fz12cDA" resolve="ClassConcept" />
    <node concept="1TJgyj" id="6M3wHT4XYA" role="1TKVEi">
      <property role="IQ2ns" value="122175582331199398" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="engineConfig" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5$bixu1eN8W" resolve="EngineConfig" />
    </node>
  </node>
  <node concept="1TIwiD" id="QA4204A5TJ">
    <property role="EcuMT" value="983491298329124463" />
    <property role="3GE5qa" value="SourceProcess" />
    <property role="TrG5h" value="AdvanceProcessingTime" />
    <ref role="1TJDcQ" node="1CpVIWKLwZ4" resolve="SourceProcess" />
    <node concept="1TJgyj" id="QA4204A5TK" role="1TKVEi">
      <property role="IQ2ns" value="983491298329124464" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="time" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="jyas:5Q9IfCXUjXS" resolve="ITime" />
    </node>
    <node concept="1TJgyi" id="QA4204BsD3" role="1TKVEl">
      <property role="IQ2nx" value="983491298329479747" />
      <property role="TrG5h" value="advanceOption" />
      <ref role="AX2Wp" node="QA4204BsCY" resolve="AdvanceOption" />
    </node>
  </node>
  <node concept="25R3W" id="QA4204BsCY">
    <property role="3F6X1D" value="983491298329479742" />
    <property role="3GE5qa" value="SourceProcess" />
    <property role="TrG5h" value="AdvanceOption" />
    <node concept="25R33" id="QA4204BsCZ" role="25R1y">
      <property role="3tVfz5" value="983491298329479743" />
      <property role="TrG5h" value="by" />
    </node>
    <node concept="25R33" id="QA4204BsD0" role="25R1y">
      <property role="3tVfz5" value="983491298329479744" />
      <property role="TrG5h" value="to" />
    </node>
  </node>
  <node concept="1TIwiD" id="7MdWNyBagav">
    <property role="EcuMT" value="8975097057067467423" />
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="PipelineRef" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="7MdWNyBagaw" role="1TKVEi">
      <property role="IQ2ns" value="8975097057067467424" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="5T7UbTDhmXB" resolve="Pipeline" />
    </node>
  </node>
  <node concept="1TIwiD" id="575gSEd4Soi">
    <property role="EcuMT" value="5892189949835576850" />
    <property role="TrG5h" value="StreamSysTStatement" />
    <ref role="1TJDcQ" to="tpee:fzclF8l" resolve="Statement" />
  </node>
  <node concept="1TIwiD" id="575gSEd5WFG">
    <property role="EcuMT" value="5892189949835856620" />
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="RunPipeline" />
    <property role="34LRSv" value="Run" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="PrWs8" id="Jw6Yu7QT3q" role="PzmwI">
      <ref role="PrY4T" node="Jw6Yu7QSWI" resolve="ICanWait" />
    </node>
    <node concept="1TJgyj" id="4vp2seWeOvG" role="1TKVEi">
      <property role="IQ2ns" value="5177179982920370156" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="7MdWNyBagav" resolve="PipelineRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="575gSEd8CXW">
    <property role="EcuMT" value="5892189949836562300" />
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="RunAllPipelines" />
    <property role="34LRSv" value="Run All" />
    <ref role="1TJDcQ" node="575gSEd4Soi" resolve="StreamSysTStatement" />
    <node concept="PrWs8" id="Jw6Yu7QSWL" role="PzmwI">
      <ref role="PrY4T" node="Jw6Yu7QSWI" resolve="ICanWait" />
    </node>
  </node>
  <node concept="PlHQZ" id="Jw6Yu7QSWI">
    <property role="EcuMT" value="855714610431102766" />
    <property role="3GE5qa" value="Pipeline" />
    <property role="TrG5h" value="ICanWait" />
    <node concept="1TJgyi" id="Jw6Yu7QSWJ" role="1TKVEl">
      <property role="IQ2nx" value="855714610431102767" />
      <property role="TrG5h" value="wait" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vp2seWfjgJ">
    <property role="EcuMT" value="5177179982920496175" />
    <property role="3GE5qa" value="Window" />
    <property role="TrG5h" value="WindowRef" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="4vp2seWfjgK" role="1TKVEi">
      <property role="IQ2ns" value="5177179982920496176" />
      <property role="20kJfa" value="ref" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4o2stJ22OSe" resolve="Window" />
    </node>
  </node>
  <node concept="1TIwiD" id="Pq8sy4bYU8">
    <property role="EcuMT" value="962118615491931784" />
    <property role="3GE5qa" value="Sink" />
    <property role="TrG5h" value="PartialSink" />
    <property role="34LRSv" value="Pipeline.Get" />
    <ref role="1TJDcQ" node="1CpVIWKRvIg" resolve="EventSinkRef" />
    <node concept="1TJgyj" id="Pq8sy4bYUb" role="1TKVEi">
      <property role="IQ2ns" value="962118615491931787" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="splitter" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="67vTL6Xyhcb">
    <property role="EcuMT" value="7052609604865823499" />
    <property role="3GE5qa" value="EngineConfig" />
    <property role="TrG5h" value="SparkSession" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="67vTL6Xyhcz" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="67vTL6Xyhcc">
    <property role="EcuMT" value="7052609604865823500" />
    <property role="3GE5qa" value="EngineConfig" />
    <property role="TrG5h" value="SparkSessionRef" />
    <property role="34LRSv" value="spark_session" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
  </node>
</model>

